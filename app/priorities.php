<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class priorities extends Model
{
    protected $primaryKey 	= 'prioritiesID';
    protected $table 		= 'priorities';

    protected $fillable = [
    		'name',
    		'description'
    ];
}
