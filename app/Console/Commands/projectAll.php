<?php

namespace App\Console\Commands;
use DB;
use Input;
use \App;
use \Auth;
use App\projects;
use App\priorities;
use App\suppliers;
use App\projectStatus;
use App\departments;
use App\projectmood;
use App\comment;
use App\dates;
use App\days;
use App\users;
use App\GanttTask;
use App\GanttLink;
use View;
use Validator;
use Session;
use Carbon\Carbon;
use Page;
use Request;
use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class projectAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:all';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush all projects away from dashboard & archive';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        

        $getProjectStatus = projects::where(function ($query) {
                                $query->where('projectStatusID', '=', 3)    //rejected
                                      ->orWhere('projectStatusID', '=', 4)  //wash up
                                      ->orWhere('projectStatusID', '=', 7) //complete
                                      ->orWhere('projectStatusID', '=', 2); //on hold
                                    })->get();

        // Create the logger
        $logger = new Logger('artisan');
        $logger->pushHandler(new StreamHandler(storage_path().'/logs/artisan.log', Logger::INFO));
        $logger->pushHandler(new FirePHPHandler());
        $logger->addInfo(count($getProjectStatus).' Projects are in rejected/washup/complete/on hold now');

        foreach ($getProjectStatus as $key => $value) 
        {
            $dt = $value->updated_at;
                        
            if(Carbon::now()->lte($dt->addDays(2)))
            {
                $projects = projects::findOrFail($value->projectsID);
      
                $input['projectShow'] = "N";
      
                $projects->update($input);

            }
        }
    }
}
