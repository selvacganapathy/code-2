<?php

namespace App\Console\Commands;
use DB;
use Input;
use \App;
use \Auth;
use App\projects;
use App\priorities;
use App\suppliers;
use App\projectStatus;
use App\departments;
use App\projectmood;
use App\comment;
use App\dates;
use App\days;
use App\users;
use App\GanttTask;
use App\GanttLink;
use View;
use Validator;
use Session;
use Carbon\Carbon;
use Page;
use Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Illuminate\Console\Command;

class projectOnHold extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:onhold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush all complete on hold projects from dashboard & archive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
                $getProjectStatus = projects::where(function ($query) {
                                $query->where('projectStatusID', '=', 2);    //on hold
                                    })->get();
        $logger = new Logger('artisan');
        $logger->pushHandler(new StreamHandler(storage_path().'/logs/artisan.log', Logger::INFO));
        $logger->pushHandler(new FirePHPHandler());
        $logger->addInfo(count($getProjectStatus).' Projects are on hold');

        foreach ($getProjectStatus as $key => $value) 
        {
            $dt = $value->updated_at;
                        
            if(Carbon::now()->lte($dt->addDays(\Config::get('const.GRACE_PERIOD_ON_HOLD'))))
            {
                $projects = projects::findOrFail($value->projectsID);
      
                $input['projectShow'] = "N";
      
                $projects->update($input);

            }
        }
    }
}
