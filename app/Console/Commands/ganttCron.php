<?php

namespace App\Console\Commands;

use DB;
use Input;
use \App;
use \Auth;
use App\projects;
use App\priorities;
use App\suppliers;
use App\projectStatus;
use App\departments;
use App\projectmood;
use App\comment;
use App\dates;
use App\days;
use App\users;
use App\GanttTask;
use App\GanttLink;
use View;
use Validator;
use Session;
use Carbon\Carbon;
use Page;
use Request;
use Illuminate\Console\Command;

class ganttCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gantt:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command use to update gannt chart tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projectPlanDates   = array('planDISCOVERStartDate',
                                'planStartDate',
                                'planDOStartDate',
                                'planIMPLEMENTStartDate',
                                'planCOMPLETEStartDate');

        $projectPlanDays    = array('planDISCOVERDays',
                                'planDays',
                                'planDODays',
                                'planIMPLEMENTDays',
                                'planCOMPLETEDays');

        $projectActualDates   = array('actualDISCOVERStartDate',
                                'actualStartDate',
                                'actualDOStartDate',
                                'actualIMPLEMENTStartDate',
                                'actualCOMPLETEStartDate');

        $projectActualDays    = array('actualDISCOVERDays',
                                'actualDays',
                                'actualDODays',
                                'actualIMPLEMENTDays',
                                'actualCOMPLETEDays');
        $projectStages        = array('DISCOVER',
                                    'PLAN',
                                    'DO',
                                    'IMPLEMENT',
                                    'CLOSE'
                                );

        $projects = projects::all();
        

        /**
         * PROJECT INSERT
         * @var [type]
         */
        GanttTask::truncate();
        GanttLink::truncate();
        
        foreach($projects as $key => $value)
        {
            $ganttChart['text'] = $value->projectName;
            $ganttChart['start_date'] = $value->planDISCOVERStartDate;
            $ganttChart['end_date'] = $value->planCOMPLETEStartDate;
            
            /**
             * CALCULATE TOTAL PROJECT DURATION
             */
            $totalDays = new Carbon($value->planCOMPLETEStartDate);
            $planCOMPLETEEndDate = $totalDays->addDays($value->planCOMPLETEDays);
            $ganttChart['duration'] = $totalDays->diffInDays(new Carbon($value->planDISCOVERStartDate));
            $ganttChart['parent'] = 0;


            $projectUploadGantt = GanttTask::create($ganttChart);
            $projectLastInsertID = $projectUploadGantt->id;
            
            /**
             * PLAN TASK INSERT
             * @var integer
             */
            


            $i = 0;
            foreach($projectPlanDates as $planDateKey)
            {
                $ganttChart['text'] = $projectStages[$i];

                $ganttChart['start_date'] = $value->$planDateKey;

                $date = new Carbon($value->$planDateKey);

                $ganttChart['end_date'] = $date->addDays($value->$projectPlanDays[$i]);

                $ganttChart['duration'] = $value->$projectPlanDays[$i];

                $ganttChart['parent'] = $projectLastInsertID;

                $taskUploadGantt = GanttTask::create($ganttChart);

                $taskLastinsertID = $taskUploadGantt->id;

                $ganttLink['source'] = $projectLastInsertID;
                $ganttLink['target'] = $taskLastinsertID;
                $ganttLink['type'] = 1;


                GanttLink::create($ganttLink);
            /**
             * ACTUAL TASK INSERT
             * @var integer
             */
                if($taskLastinsertID && !is_null($value->$projectActualDates[$i]) && !is_null($value->$projectPlanDays[$i]))
                {
                    $ganttChart['parent'] = $taskLastinsertID;

                    $ganttChart['text'] = 'Actual';
                    
                    $ganttChart['start_date'] = $value->$projectActualDates[$i];

                    $actualEnddate = new Carbon($value->$planDateKey);

                    $ganttChart['end_date'] = $actualEnddate->addDays($value->$projectPlanDays[$i]);
                    
                    $ganttChart['duration'] = $value->$projectActualDays[$i];

                    $actualTask = GanttTask::create($ganttChart);

                    $actualTaskLastInsertID = $actualTask->id;

                    $ganttLink['source'] = $taskLastinsertID;
                    
                    $ganttLink['target'] = $actualTaskLastInsertID;
                    
                    $ganttLink['type'] = 2;

                    GanttLink::create($ganttLink);
                }
                $i++;
            }

        }
        \Log::info(' The gantt chart table updated ');
    }
}
