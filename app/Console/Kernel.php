<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\ganttCron::class,
        \App\Console\Commands\projectClose::class,
        \App\Console\Commands\projectOnHold::class,
        \App\Console\Commands\projectRejected::class,
        \App\Console\Commands\projectWashup::class,
        \App\Console\Commands\projectAll::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('gantt:update')
                 ->everyMinute();
                 
        $schedule->command('project:close')
                 ->everyMinute();

        $schedule->command('project:onhold')
                 ->everyMinute();
    }
}
