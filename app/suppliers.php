<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suppliers extends Model
{
    protected $primaryKey 	= 'suppliersID';
    protected $table 		= 'suppliers';

    protected $fillable = [
    		'name',
    		'description'
    ];
}
