<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projectaccess extends Model
{
    protected $primaryKey 	= 'accessID';
    protected $table 		= 'projectaccess';

    protected $fillable = [
    		'usersID',
    		'projectsID',
    		'userType'
    ];
}
