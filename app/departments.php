<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departments extends Model
{
    protected $primaryKey 	= 'departmentsID';
    protected $table 		= 'departments';

    protected $fillable = [
    		'name',
    		'description'
    ];

}
