<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projectStatus extends Model
{
    //

    protected $primaryKey 	= 'projectStatusID';
    protected $table 		= 'project_statuses';

    protected $fillable = [
    		'name',
    		'description',
    		'colorCode',
    ];
}
