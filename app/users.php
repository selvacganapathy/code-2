<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    //
    protected $primaryKey 	= 'usersID';
    protected $table 		= 'users';

protected $fillable = [
    		'username',
    		'email',
    		'userType',
            'NickName',
    		'userGroup',
    		'firstName',
    		'lastName'
    ];
} 
