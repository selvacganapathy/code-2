<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projectmood extends Model
{
    protected $primaryKey 	= 'projectmoodID';
    protected $table 		= 'projectMood';

    protected $fillable = [
    		'name',
    		'description'
    ];
}
