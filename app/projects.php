<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class projects extends Model
{
    
    protected $primaryKey = 'projectsID';

    protected $table = 'projects';
    
    protected $fillable = [
            'projectRef',
    		'projectNE',
    		'projectNO',
    		'projectShow',
            'systemID',
            'projectName',
            'prioritiesID',
            'projectStatusID',
            'suppliersID',
            'departmentsID',
            'usersID',
            'projectManagerID',
            'readerID',
            'projectMood',
            'projectStage',
            'planDISCOVERStartDate',
            'actualDISCOVERStartDate',
            'planStartDate',
            'actualStartDate',
            'planDOStartDate',
            'actualDOStartDate',
            'planIMPLEMENTStartDate',
            'actualIMPLEMENTStartDate',
            'planCOMPLETEStartDate',
            'actualCOMPLETEStartDate',
            'planDISCOVERDays',
            'actualDISCOVERDays',
            'planDays',
            'actualDays',
            'planDODays',
            'actualDODays',
            'planIMPLEMENTDays',
            'actualIMPLEMENTDays',
            'planCOMPLETEDays',
            'actualCOMPLETEDays',
            'includeCIL'
    ];
    
    /**
     * [setActualDISCOVERStartDateAttribute set attrubute to null if empty date]
     * @param [date] $value [set attrubute to null if empty date]
     */
    public function setActualDISCOVERStartDateAttribute($value) 
    {
        if ($value != '' )
        $this->attributes['actualDISCOVERStartDate'] = $this->dateValidation($value);
    }
    public function setActualStartDateAttribute($value) 
    {
        $this->attributes['actualStartDate'] = $this->dateValidation($value);
    }
    public function setActualDOStartDateAttribute($value) 
    {
        $this->attributes['actualDOStartDate'] = $this->dateValidation($value);
    }
    public function setActualIMPLEMENTStartDateAttribute($value) 
    {
        $this->attributes['actualIMPLEMENTStartDate'] = $this->dateValidation($value);
    }
    public function setActualCOMPLETEStartDateAttribute($value) 
    {
        $this->attributes['actualCOMPLETEStartDate'] = $this->dateValidation($value);
    }

    /**
     * [setActualDISCOVERDaysAttribute discover days attribute]
     * @param [Integer] $value [set the value to NULL]
     */
    public function setActualDISCOVERDaysAttribute($value) 
    {
        $this->attributes['actualDISCOVERDays'] = empty($value) ? NULL : $value;
    }
    public function setActualDaysAttribute($value) 
    {
        $this->attributes['actualDays'] = empty($value) ? NULL : $value;
    }
    public function setActualDODaysAttribute($value) 
    {
        $this->attributes['actualDODays'] = empty($value) ? NULL : $value;
    }
    public function setActualIMPLEMENTDaysAttribute($value) 
    {
        $this->attributes['actualIMPLEMENTDays'] = empty($value) ? NULL : $value;
    }
    public function setActualCOMPLETEDaysAttribute($value) 
    {
        $this->attributes['actualCOMPLETEDays'] = empty($value) ? NULL : $value;
    }

    //convert date formats
    public function setPlanDISCOVERStartDateAttribute($value) 
    {
        $this->attributes['planDISCOVERStartDate'] = $this->dateValidation($value);
    }
     public function setPlanStartDateAttribute($value) 
    {
        $this->attributes['planStartDate'] = $this->dateValidation($value);
    }
     public function setPlanDOStartDateAttribute($value) 
    {
        $this->attributes['planDOStartDate'] = $this->dateValidation($value);
    }
     public function setPlanIMPLEMENTStartDateAttribute($value) 
    {
        $this->attributes['planIMPLEMENTStartDate'] = $this->dateValidation($value);
    }
     public function setPlanCOMPLETEStartDateAttribute($value) 
    {
        $this->attributes['planCOMPLETEStartDate'] = $this->dateValidation($value);
    }
   

public function dateValidation($date)
{
    if($date !== "" && $date !== "1970-01-01")
    {
        return date("Y-m-d", strtotime($date));        
    }
    return NULL;

}

}
