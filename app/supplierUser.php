<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplierUser extends Model
{

    protected $primaryKey 	= 'supplier_usersID';
    protected $table 		= 'supplier_users';

    protected $fillable = [
    		'name',
    		'description'
    ];

}
