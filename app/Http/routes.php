<?php

Route::get('errors','errorController@index');
Route::get('restrict','errorController@restrict');

Route::get('/', function () {
    return redirect('errors')->with('flash_message', 'You have no access to view this page.');;
});

Route::match(['get', 'post'], '/scheduler_data', "SchedulerController@data");

Route::get('/gantt', function () {
    return view('gantt');
});

Route::match(['get', 'post'], '/gantt_data', "GanttController@data");

Route::get('/auth/login', function () {
    return redirect('errors')->with('flash_message', 'You have no access to view this page.');
});

Route::get('/auth/register', function () {
    return redirect('errors')->with('flash_message', 'You have no access to view this page.');
});

Route::group(array('prefix' => 'admin'), function()
    {

       Route::get('/',function() {

	 		return redirect('errors')->with('flash_message', 'You have no access to view this page.');

		});
        route::get('/{token?}/{id?}','ssoController@index');

    });
/**
 * PROJECT  CONTROLLER
 */

Route::get('projects',['as'=>'projects','uses' => 'projectsController@index']);

Route::get('projects/create',['as'=>'createproject','uses' => 'projectsController@create']);

Route::get('projects/{id}',['as'=>'showproject','uses' => 'projectsController@show']);

Route::post('projects',['uses' => 'projectsController@store']);

Route::get('projects/{id}/edit', ['as'=>'editproject','uses' => 'projectsController@edit']);

Route::post('projects/{id}/edit', ['as'=>'updateproject','uses' => 'projectsController@update']);

Route::post('projects/{id}', ['uses' => 'projectsController@createcomment']);

Route::post('archive/{projectID}', ['uses' => 'projectsController@archive']);

Route::get('downloadCIL/{tagName}/{tagID}', ['uses' => 'projectsController@downloadCIL']);

Route::get('CILview', ['uses' => 'projectsController@CILview']);

Route::get('getCIL','projectsController@getCIL');

Route::post('putCIL','projectsController@putCIL');

Route::post('/CIL','projectsController@updateCIL');


/**
 * ARCHIVE PROJECT CONTROLLER
 */
Route::get('/archive',['as'=>'archivedProject','uses' => 'archiveController@index']);

Route::get('archived/{status}', ['uses' => 'archiveController@show']);
Route::get('archived/projects/{status}', ['uses' => 'archiveController@makelive']);

/**
 * users controller 
 */

Route::get('users','userController@index');

Route::get('users/create','userController@create');

Route::get('users/{id}/edit',['as'=>'showusers','uses' => 'userController@edit']);

Route::post('users', array('uses' => 'userController@store'));

Route::post('users/{id}', array('uses' => 'userController@destroy'));

/**
 * IMPORT PROJECT CONTROLLER
 */
Route::get('import','importController@index');

Route::post('import','importController@store');

Route::get('export','exportController@index');

Route::post('export','exportController@create');


Route::get('/logout', function() {
		
		Session::flush();
      	return view('pages.logout');

 });
/**
 * 
 */
