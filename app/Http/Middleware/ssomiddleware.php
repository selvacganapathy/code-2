<?php

namespace App\Http\Middleware;

use Request;
use App\users;
use Session;
use App\Http\Requests;
use Config;
use Closure;
use \App;
use View;
use App\projects;
use App\projectaccess;
use Illuminate\Session\Store;
class ssomiddleware
{
    protected $session;

    protected $timeout = 1800;

    public function __construct(Store $session){
        $this->session=$session;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {

        if(time() - $this->session->get('activity.lastActivityTime') > $this->timeout)
        {
            $this->session->forget('activity.lastActivityTime');

            Session::flush();

            return redirect('errors')->with(['flash_message' => 'You had not activity for '.$this->timeout/60 .' minutes.']);
        }

        $this->session->put('activity.lastActivityTime',time());

                $routename = $request->route()->getName();

        if($this->isDeleted(Session::get('user.username'))->isEmpty())
        {
            return redirect('errors')->with('flash_message', 'Your are not authenticated into the system.
                                            please contact system admin to renew your authentication');
        } 

        /**
         * RESTRICT PAGE BASED ON USER TYPE
         */
        
        if(Session::get('user.userType') == 'READER' && ($routename == 'createproject' || $routename == 'editproject' || $routename == 'deleteproject'))
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        elseif(Session::get('user.userType') == 'PROJECTMANAGER' && ($routename == 'createproject' || $routename == 'deleteproject'))
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        elseif(Session::get('user.userType') == 'PROJECTADMIN' && $routename == 'deleteproject' )
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        /**
         * SET UP USER PERMISSION 
         */
        
        if(Session::get('user.userType') == 'READER')
        {
            Session::set('projects.key','readerID');

            Session::set('projects.permission',100);
        }
        elseif(Session::get('user.userType') == 'PROJECTMANAGER')
        {
            Session::set('projects.key','projectManagerID');

            Session::set('projects.permission',200);
        }
        elseif(Session::get('user.userType') == 'PROJECTADMIN')
        {
            Session::set('projects.permission',300);
        }
        elseif(Session::get('user.userType') == 'SYSTEMADMIN')
        {
            Session::set('projects.permission',400);
        }

        // if($routename == 'showproject' && $this->isUserAllowedToView(Session::get('user.usersID'),$request->id)->isEmpty() && Session::get('projects.permission') <=200)
        // {
        //     return redirect('errors')->with('flash_message', 'Your are not entitled to view the project');
        // }
        return $next($request);
    }
    
    public function isDeleted($username) 
    {
        $userDeleted = users::whereRaw('username ="'.$username.'" AND isDeleted = "N"')->get();
        return $userDeleted;
    }
    
    public function isUserAllowedToView($usersID,$projectsID) 
    {
        $isUserAllowedToView = projectaccess::whereRaw('usersID ="'.$usersID.'" AND projectsID ="'.$projectsID.'"')->get();

        return $isUserAllowedToView;
    }


}
