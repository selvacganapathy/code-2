<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\users;
use \App;
use View;
use App\projects;

class usertypemiddleware
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routename = $request->route()->getName();

        if($this->isDeleted(Session::get('user.username'))->isEmpty())
        {
            return redirect('errors')->with('flash_message', 'Your are not authenticated into the system.
                                            please contact system admin to renew your authentication');
        } 

        $response = $next($request);

        /**
         * RESTRICT PAGE BASED ON USER TYPE
         */
        
        if(Session::get('user.userType') == 'READER' && ($routename == 'createproject' || $routename == 'editproject' || $routename == 'deleteproject'))
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        elseif(Session::get('user.userType') == 'PROJECTMANAGER' && ($routename == 'createproject' || $routename == 'deleteproject'))
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        elseif(Session::get('user.userType') == 'PROJECTADMIN' && $routename == 'deleteproject' )
        {
            return redirect('restrict')->with('flash_message', 'You dont have access to view this page');
        }
        /**
         * SET UP USER PERMISSION 
         */
        
        if(Session::get('user.userType') == 'READER')
        {
            Session::set('projects.key','readerID');
            Session::set('projects.permission',100);
        }
        elseif(Session::get('user.userType') == 'PROJECTMANAGER')
        {
            Session::set('projects.key','projectManagerID');
            Session::set('projects.permission',200);
        }
        elseif(Session::get('user.userType') == 'PROJECTADMIN')
        {
            Session::set('projects.permission',300);
        }
        elseif(Session::get('user.userType') == 'SYSTEMADMIN')
        {
            Session::set('projects.permission',400);
        }

        return $response;
    }

    public function isDeleted($username) 
    {
        $userDeleted = users::whereRaw('username ="'.$username.'" AND isDeleted = "N"')->get();
        return $userDeleted;
    }

}
