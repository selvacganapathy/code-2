<?php

namespace App\Http\Controllers;

use Request;
use App\users;
use Session;
use App\Http\Requests;
use Config;
use App\Http\Controllers\Controller;

class ssoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($token,$id)
    {
        $error = null;

        $validateUser = $this->SSOVerification('json','pca',$token,$id);

        if(isset($validateUser->error))
        {
          return redirect('errors')->with('flash_message', $validateUser->error); 
        }
        elseif(isset($validateUser->user))
        {
          $authenticatedUsers = users::where('username',$validateUser->user)->get();
          $authenticatedUsers->toArray();

          if(!$authenticatedUsers->isEmpty())
          {
            Session::set('user.username', $authenticatedUsers[0]['username']);
            Session::set('user.firstName',$authenticatedUsers[0]['firstName']);
            Session::set('user.lastName', $authenticatedUsers[0]['lastName']);
            Session::set('user.userType', $authenticatedUsers[0]['userType']);
            Session::set('user.usersID', $authenticatedUsers[0]['usersID']);
            Session::set('activity.lastActivityTime', time());
            
            return redirect()->route('projects');
          }
          else
          {
            return redirect('errors')->with('flash_message', 'We are not able to authenticate you into the system'); 
          }

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function SSOVerification($format,$brand,$token,$systemId) 
    {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.inetpca.co.uk/v2.1/$format/$brand/decryption/$token/$systemId");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:WN1snkYhj5CgjBNEmjfX7EeeqGxliLk2PzJTE0D33Mg0k16o8veMCnmtHGd66t'));

            $response = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            $curl_error = curl_error($ch);

            curl_close($ch);

            if ($curl_errno > 0)
            {
                $data[] = "cURL Error ($curl_errno): $curl_error\n";
            }
            else
            {
                $data = json_decode($response);
            }
            if($data)
            {
                return $data;
            }  
            return false; 
    }
}
