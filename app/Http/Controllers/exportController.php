<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use DB;
use App\Http\Requests;
use App\projects;
use App\Http\Controllers\Controller;

class exportController extends Controller
{
    public function __construct()
    {
        $this->middleware('sso');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = DB::table('projects')
                    ->select('priorities.name as priorityname','project_statuses.name as projectstatusname', 'projects.*','pm.firstName as pmfirstname','rd.firstName as rdfirstname','pm.lastName as pmlastname','rd.lastName as rdlastname')
                    ->leftjoin('project_statuses', 'projects.projectStatusID', '=', 'project_statuses.projectStatusID') 
                    ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID')
                    ->leftjoin('users as pm', 'projects.projectManagerID', '=', 'pm.usersID')
                    ->leftjoin('users as rd', 'projects.readerID', '=', 'rd.usersID')
                    ->get();

        foreach($projects as $object)
        {
            $arrays[] =  (array) $object;
        }
        Excel::create('ITUK 2015 Project Scheduling', function($excel) use($arrays) {

        $excel->sheet('Project List', function($sheet) use($arrays) {
            $sheet->cell('A1', function($cell) {
                // Set black background
                $cell->setBackground('#000000');
            });
            
            $sheet->fromArray($arrays);

            });

        })->export('xls');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
