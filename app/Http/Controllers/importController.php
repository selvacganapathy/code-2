<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Excel;
use App\projects;
use App\priorities;
use App\departments;
use App\projectStatus;
use App\comment;
use projectmood;
use App\users;
use App\suppliers;
use App\supplierUser;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class importController extends Controller
{
    public function __construct()
    {
        $this->middleware('sso');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        return view('pages.import');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
      if(Input::hasFile('file'))
      {
        $isError = false;
        Excel::load(Input::file('file'), function($reader) use (&$isError) 
        {
            $reader->formatDates(true, 'Y-m-d');
            $rows = $reader->toArray();

            foreach($rows as $row)
            {
                /**
                 * [$priorityID CONVERT THE PRIORITY TO PRIORITY ID]
                 */ 
                $row['projectRef'] = $row['project_id'];
                $row['projectNE'] = $row['ne'];
                $row['projectNO'] = $row['no'];
                $row['projectShow'] = "N";
                $row['systemID'] = $row['system_id'];
                $row['projectName'] = $row['project'];
                $row['projectStage'] = 'DISCOVER';
                $row['planDISCOVERStartDate'] = $row['plandiscoverstart'];
                $row['planStartDate'] = $row['planplanstart'];
                $row['planDOStartDate'] = $row['plandostart'];
                $row['planIMPLEMENTStartDate'] = $row['planimplementstart'];
                $row['planCOMPLETEStartDate'] = $row['plancompletestart'];
                $row['planDISCOVERDays'] = $row['plandiscoverduration'];
                $row['planDays'] = $row['planplanduration'];
                $row['planDODays'] = $row['plandoduration'];
                $row['planIMPLEMENTDays'] = $row['planimplementduration'];
                $row['planCOMPLETEDays'] = $row['plancompleteduration'];
                $row['actualDISCOVERStartDate'] = $row['actualdiscoverstart'];
                $row['actualStartDate'] = $row['actualplanlstart'];
                $row['actualDOStartDate'] = $row['actualdostart'];
                $row['actualIMPLEMENTStartDate'] = $row['actualimplementstart'];
                $row['actualCOMPLETEStartDate'] = $row['actualcompletestart'];
                $row['actualDISCOVERDays'] = $row['actualdiscoverduration'];
                $row['actualDays'] = $row['actualplanduration'];
                $row['actualDODays'] = $row['actualdoduration'];
                $row['actualIMPLEMENTDays'] = $row['actualimplementduration'];
                $row['actualCOMPLETEDays'] = $row['actualcompleteduration'];


                $priority = priorities::get();

                
                foreach($priority as $priorityID => $priorityName)
                {
                    // echo $row->prioritytracked.' / '.$priorityName['name']."<br>";
                    if($row['prioritytracked'] == $priorityName['name'])
                    {
                        $row['prioritiesID'] = $priorityName->prioritiesID;
                        break;
                    }
                    else
                    {
                        $row['prioritiesID'] = 0;
                    }
                }

                /**
                 * [$departmentsID CONVERT THE department TO department ID]
                 * @var [type]
                 */
                $departments = departments::get();
                
                foreach($departments as $departmentsID => $departmentsName)
                {
                    if($row['department'] == $departmentsName['name'])
                    {
                        $row['departmentsID'] = $departmentsName->departmentsID;
                        break;
                    }
                    else
                    {

                        $row['departmentsID'] = 0;
                    }
                }
                /**
                 * [$status CONVERT THE status TO status ID]
                 * @var [type]
                 */
                $projectStatus = projectStatus::get();

                foreach($projectStatus as $projectStatusID => $projectStatusName)
                {
                    if($row['status'] == $projectStatusName['name'])
                    {
                        $row['projectStatusID'] = $projectStatusName->projectStatusID;
                        break;
                    }
                    else
                    {
                        $row['projectStatusID'] = 6;
                    }
                }

                /**
                 * [$pm CONVERT THE pm TO pm ID]
                 * @var [type]
                 */
                $projectManager = users::get();
                
                unset($pmList);

                $pmList = array();
                
                $pm = explode(",", str_replace('/',',',$row['pm']));
                
                foreach ($pm as $pmkey => $pmvalue) 
                {
                    foreach($projectManager as $projectManagerID => $projectManagerName)
                    {
                        if($pmvalue == $projectManagerName['NickName'])
                        {
                            $row['getprojectManagerID'] = $projectManagerName->usersID;
                            $pmList[]= $row['getprojectManagerID'];
                            break;
                        }
                        else
                        {
                            $row['projectManagerID'] = 1;
                        }
                    }
                }
                $row['projectManagerID'] = implode(",",$pmList);

                /**
                 * [$pm CONVERT THE pm TO pm ID]
                 * @var [type]
                 */
                unset($readersList);

                $readersList = array();
                
                $projectReader = users::get();
                
                $readers = explode(",", str_replace('/',',',$row['resources']));

                foreach ($readers as $readerskey => $readersvalue) 
                {
                    foreach($projectReader as $projectReaderID => $projectReaderName)
                    {
                        if($readersvalue == $projectReaderName['NickName'])
                        {
                            $row['getReaderID'] = $projectReaderName->usersID;
                            $readersList[]= $row['getReaderID'];
                            break;
                        }
                        else
                        {
                            $row['readerID'] = 1;
                        }
                    }
                }  

                $row['readerID'] = implode(",",$readersList);

                $suppliers = suppliers::get();

                foreach($suppliers as $suppliersID => $suppliersName)
                {
                    if($row['supplier'] == $suppliersName['name'])
                    {
                        $row['suppliersID'] = $suppliersName->suppliersID;
                        break;
                    }
                    else
                    {
                        $row['suppliersID'] = 14;
                    }
                }
                $suppliersUsers = supplierUser::get();

                foreach($suppliersUsers as $suppliersUsersID => $suppliersUsersName)
                {
                    if($row['user'] == $suppliersUsersName['name'])
                    {
                        $row['usersID'] = $suppliersUsersName->supplier_usersID;
                        break;
                    }
                    else
                    {
                        $row['usersID'] = 14;
                    }
                }
                if($row['mood'] == "J")
                {
                    $row['projectMood'] = 1;
                }
                elseif($row['mood'] == "K")
                {
                    $row['projectmood'] = 2;
                }
                else
                {
                    $row['projectmood'] = 3;
                }

                if($row['projectRef'] !== null)
                {
                    $insertID = projects::create($row);  
                    $row['projectsID'] = $insertID->projectsID;
                    $row['title'] = "Migration";
                    $row['userID'] = 1;
                    if($row['weekly_commentary'] !== null)
                    {
                        $row['comments'] = $row['weekly_commentary'];
                        comment::create($row);                       
                    }
                }                
            echo "Current Execution finish : ".$row['project_id']."<br>";
            }
        });
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
