<?php

namespace App\Http\Controllers;

use DB;
use Input;
use \App;
use \Auth;
use App\projects;
use App\priorities;
use App\suppliers;
use App\projectStatus;
use App\departments;
use App\projectmood;
use App\projectaccess;
use App\comment;
use App\dates;
use App\days;
use App\users;
use View;
use Validator;
use Session;
use Carbon\Carbon;
use Page;
use Request;
use App\departmentTags;
use App\tags;
use Excel;

// use Illuminate\Http\Request;
// use App\Http\Requests\Request;
use App\Http\Requests\projectcomments;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class projectsController extends Controller
{
    /**
     * 
     * 
     */
    
    public function __construct()
    {
        $this->middleware('sso');
    } 
    
    /**
     * display list of projects
     * @param variabl $projects display the project information
     * @return view navigate to list projects
     */                 
    public function index()
    {

        $variable = Session::get('projects.key');

        $variable1 = Session::get('user.usersID');

        $projects['myprojects'] = DB::table('projects')
                    ->select('priorities.name as priorityname','project_statuses.name as projectstatusname','project_statuses.colorCode as colorCode', 'projects.*','pm.firstName','pm.lastName','rd.firstName','rd.lastName')
                    ->leftjoin('project_statuses', 'projects.projectStatusID', '=', 'project_statuses.projectStatusID') 
                    ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID')
                    ->leftjoin('projectaccess', 'projects.projectsID', '=', 'projectaccess.projectsID')
                    ->leftjoin('users as pm', 'projects.projectManagerID', '=', 'pm.usersID')
                    ->leftjoin('users as rd', 'projects.readerID', '=', 'rd.usersID')
                    ->where(function($query) use ($variable, $variable1) {
 
                        if(Session::has('projects.key'))
                        {
                            $query->whereRaw('FIND_IN_SET('.$variable1.',projects.'.$variable.')');
                        }

                    })
                    ->where('projects.projectShow','=','Y')
                    ->distinct()
                    ->orderBy('projectsID','DESC')
                    ->get();
        //get the projects ID 's 
        // COMPARE THEY NOT IN THE BOTTOM LIST'
        $userProjects = '';

        for ($i=0; $i < count($projects['myprojects']); $i++) 
        { 
          // WHAT IF NO PROJECTS
          $userProjects[]  = $projects['myprojects'][$i]->projectsID;
        }
        
        if(Session::get('projects.permission') <= 200)
        {
          $projects['allprojects'] = DB::table('projects')
                    ->select('priorities.name as priorityname','project_statuses.colorCode as colorCode','project_statuses.name as projectstatusname', 'projects.*')
                    ->leftjoin('project_statuses', 'projects.projectStatusID', '=', 'project_statuses.projectStatusID') 
                    ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID')
                    ->leftjoin('users as pm', 'projects.projectManagerID', '=', 'pm.usersID')
                    ->leftjoin('users as rd', 'projects.readerID', '=', 'rd.usersID')
                    ->leftjoin('projectaccess', 'projectaccess.projectsID', '=', 'projects.projectsID')
                    // ->whereNotIn('projectaccess.projectsID',$userProjects) 
                    ->where('projects.projectShow','=','Y')
                    ->distinct()->get();
        }
        /**
         * USER HAVE NO PROJECTS
         */
        if(null == $projects)
        {
            return redirect('restrict')->with('flash_message', 'Sorry You have no projects now');
        }

        /**
         * [$projectActualDates DATE ATTRIBUTES FOR CALCULATION]
         * @var $projectactual dates
         */
        
        $projectPlanDates   = array('planDISCOVERStartDate','planStartDate','planDOStartDate','planIMPLEMENTStartDate','planCOMPLETEStartDate');
        $projectPlanDays    = array('planDISCOVERDays','planDays','planDODays','planIMPLEMENTDays','planCOMPLETEDays');
        $projectActualDates = array('actualDISCOVERStartDate','actualStartDate','actualDOStartDate','actualIMPLEMENTStartDate','actualCOMPLETEStartDate');
        $projectActualDays  = array('actualDISCOVERDays','actualDays','actualDODays','actualIMPLEMENTDays','actualCOMPLETEDays');
        $projectStages      = array('DISCOVER','PLAN','DO','IMPLEMENT','CLOSE');

        foreach($projects as $allprojectkey => $allprojectvalue)
        {
          foreach ($allprojectvalue as $key => $value) 
          {
            /**
             * PROJECT MANAGER AND READER
             */
            $projectManagers = explode(",", $value->projectManagerID);

            $readers = explode(",", $value->readerID);

            $calculation['pmlist'] = users::whereIn('users.usersID',$projectManagers)->get(array('NickName'));
    
            $calculation['rdlist'] = users::whereIn('users.usersID',$readers)->get(array('NickName'));
            
            /**
             * GET TOTAL PLAN AND ACTUAL DAYS
             */
            $totalPlanDays = 0;

            $totalActualDays = 0;

            $calculation['totalPercentage'] = 0;

            $calculation['currentActualStage'] = $value->projectStage;
            /**
             * TOTAL PLAN DAYS
             */
            foreach ($projectPlanDays as $planDateKey => $planDateValue) 
            {
                $totalPlanDays+= $value->$planDateValue;
            }
            /**
             * TOTAL ACTUAL DAYS
             */
             foreach ($projectActualDays as $actualDateKey => $actualDateValue) 
            {
                $totalActualDays+= $value->$actualDateValue;
            }
                        
            $calculation['totalPlanDays'] = $totalPlanDays;

            $calculation['totalActualDays'] = $totalActualDays;            
            /******************************** FINE UNTIL HERE **********************************************************/
            
            /**
             * CALCULATE COMPLETED PERCENTAGE - PLAN
             * CALCULATE CURRENT PERCENTAGE - PLAN
             */
            
            $calculation['planCompletePercentage'] = 0;
            $calculation['currentPlanStage'] = "NI-S";
            $calculation['currentPlanPercentage'] = "NI-S";
            $i=0;
            foreach ($projectPlanDates as $key1 => $planDate) 
            {
            /**
             * IF PLAN PHASE DATES EXIST - NOT NULL - VALUE IS NOT '1970-01-01' - TOTAL PLAN DAYS IS NOT ZERO
             * ex. GET planDISCOVEREndDate = planDISCOVERStartDate + planDISCOVERDays
             */
            if(array_key_exists($planDate, $value) && ( $value->$planDate !== null || $value->$planDate !== '1970-01-01' ) && $calculation['totalPlanDays'] !== 0)
              {
                  $dt = new Carbon($value->$planDate);

                  $planEndDate = str_replace('Start', 'End', $planDate);
                  /**
                   * NO ERROR IF WE ADD NULL VALUE TO A DATE - CARBON HANDLES IT
                   */
                  $calculation[$planEndDate] = $dt->addDays($value->$projectPlanDays[$i]);
                  
                  $value3 = str_replace('StartDate', 'Percentage', $planDate);
                  
                  $calculation[$value3]= round(($value->$projectPlanDays[$i] /  $calculation['totalPlanDays']) * 100,1);

                  if( Carbon::now()->between(new Carbon($value->$planDate),$calculation[$planEndDate]))
                  {

                      $calculation['currentPlanStage'] = $projectStages[$i];
                      break;
                  }
                  elseif( Carbon::now()->lt(new Carbon($value->$projectPlanDates[0])))
                  {
                      $calculation['currentPlanStage'] = "NOT STARTED";
                      break;
                  }
                  elseif( Carbon::now()->gt(new Carbon($value->$projectPlanDates[4])))
                  {

                      $calculation['currentPlanStage'] = "COMPLETE";
                      break;
                  }


              }
              elseif(array_key_exists($planDate, $value) && ($value->$planDate !== null || $value->$planDate !== '1970-01-01') && $calculation['totalPlanDays'] == 0)
              {
                      $calculation['currentPlanStage'] = "PM-D";
                      $calculation['currentPlanPercentage'] = "PM-DP";
                      break;
              }
              elseif(array_key_exists($planDate, $value) && ($value->$planDate == null || $value->$planDate == '1970-01-01') && $calculation['totalPlanDays'] !== 0)
              {
                      $calculation['currentPlanStage'] = "PM-Y";
                      $calculation['currentPlanPercentage'] = "PM-YP";
                      break;

              }
              else
              {
                      $calculation['currentPlanStage'] = "ND";
                      $calculation['currentPlanPercentage'] = "ND";
                      break;
              }
              $i++;
              if(isset($calculation['currentPlanStage']) && ($value->$planDate !== null))
              {
                $currentPlanPercentage = 0;
                $calculation['currentPlanPercentage'] =+ $currentPlanPercentage;
                if(key($projectStages) == 4)
                {
                  $calculation['currentPlanPercentage'] = 100 - $calculation['currentPlanPercentage'];
                }
              }
              else
              {
                $calculation['currentPlanPercentage'] = "- 0% -";
              }
              /**
                     * validate the plan percentage calculation
                     */
                    // $calculation['totalPercentage']+=$calculation[$value3];           
              }

            /**
             * CALCULATE COMPLETED PERCENTAGE - ACTUAL
             */
            
            $j=0;
            foreach ($projectActualDates as $key2 => $actualDateValues) 
            { 
                if(array_key_exists($actualDateValues, $value) && $projectActualDates[$j] && $calculation['totalPlanDays'] !==0)
                    {
                        $dtActual = new Carbon($value->$actualDateValues);
                        
                        $value2 = str_replace('Start', 'End', $actualDateValues);
                        
                        $calculation[$value2] = $dtActual->addDays($value->$projectActualDays[$j]);
                        
                        $value3 = str_replace('StartDate', 'Percentage', $actualDateValues);
                        
                        $calculation[$value3]= round(($value->$projectActualDays[$j] /  $calculation['totalPlanDays']) * 100,1);

                        /**
                         * create another instance for passing carbon method ????? because object will be altered to carbon method 
                         * @var [type]
                         */
                        if($projectStages[$j] == $value->projectStage)
                        {
                            
                            $calculation['actualCompletePercentage'] =$calculation[$value3];
                            $calculation['actualCompletePercentage']+=$calculation['actualCompletePercentage'];
                        }
                        $dtactual1 = new Carbon($value->$actualDateValues);
                        $dtactual2 = new Carbon($value->$actualDateValues);


                        $gracePeriodStartDate1 = $dtactual1->addDays($value->$projectActualDays[$j]);

                        $gracePeriodStartDate2 = $dtactual2->addDays($value->$projectActualDays[$j]);

                        $gracePeriodStartDate2->subDays(2);

                        if(Carbon::now()->between($gracePeriodStartDate1,$gracePeriodStartDate2) && $projectStages[$j] == $value->projectStage)
                        {
                             $calculation['tableRowColor'] = '#FCF8E3'; //'#FFCC00';
                        }
                        /**
                         * check the between dates in carbon
                         */
                        elseif(Carbon::now()->gt($gracePeriodStartDate1) && $projectStages[$j] == $value->projectStage)
                        {
                            $calculation['tableRowColor'] =  '#F2DEDE'; //" #FF4D4D";

                        }
                        else
                        {
                            $calculation['tableRowColor'] = "#FFFFFF";
                        }
                        
                        
                    }
                $j++;
                    /**
                     * validate the plan percentage calculation
                     */
                    //$calculation['totalPercentage']+=$calculation[$value3];           

            }

            /**
             * mark red
             */

            /**
             * push to object 
             * @var [type]
             */
            // $calculation['currentPlanStage'] = "ND";            
            $calculation['tableRowColor'] = "#FFFFFF";
            $calculation['actualCompletePercentage'] = 0;
            // $calculation['currentPlanStage'] = "EMPTY";
            // $calculation['tableRowColor'] = "#FFFFFF";
            foreach ($calculation as $key => $value4) 
            {
               $value->$key = $value4;
            }
        }
      }
        return view('projects.index',compact('projects'));        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
public function create()
{
    $maxID = projects::whereRaw('projectRef = (select max(`projectRef`) from projects)')->get(['projectRef']);

    $int = filter_var($maxID[0]->projectRef, FILTER_SANITIZE_NUMBER_INT);
    
    $dropdowns['nextID'] = "SR0".($int+1);

    $dropdowns['suppliers'] =suppliers::lists('name','suppliersID');

    $dropdowns['projectStatus'] = projectStatus::lists('name','projectStatusID');

    $dropdowns['priorities'] = priorities::lists('name','prioritiesID');

    $dropdowns['departments'] = departments::lists('name','departmentsID');
    
    $dropdowns['projectmood'] = projectmood::lists('name','projectmoodID');

    $dropdowns['projectmanager'] = users::where('userType', '=', 'PROJECTMANAGER')->lists('NickName','usersID');
    
    $dropdowns['reader'] = users::where('userType', '=', 'READER')->lists('NickName','usersID');

    return view('projects.create')->with('dropdowns', $dropdowns);

}



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        // public function submit()
        // {
        //     dd(Input::all());
        //     projects::create($request->all());

        //     return redirect('projects');
        // }
    public function show($id)
    {
           
        $projectTable = array();       

        $projectTables['projectInfo'] = DB::table('projects')
                                        ->select('priorities.name as priorityname','project_statuses.name as projectstatusname','suppliers.name as suppliername' ,'supplier_users.name as supplier_user_name' ,'departments.name as departmentsname','projects.*','pm.firstName as pmfirstname','rd.firstName as rdfirstname','pm.lastName as pmlastname','rd.lastName as rdlastname')
                                        ->leftjoin('project_statuses', 'projects.projectStatusID', '=', 'project_statuses.projectStatusID') 
                                        ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID')
                                        ->leftjoin('users as pm', 'projects.projectManagerID', '=', 'pm.usersID')
                                        ->leftjoin('users as rd', 'projects.readerID', '=', 'rd.usersID')
                                        ->leftjoin('departments','projects.departmentsID','=','departments.departmentsID')
                                        ->leftjoin('suppliers','projects.suppliersID','=','suppliers.suppliersID')
                                        ->leftjoin('supplier_users','projects.usersID','=','supplier_users.supplier_usersID')
                                        ->where('projectsID', $id)
                                        ->get(); 

        $projectTables['commentTable'] = DB::table('comment')
                                        ->select('comment.created_at','comment.comments','comment.title','users.firstName','users.lastName')
                                        ->leftjoin('users','comment.userID','=','users.usersID')
                                        ->where('projectsID', $id)
                                        ->orderBy('users.created_at', 'asc')
                                        ->get(); 
        foreach($projectTables['projectInfo'] as $key => $value)
        {
          $projectManagers = explode(",", $value->projectManagerID);

          $readers = explode(",", $value->readerID);

          $calculation['pmlist'] = users::whereIn('users.usersID',$projectManagers)->get(array('NickName'));
    
          $calculation['rdlist'] = users::whereIn('users.usersID',$readers)->get(array('NickName'));        
        
          foreach ($calculation as $key => $value4) 
          {
             $value->$key = $value4;
          }
        }
        return view('projects.show')->with('projectTables',$projectTables);
    }

    public function createcomment($id)
    {
        $input = Request::all();

        comment::create($input);

        return $this->show($id);

    }

    /**
     * edit the project 
     * @param  $projectsID [description]
     * @return [type]             [description]
     */
    public function edit($id)
    {    
    $projectActualDates   = array(
                    'planDISCOVERStartDate',
                    'planStartDate',
                    'planDOStartDate',
                    'planIMPLEMENTStartDate',
                    'planCOMPLETEStartDate',
                    'actualDISCOVERStartDate',
                    'actualStartDate',
                    'actualDOStartDate',
                    'actualIMPLEMENTStartDate',
                    'actualCOMPLETEStartDate');

        $dropdowns['suppliers'] =suppliers::lists('name','suppliersID');

        $dropdowns['projectStatus'] = projectStatus::lists('name','projectStatusID');

        $dropdowns['priorities'] = priorities::lists('name','prioritiesID');

        $dropdowns['departments'] = departments::lists('name','departmentsID');
        
        $dropdowns['projectmood'] = projectmood::lists('name','projectmoodID');

        $dropdowns['projectmanager'] = users::where('userType', '=', 'PROJECTMANAGER')->lists('NickName','usersID');
        
        $dropdowns['reader'] = users::where('userType', '=', 'READER')->lists('NickName','usersID');

        $dropdowns['readerSelected'] = projects::where('projectsID', '=',$id)->first(['readerID'])->toArray();
        
        $dropdowns['pmSelected'] = projects::where('projectsID', '=',$id)->first(['projectManagerID'])->toArray();

        $dropdowns['pmSelected'] = explode(",",$dropdowns['pmSelected']['projectManagerID']);
        
        $dropdowns['readerSelected'] = explode(",",$dropdowns['readerSelected']['readerID']);
        
        $dropdowns['projectInfo'] = DB::table('projects')
                                        ->select('priorities.name as priorityname','project_statuses.name as projectstatusname','suppliers.name as suppliername' ,'departments.name as departmentsname','projects.*','pm.firstName as pmfirstname','rd.firstName as rdfirstname','pm.lastName as pmlastname','rd.lastName as rdlastname')
                                        ->leftjoin('project_statuses', 'projects.projectStatusID', '=', 'project_statuses.projectStatusID') 
                                        ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID')
                                        ->leftjoin('users as pm', 'projects.projectManagerID', '=', 'pm.usersID')
                                        ->leftjoin('users as rd', 'projects.readerID', '=', 'rd.usersID')
                                        ->leftjoin('departments','projects.departmentsID','=','departments.departmentsID')
                                        ->leftjoin('suppliers','projects.suppliersID','=','suppliers.suppliersID')
                                        ->where('projectsID', $id)
                                        ->get(); 

        $dropdowns['pmlist'] = explode(",", $dropdowns['projectInfo'][0]->projectManagerID);
        $dropdowns['rdlist'] = explode(",", $dropdowns['projectInfo'][0]->readerID);

        if(Session::get('projects.permission') < 200)
        { 
          $disable = 'readonly';
        }
        else
        {
          $disable = false;
        }

        return view('projects.edit',compact('dropdowns','disable'));
    }
    
    /**
     *
     * 
     */
    public function store(Request $request)
    {
        $rules = array(
                'projectRef'            => 'required',                                 
                'projectName'           => 'required',                     
                'projectMood'           => 'required',     
                'projectManagerID'      => 'required', 
                'prioritiesID'          => 'required',        
                'projectStatusID'       => 'required',        
                'departmentsID'         => 'required',    
                'suppliersID'           => 'required',   
                'readerID'              => 'required', 
                'planDISCOVERStartDate' => 'required',   
                'planDISCOVERDays'      => 'required',   
                'planStartDate'         => 'required',    
                'planDays'              => 'required',   
                'planDOStartDate'       => 'required',   
                'planDODays'            => 'required',   
                'planIMPLEMENTStartDate'=> 'required',  
                'planIMPLEMENTDays'     => 'required',  
                'planCOMPLETEStartDate' => 'required',  
                'planCOMPLETEDays'      => 'required',  
        );

        $validator = Validator::make(Input::all(), $rules);
         // check if the validator failed -----------------------
        if ($validator->fails()) 
        {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('projects/create')
                ->withInput($request->only('projectRef', 'projectName','projectMood','projectManagerID','prioritiesID','projectStatusID','departmentsID',
                  'suppliersID','readerID','planDISCOVERStartDate','planDISCOVERDays','planStartDate','planDays','planDOStartDate','planDODays','planIMPLEMENTStartDate','planIMPLEMENTDays','planCOMPLETEStartDate','planCOMPLETEDays'))
                   ->withErrors($validator);

        }
        $projectIDalreadyExist =  projects::all()->where('projectRef',Input::get('projectRef'));
        if (!$projectIDalreadyExist->isEmpty()) 
        { 

            return Redirect::to('projects/create')
                       ->withErrors([
                    'username' => 'project already exist ',
                ]);
        }
        else
        {       
            $input = Request::all();
            // dd(Input::get('readerID'));
            $input['projectManagerID'] = implode(",", Input::get('projectManagerID'));
            $input['readerID'] = implode(",", Input::get('readerID'));

            $projects = projects::create($input);
            $lastInsertedId= $projects->projectsID;
            $input['projectsID'] = $lastInsertedId;
            foreach (Input::get('readerID') as $key) {
              $input['usersID'] = $key;
              projectaccess::create($input);
            }
            foreach (Input::get('projectManagerID') as $key) {
              $input['usersID'] = $key;
              projectaccess::create($input);
            }

            return redirect('projects');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
      $rules = array(
                'projectRef'            => 'required',                                 
                'projectName'           => 'required',                     
                'projectMood'           => 'required',     
                'projectManagerID'      => 'required', 
                'prioritiesID'          => 'required',        
                'projectStatusID'       => 'required',        
                'departmentsID'         => 'required',    
                'suppliersID'           => 'required',   
                'readerID'              => 'required', 
                'planDISCOVERStartDate' => 'required',   
                'planDISCOVERDays'      => 'required',   
                'planStartDate'         => 'required',    
                'planDays'              => 'required',   
                'planDOStartDate'       => 'required',   
                'planDODays'            => 'required',   
                'planIMPLEMENTStartDate'=> 'required',  
                'planIMPLEMENTDays'     => 'required',  
                'planCOMPLETEStartDate' => 'required',  
                'planCOMPLETEDays'      => 'required',  
        );

        $validator = Validator::make(Input::all(), $rules);
         // check if the validator failed -----------------------
        if ($validator->fails()) 
        {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('projects/'.$id.'/edit')->withErrors($validator);

        }
            $input = Request::all();
            $input['projectManagerID'] = implode(",", Input::get('projectManagerID'));
            $input['readerID'] = implode(",", Input::get('readerID'));

            $projects = projects::findOrFail($id);

            $projects->update($input);

        return $this->show($id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function archive($id)
    {
      $projects = projects::findOrFail($id);
      
      $input['projectShow'] = "N";
      
      $projects->update($input);


      return redirect()->route('projects');
    
    }
    public function logger()
    {
      
    }
    public function CILview()
    { 
      $tags = tags::all();
      return view('projects.cil',compact('tags')); 
    }
    public function downloadCIL($tagName,$tagID)
    {

      $projectsCIL  = DB::table('projects')
        ->select('projects.projectRef','projects.projectName','projects.projectsID')
        ->leftjoin('departments', 'departments.departmentsID', '=', 'projects.departmentsID') 
        ->leftjoin('departments_tags', 'departments_tags.departmentsID', '=', 'departments.departmentsID')
        ->leftjoin('tags', 'tags.tagID', '=', 'departments_tags.tagID')
        ->where('tags.tagID',$tagID)
        ->where('projects.includeCIL','Y')
        ->get();

      foreach ($projectsCIL as $key => $value) 
        {

            $comment = $this->getComments($value->projectsID);
            
             $values[] = (array)$value;
   
               $i=1;
              foreach ($comment as $commentKey => $commentValue) {
                        
                    $values[]['comments_'.$i] = $commentValue['comments'];

                }
                $i++;
           $values1[]=$values; 

        }
      Excel::create('CIL_REPORT_'.date('jS_F_Y'), function($excel) use($values,$tagName) {

        $excel->sheet($tagName, function($sheet)  use($values){

            $sheet->with($values);

            });

        })->export('xls');

    }
    public function getComments($projectsID)
    {
        $getComments = comment::where('projectsID',$projectsID)->orderBy('projectsID')->get();
      
        return $getComments->toArray();
    }
    public function updateCIL()
    {
        if(Request::ajax()) 
        {
          $projects = projects::findOrFail(Input::get('projectsID'));

          $input['includeCIL'] = Input::get('includeCIL');

          $projects->update($input);

        }
    }

    public function getCIL()
    {

      $tags = tags::get(['tagName','tagID']);
      

      $nonAssignDepartments = DB::table('departments')
                                        ->select('*')
                                        ->whereNotIn('departments.departmentsID', function($query)
                                        {
                                            $query->select(DB::raw('departments_tags.departmentsID FROM departments_tags'));
                                        })
                                        ->get(); 
      $assignDepartments    = DB::table('departments')
                                    ->select('*')
                                    ->join('departments_tags', 'departments_tags.departmentsID', '=', 'departments.departmentsID')
                                    ->get();      
                                       
      return view('projects.getCIL',compact('tags','assignDepartments','nonAssignDepartments'));
    } 
    public function putCIL()
    {

      foreach(Input::get('departmentsID') as $departmentID)
      {


            $input['departmentsID'] = $departmentID;

            $input['tagID']         = Input::get('tagID');
            
            departmentTags::create($input);
      }

      return $this->getCIL();
    }
    
}