<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\projects;
use App\Http\Requests;
use Khill\Lavacharts\Lavacharts;
use App\Http\Controllers\Controller;
use DB;

class archiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('sso');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archivedProjects = projects::where(function ($query) {
                                $query->where('projectStatusID', '=', 3)    //rejected
                                      ->orWhere('projectStatusID', '=', 4);  //wash up
                                      })
                                      ->where('projectShow','=','N')
                                      ->get();

        $totalProjects      = projects::count();

        $onHoldProjects     = projects::where('projectStatusID','=',2)->count();

        $rejectedProjects   = projects::where('projectStatusID', '=',3)->count() ;
        
        $washupProjects     = projects::where('projectStatusID', '=', 4)->count() ;

        $liveProjects       = projects::where('projectStatusID','=',6)->count();
        
        $NewProjects        = projects::where('projectStatusID','=',1)->count();

        $completeProjects   = projects::where('projectStatusID','=',7)->count();

        $projectsByPM       =  DB::table('projects')
                                ->select('users.nickName',
                                  DB::raw('count(projects.projectsID) as myProjects'),
                                  DB::raw('sum(case when projectStatusID = 1 then 1 else 0 end) newEnquiry'), 
                                  DB::raw('sum(case when projectStatusID = 2 then 1 else 0 end) onHold'),
                                  DB::raw('sum(case when projectStatusID = 3 then 1 else 0 end) rejected'),
                                  DB::raw('sum(case when projectStatusID = 4 then 1 else 0 end) washUp'),
                                  DB::raw('sum(case when projectStatusID = 6 then 1 else 0 end) inProgress'),
                                  DB::raw('sum(case when projectStatusID = 7 then 1 else 0 end) completed'))
                                ->leftjoin('users', 'projects.projectManagerID', '=', 'users.usersID') 
                                ->where(function($query) {

                                        $query->whereRaw('FIND_IN_SET(users.usersID,projects.projectManagerID)');
                                })
                                ->groupBy('users.usersID')
                                ->orderBy('users.usersID','ASC')
                                ->get();

        $projectsByReader       =  DB::table('projects')
                                 ->select('users.nickName',
                                  DB::raw('count(projects.projectsID) as myProjects'),
                                  DB::raw('sum(case when projectStatusID = 1 then 1 else 0 end) newEnquiry'), 
                                  DB::raw('sum(case when projectStatusID = 2 then 1 else 0 end) onHold'),
                                  DB::raw('sum(case when projectStatusID = 3 then 1 else 0 end) rejected'),
                                  DB::raw('sum(case when projectStatusID = 4 then 1 else 0 end) washUp'),
                                  DB::raw('sum(case when projectStatusID = 6 then 1 else 0 end) inProgress'),
                                  DB::raw('sum(case when projectStatusID = 7 then 1 else 0 end) completed'))
                                ->leftjoin('users', 'projects.readerID', '=', 'users.usersID') 
                                ->where(function($query) {

                                        $query->whereRaw('FIND_IN_SET(users.usersID,projects.readerID)');
                                })
                                ->groupBy('users.usersID')
                                ->orderBy('users.usersID','ASC')
                                ->get();                        

        $projectMood            = DB::table('projects')
                                ->select('projectMood.name',DB::raw('count(projects.projectsID) as myProjects'))
                                ->leftjoin('projectMood', 'projects.projectMood', '=', 'projectMood.projectMoodID') 
                                ->groupBy('projectMood.projectMoodID')
                                ->orderBy('projects.projectMood','ASC')
                                ->get();

        $projectpriority        = DB::table('projects')
                                ->select('priorities.name',DB::raw('count(projects.projectsID) as myProjects'))
                                ->leftjoin('priorities', 'projects.prioritiesID', '=', 'priorities.prioritiesID') 
                                ->groupBy('projects.prioritiesID')
                                ->orderBy('projects.prioritiesID','ASC')
                                ->get();
                
        $department             = DB::table('projects')
                                 ->select('departments.name',
                                  DB::raw('count(projects.projectsID) as myProjects'),
                                  DB::raw('sum(case when projectStatusID = 1 then 1 else 0 end) newEnquiry'), 
                                  DB::raw('sum(case when projectStatusID = 2 then 1 else 0 end) onHold'),
                                  DB::raw('sum(case when projectStatusID = 3 then 1 else 0 end) rejected'),
                                  DB::raw('sum(case when projectStatusID = 4 then 1 else 0 end) washUp'),
                                  DB::raw('sum(case when projectStatusID = 6 then 1 else 0 end) inProgress'),
                                  DB::raw('sum(case when projectStatusID = 7 then 1 else 0 end) completed'))
                                ->leftjoin('departments', 'projects.departmentsID', '=', 'departments.departmentsID') 
                                ->groupBy('projects.departmentsID')
                                ->orderBy('projects.departmentsID','ASC')
                                ->get();


    $selva = ['name'=>'selva','wins' => 50 ];

    $gana = ['name'=>'ganapathy','wins' => 30 ];
      
    return view('projects.archives',compact('archivedProjects',
                                            'totalProjects',
                                            'projectsByPM',
                                            'projectsByReader',
                                            'onHoldProjects',
                                            'rejectedProjects',
                                            'washupProjects',
                                            'liveProjects',
                                            'NewProjects',
                                            'completeProjects',
                                            'projectMood',
                                            'projectpriority',
                                            'department',
                                            'selva',
                                            'gana'));


    }
    public function live($id)
    {
      $projects = projects::findOrFail($id);
      
      $input['projectShow'] = "N";
      
      $projects->update($input);


      return $this->index();
    
    }
    public function show($status)
    {
        if($status == 'Rejected')
        {
            $getProjects = projects::where(function ($query) {
            $query->where('projectStatusID', '=', 3)    //rejected
                    ->where('projectShow', '=', 'N');
                })
                ->get();
            $title = "Rejected Projects";

        }
        elseif($status == 'OnHold')
        {
            $getProjects = projects::where(function ($query) {
            $query->where('projectStatusID', '=', 2);    //rejected
                })
                ->where('projectShow', '=', 'N')
                ->get();
            $title = "On Hold Projects";

        }
        elseif($status == 'washup')
        {
            $getProjects = projects::where(function ($query) {
            $query->where('projectStatusID', '=', 4);    //rejected
                })
                ->where('projectShow', '=', 'N')
                ->get();
            $title = "Wash up Projects";

        }
        elseif($status == 'complete')
        {
            $getProjects = projects::where(function ($query) {
            $query->where('projectStatusID', '=', 7);    //rejected
                })
                ->where('projectShow', '=', 'N')
                ->get();
            $title = "Completed Projects";

        }

            return view('projects.view',compact('getProjects','title'));

    }
    public function makelive($projectsID){

    $projects = projects::findOrFail($projectsID);
      
    $input['projectShow'] = "Y";
      
    $projects->update($input);

    return redirect()->route('projects');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
