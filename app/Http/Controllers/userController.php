<?php

namespace App\Http\Controllers;

use App\users;
use Request;
//use Illuminate\Http\Request;
//use App\Http\Requests;
// use App\Http\Requests\Request;
use App\Http\Controllers\Controller;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('sso');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    $users = users::all();

    return view('users.index')->with('users',$users);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
            
        $input = Request::all();
        
        users::create($input);

        return redirect('users');



    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
        $user = users::findorFail($id);
        return view('users.edit',compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $deleteUser = users::findorFail($id);

        dd($deleteUser);

        $users->delete();

        Session::flash('flash_message','User deleted successfully');

        return redirect()->route('users.index');
    }
}
