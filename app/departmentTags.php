<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departmentTags extends Model
{
    protected $primaryKey 	= 'departmentTagID';
    protected $table 		= 'departments_tags';
	public $timestamps = false;
    protected $fillable = [
    		'departmentsID',
    		'tagID'
    ];
}
