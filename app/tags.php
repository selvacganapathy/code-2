<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tags extends Model
{
    protected $primaryKey 	= 'tagID';
    protected $table 		= 'tags';

    protected $fillable = [
    		'tagName',
    ];
}
