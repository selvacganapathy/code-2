<?php

use Carbon\Carbon;
use Khill\Lavacharts\Lavacharts;
use App\projects;
use App\departmentTags;
use App\tags;

function getLatestComments($id)
{
    $latestComment = DB::table('comment')
                    ->select('comment.comments','comment.created_at','users.firstname','users.lastname')
                    ->leftjoin('users','comment.userID','=','users.usersID')
                    ->where('projectsID', $id)
                   	->orderBy('comment.created_at', 'desc')
                    ->take(5)
                    ->get(); 

foreach ($latestComment as $key => $value) {
	for ($i=0; $i < count($latestComment); $i++) { 
		$timeAgo = Carbon::parse($value->created_at)->format('jS \\of F Y h:i A');
	 	$value->timeAgo = $timeAgo;
	}
	
	}
    return $latestComment;
}

function getAllUsersNames()
{
	$userNames = DB::table('users')
				->select('firstname','lastname','NickName')
				->get();

	return $userNames;
}


function CILprojects($tagID)
{
    $CILprojects['totalProjects'] = DB::table('projects')
        ->select(count('projects.projectName'))
        ->leftjoin('departments', 'departments.departmentsID', '=', 'projects.departmentsID') 
        ->leftjoin('departments_tags', 'departments_tags.departmentsID', '=', 'departments.departmentsID')
        ->leftjoin('tags', 'tags.tagID', '=', 'departments_tags.tagID')
        ->where('tags.tagID',$tagID)->count();
    
    $CILprojects['includeCILProjects'] = DB::table('projects')
        ->select(count('projects.projectName'))
        ->leftjoin('departments', 'departments.departmentsID', '=', 'projects.departmentsID') 
        ->leftjoin('departments_tags', 'departments_tags.departmentsID', '=', 'departments.departmentsID')
        ->leftjoin('tags', 'tags.tagID', '=', 'departments_tags.tagID')
        ->where('tags.tagID',$tagID)
        ->where('projects.includeCIL','Y')
        ->count();
        
    return $CILprojects;
}
function convertToCarbon($date)
{
    if($date != '1970-01-01' && $date != "")
    {
        return \Carbon\Carbon::parse($date)->format('d-m-Y');    
    }
    return "";
    
}
function checkOldProject($date)
{
    $oldDate = Carbon::create(2016, 06, 30, 00, 00, 00);

    if($oldDate->gt(Carbon::createFromFormat('Y-m-d H:i:s', $date)))
    {
        return "#F2DEDE";
    }
    else
    {
        return "#000000";
    }
}
function formatValue($value)
{
    if($value <= 0)
    {
        return '';
    }

    return $value;
}
?>