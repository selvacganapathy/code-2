<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('auditID');
            $table->integer('userID')->references('userID')->on('users');;
            $table->integer('projectsID')->unsigned();
            $table->foreign('projectsID')->references('projectsID')->on('projects')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('action',['CREATE','UPDATE','DELETE']);
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit');
    }
}
