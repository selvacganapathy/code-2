<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectaccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('projectaccess', function (Blueprint $table) 
         {
            $table->engine = 'InnoDB';
            $table->increments('accessID');
            $table->integer('usersID');
            $table->integer('projectsID');
            $table->string('userType');
            $table->timestamps();

         });
    }     
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('projectaccess');
    }
}
