<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('departments_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('departmentTagID');
            $table->integer('departmentsID');
            $table->integer('tagID');
            $table->foreign('departmentsID')->references('departmentsID')->on('departments')->onDelete('cascade');            
            $table->foreign('tagID')->references('tagID')->on('tags')->onDelete('cascade');                        
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
