<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('projectsID');
            $table->string('projectRef');
            $table->string('projectNE');
            $table->string('projectNO');
            $table->enum('projectShow',['Y','N']);
            $table->integer('systemID');
            $table->string('projectName');
            $table->integer('prioritiesID');
            $table->integer('projectStatusID');
            $table->integer('suppliersID');
            $table->integer('departmentsID');
            $table->integer('usersID');
            $table->integer('projectManagerID');
            $table->integer('readerID');
            $table->enum('projectStage',['DISCOVER','PLAN','DO','IMPLEMENT','CLOSE']);
            $table->string('projectMood');
            $table->date('planDISCOVERStartDate');
            $table->date('actualDISCOVERStartDate');
            $table->date('planStartDate');
            $table->date('actualStartDate');
            $table->date('planDOStartDate');
            $table->date('actualDOStartDate');
            $table->date('planIMPLEMENTStartDate');
            $table->date('actualIMPLEMENTStartDate');
            $table->date('planCOMPLETEStartDate');
            $table->date('actualCOMPLETEStartDate');
            $table->integer('planDISCOVERDays');
            $table->integer('actualDISCOVERDays');
            $table->integer('planDays');
            $table->integer('actualDays');
            $table->integer('planDODays');
            $table->integer('actualDODays');
            $table->integer('planIMPLEMENTDays');
            $table->integer('actualIMPLEMENTDays');
            $table->integer('planCOMPLETEDays');
            $table->integer('actualCOMPLETEDays');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
