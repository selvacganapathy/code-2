<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        for($i=1;$i<4;$i++)
        {
            DB::table('projects')->insert([
            'projectsID' => $i,
            'projectNE' => str_random(10),
            'projectNO' => str_random(10),
            'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            // 'email' => str_random(10).'@gmail.com',
            // 'password' => bcrypt('secret'),
            ]);

            DB::table('audit')->insert([
            // 'userID' => str_random(10),
            'projectsID' =>  $i,
            'action' => 'CREATE',
            'description' => 'Project',
            ]);
        }

        Model::reguard();
    }
}
