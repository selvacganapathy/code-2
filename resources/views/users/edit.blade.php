@extends('includes.defaults')


@section('content')
{{ dd($user) }}
<div class="row">
  <div class="col-xs-6">
    <div class="panel-body subheading">Edit Details</div>
      <div class="well">
             {!! Form::model($user,array('action' => 'userController@store', $user->id)) !!}
              <div class="form-group">
                {!! Form::label('name','username') !!}      
                {!! Form::text('username', null , ['class' => 'form-control'] ) !!}     
                </div>
          		<div class="form-group">
          			{!! Form::label('name','firstName') !!}			
          			{!! Form::text('firstName', null , ['class' => 'form-control'] ) !!}			
          		</div>
          		<div class="form-group">
          			{!! Form::label('name','lastName') !!}			
          			{!! Form::text('lastName', null , ['class' => 'form-control'] ) !!}			
          		</div>
          		<div class="form-group">
          			{!! Form::label('name','email') !!}			
          			{!! Form::text('email', null , ['class' => 'form-control'] ) !!}			
          		</div>
                  <div class="form-group">
                {!! Form::label('name','userType') !!}      
                {!! Form::text('userType', null , ['class' => 'form-control'] ) !!}     
              	</div>
              	<div class="form-group">
                {!! Form::label('name','userGroup') !!}      
                {!! Form::text('userGroup', null , ['class' => 'form-control'] ) !!}     
              	</div>

        		<div class="form-group">
              		{!! Form::submit('Update User',['class' => 'btn btn-primary form-control']) !!}
              {!! Form::close() !!}
    			</div>
      </div>
    </div>
  </div>
</div>

@stop
