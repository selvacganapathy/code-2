@extends('includes.defaults')


@section('content')

<div class="row">
  <div class="col-xs-6">
    <div class="panel-body subheading">User Details</div>
      <div class="well">
             {!! Form::open(array('action' => 'userController@store', 'class' => 'form')) !!}
              <div class="form-group">
                {!! Form::label('name','username') !!}      
                {!! Form::text('username', null , ['class' => 'form-control'] ) !!}     
                </div>
          		<div class="form-group">
          			{!! Form::label('name','firstName') !!}			
          			{!! Form::text('firstName', null , ['class' => 'form-control'] ) !!}			
          		</div>
          		<div class="form-group">
          			{!! Form::label('name','lastName') !!}			
          			{!! Form::text('lastName', null , ['class' => 'form-control'] ) !!}			
          		</div>
          		<div class="form-group">
          			{!! Form::label('name','email') !!}			
          			{!! Form::text('email', null , ['class' => 'form-control'] ) !!}			
          		</div>
           <!--    <div class="form-group">
                {!! Form::label('name','Password') !!}      
                {!! Form::password('password', null , ['class' => 'form-control'] ) !!}     
                </div> -->
                 <div class="form-group">
                {!! Form::label('name','User Type: * ') !!}      <br>
                {!! Form::select('userType', ['SYSTEMADMIN'=>'SYSTEMADMIN',
                 'PROJECTADMIN'=>'PROJECTADMIN', 'PROJECTMANAGER'=>'PROJECTMANAGER','READER'=>'READER'],null) !!}
              </div>
                  <!-- <div class="form-group">
                {!! Form::label('name','userType') !!}      
                {!! Form::text('userType', null , ['class' => 'form-control'] ) !!}     
              	</div> -->
                <div class="form-group">
                {!! Form::label('name','User Group: * ') !!}   <br>   
                {!! Form::select('userGroup', ['BUILD'=>'BUILD',
                 'RUN'=>'RUN'],null) !!}
              </div>
              	<!-- <div class="form-group">
                {!! Form::label('name','userGroup') !!}      
                {!! Form::text('userGroup', null , ['class' => 'form-control'] ) !!}     
              	</div> -->

        		<div class="form-group">
              		{!! Form::submit('Add User',['class' => 'btn btn-primary form-control']) !!}
    			</div>
      </div>
    </div>
  </div>
</div>

@stop
