@extends('includes.defaults')

@section('content')

<h2>User List</h2>
<div class="table-responsive" id="no-more-tables">
	
<table class="table table-bordered sort_table">
    <thead class="cf">
      <tr>
      	<th>User no</th>
      	<th>Name</th>
        <th>User name</th>
        <th>Email</th>
        <th>User Type</th>
        <th>User Group</th>
        <!-- <th><span class="glyphicon glyphicon-cog"></span></th> -->
 
      </tr>
    </thead>
    <tbody>
    	@foreach($users as $listusers)
    	<tr>
      		<td>{{ $listusers->usersID }}</td>
      		<td>{{ $listusers->firstName }}  {{ $listusers->lastName }}  </td>
      		<td>{{ $listusers->username }}  </td>
      		<td>{{ $listusers->email }}  </td>
      		<td>{{ $listusers->userType }}  </td>
      		<td>{{ $listusers->userGroup }}  </td>
<!--       		<td> <a href="users/{{ $listusers->usersID }}/edit"><span class="glyphicon glyphicon-pencil"></span></a>   |    
          <span class="glyphicon glyphicon-minus-sign"> -->
          </span></td>

      	</tr>
    	@endforeach
    </tbody>
  </table>
</div>
@stop
