
@extends('app')


@section('content')

	{!! Form::open(['url' => 'admin']) !!}
		<div class="form-group">
			{!! Form::label('name','Project Status Name: ') !!}			
			{!! Form::text('name', null , ['class' => 'form-control'] ) !!}			
		</div>

		<div class="form-group">
			{!! Form::label('description','Project Status description: ') !!}			
			{!! Form::textarea('description', null , ['class' => 'form-control'] ) !!}			
		</div>

		<div class="form-group">
		{!! Form::submit('Add',['class' => 'btn btn-primary form-control']) !!}
		</div>
	{!! Form::close() !!}

@stop
