@extends('includes.admin')


@section('content')
<h2>Project Status List</h2>

<table class="table">
    <thead>
      <tr>
      	<th>Sl.No</th>
        <th>Project Status</th>
        <th>Description</th>
        <th>Created at</th>
        <th>Updated at</th>
        <th><span class="glyphicon glyphicon-cog"></span></th>

      </tr>
    </thead>
    <tbody>
    	@foreach($projectStatus as $projectStatuses)
      <tr>
      		<td>{{ $projectStatuses->projectStatusID }}</td>
      		<td>{{ $projectStatuses->name }}</td>
      		<td>{{ $projectStatuses->description }}</td>
      		<td>{{ $projectStatuses->created_at }}</td>
      		<td>{{ $projectStatuses->updated_at }}</td>
      		<td>
      			{!! HTML::linkRoute('delete_projectstatus','Delete ',array($projectStatuses->projectStatusID)) !!}</td>

      </tr>
      	@endforeach  
    </tbody>
  </table>


@stop

