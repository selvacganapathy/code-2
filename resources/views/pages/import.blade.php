@extends('includes.defaults')


@section('content')


<h4 class="subtitle">Upload project</h4> 
{!! Form::open(array('action' => 'importController@store', 'class' => 'form','files' => true)) !!}
{!! Form::label('file','File',array('id'=>'','class'=>'')) !!}
  
{!! Form::file('file','',array('id'=>'','class'=>'')) !!}  
{!! Form::submit('add') !!}
{!! Form::close() !!}
@stop
