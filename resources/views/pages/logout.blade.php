@extends('includes.defaults')

@section('content')
<div id="fullpage" class="content-page">
			<div class="content downloads download-block">
				<div class="title-container">
                <br><b class="paragraph">LOG OUT</b>
    		</div>	
			<hr>
			<p class="paragraph">
        	You have been logged out from the system<br>
			To log in again, follow the steps below:<br><br>
		    If you came from a portal:<br>
		        <li>Go back to the portal and then refresh the page.</li>
		        <li>Click the link to this system again.</li><br><br>
			If you used a bookmark in your browser:<br>
		        <li>Click the link to this system again.</li><br><br>
			If you have any issue, please contact your helpdesk (contact details below)<br><br>
			<div class="">
			<b>Citroen dealer helpdesk - 0845 6030 638<br>
			Internal staff help line - 3636 </b>
			</div>
			<hr>
		</p>
 		</div>
</div>
@stop