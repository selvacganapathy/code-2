@extends('includes.defaults')

@section('content')
<div id="fullpage" class="content-page">
			<div class="content downloads download-block">
				<div class="title-container">
                <br><b class="paragraph">Single Sign On Failure </b>
    		</div>	
			<hr>
			<p class="paragraph">
        	This system was unable to log you in automatically using Single Sign On:
            	<b>@if ( Session::has('flash_message') )
      				{{ Session::get('flash_message') }}
				@endif</b><br>
			To resolve the problem, follow the steps below:<br><br>
		    If you came from a portal:<br>
		        <li>Go back to the portal and then refresh the page.</li>
		        <li>Click the link to this system again.</li><br><br>
			If you used a bookmark in your browser:<br>
		        <li>Click the link to this system again.</li><br><br>
			If the issue persists, please contact your helpdesk (contact details below)<br><br>
			<div class="">
			<b>Citroen dealer helpdesk - 0845 6030 638<br>
			Internal staff help line - 3636 </b>
			</div>
			<hr>

		</p>
 		</div>
</div>
@stop