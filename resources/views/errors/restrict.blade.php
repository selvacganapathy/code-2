@extends('includes.defaults')

@section('content')
<div id="fullpage" class="content-page">
			<div class="content downloads download-block">
				<div class="title-container">
                <br><b class="paragraph">Restricted Access </b>
    		</div>	
			<hr>
			<p class="paragraph">
        	This system was unable to let you to view the page:
            	<b>@if ( Session::has('flash_message') )
      				{{ Session::get('flash_message') }}
				@endif</b><br>
			If the issue persists, please contact your helpdesk (contact details below)<br><br>
			<div class="">
			<b>Citroen dealer helpdesk - 0845 6030 638<br>
			Internal staff help line - 3636 </b>
			</div>
			<hr>

		</p>
 		</div>
</div>
@stop