@extends('includes.defaults')

@section('content')
<div id="fullpage" class="content-page">
            <div class="content downloads download-block">
                <div class="title-container">
                <br><b class="paragraph">Maintenance Mode </b>
            </div>  
            <hr>
            <p class="paragraph">
                    Project Controller is currently undergoing maintenance. Please try again later
            </p>
            </div>
            <hr>
        </p>
        </div>
</div>
@stop