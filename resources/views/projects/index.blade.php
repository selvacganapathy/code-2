@extends('includes.defaults')

@section('content')
<br>
@if(isset($usersType))
    @foreach( $usersType as $type )
        <input type="checkbox" class='default-checkbox'> <span>{{ $type->type }}</span> &nbsp; 
    @endforeach
@endif
<div class="table-responsive" id="no-more-tables">
    @foreach($projects as $allprojectskey => $allprojects)
      @if($allprojectskey == 'allprojects')
          <div class="panel-body subheading"> <a onclick="toggler('allprojects');">{{ $allprojectskey }}(+/-)</a></div>
      @else
          <div class="panel-body subheading">{{ $allprojectskey }}</div>
      @endif
<div id="{{ $allprojectskey }}">
@if($allprojectskey !== 'allprojects')
<div class="input-group"> <span class="input-group-addon">Filter</span>

    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
      @endif
  <table class="table table-bordered " >
    <thead class="cf">
      <tr>
        <th rowspan="2">Priority</th>
        <th rowspan="2">Mood</th>
      	<th rowspan="2">Project No</th>
        <th rowspan="2">Project Name</th>
        <th colspan="2">Total Days</th>
        <th colspan="2">Project stage</th>
        <th colspan="3">Project <a data-toggle="tooltip" data-placement="bottom" title="
        @foreach(getAllUsersNames() as $key=> $value)
          {{ $value->NickName }} -  {{ $value->firstname }} {{ $value->lastname }} <br/>
        @endforeach
        "><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a></th> 
        <th rowspan="2">comment @if(Session::get('projects.permission') > 200)| Archive @endif</th>        
        @if(Session::get('projects.permission') > 200)<th rowspan="2">CIL</th>@endif
      </tr>
      <tr>
      <td bgcolor="">Planned</td>
      <td> Actual </td>
      <td>Plan</td>
      <td>Actual</td>
      <td>Status</td>
      <td>Manager(s) </td>
      <td>Resource(s)</td>
     </tr>    
</thead>
    <tbody class="searchable">
      @foreach($allprojects as $projectlist)
      <tr bgcolor="{{ $projectlist->tableRowColor }}">
          @if($projectlist->priorityname == 'T0')
            <td bgcolor="brown" >{{ $projectlist->priorityname }}</td>
          @elseif($projectlist->priorityname == 'T1' || $projectlist->priorityname == 'T2' || $projectlist->priorityname == 'T3' )
            <td bgcolor="#3875D7" >{{ $projectlist->priorityname }}</td>
          @elseif($projectlist->priorityname == 'PO' || $projectlist->priorityname == 'P1' ||$projectlist->priorityname == 'P2' || $projectlist->priorityname == 'P3')
            <td bgcolor="yellow" >{{ $projectlist->priorityname }}</td>
            @else
            <td bgcolor="gray" >ND</td>
          @endif
          @if($projectlist->projectMood == 1)
            <td>{!! HTML::image("images/happy.png", "Logo",array('class' => 'projectmood')) !!}</td>
          @elseif($projectlist->projectMood == 2)
            <td>{!! HTML::image("images/ambient.png", "Logo",array('class' => 'projectmood')) !!}</td>
          @else 
            <td>{!! HTML::image("images/sad.png", "Logo",array('class' => 'projectmood')) !!}</td>
          @endif
          <td>{{ $projectlist->projectRef }}</td>
          @if($allprojectskey == 'allprojects')
          <td><a href="projects/{{ $projectlist->projectsID }}">{{ $projectlist->projectName }}</a></td>
          @else
          <td><a href="projects/{{ $projectlist->projectsID }}">{{ $projectlist->projectName }}</a></td>
          @endif
          
          <td class="block">{{ $projectlist->totalPlanDays }} <!-- ({{ $projectlist->planCompletePercentage }} % ) --></td>
          <td class="block">{{ $projectlist->totalActualDays }} <!-- ({{ $projectlist->actualCompletePercentage }} %) --></td>
          <td style="background:{{Config::get('const.'.$projectlist->currentPlanStage.'')}}">{{ $projectlist->currentPlanStage }}
          @if($projectlist->currentPlanStage == "PM-D" || $projectlist->currentPlanStage == "PM-Y" || $projectlist->currentPlanStage == "NI-S")
            <a data-toggle="tooltip" data-placement="bottom" title="{{Config::get('const.'.$projectlist->currentPlanStage.'-D')}}"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a>
          @endif
          </td>
          <td style="background:{{Config::get('const.'.$projectlist->projectStage.'')}}">{{ $projectlist->projectStage }}           
            @if($projectlist->projectStage == "NOT DEFINED")
            <a data-toggle="tooltip" data-placement="bottom" title="{{Config::get('const.'.$projectlist->projectStage.'-D')}}"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a>
          @endif</td>
          <td style="background:{{ $projectlist->colorCode }}">{{ $projectlist->projectstatusname }}</td>
          <td>@foreach($projectlist->pmlist as $pmlist)
            {{ $pmlist->NickName }}
          @endforeach
          </td>
          <td>@foreach($projectlist->rdlist as $rdlist)
            {{ $rdlist->NickName }} 
          @endforeach
          </td>
          <td><!-- Button trigger modal -->
      <a class="btn-link" data-id="{{ $projectlist->projectsID }}" data-toggle="modal" data-target="#myModal" data-comments="{{ json_encode(getLatestComments($projectlist->projectsID)) }}"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a> | 
      @if(Session::get('projects.permission') > 200)
      {!! Form::open(array('url' =>array('/archive',$projectlist->projectsID),'id' => 'formfield')) !!}
    <!--   <button data-toggle="modal" data-target="#confirm-submit" class="btn-link" href="/archive/{{ $projectlist->projectsID }}">  <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span></button> -->
      <input type="button" name="btn" value="Archive" id="submitBtn" data-toggle="modal" data-target="#confirm-submit" class="btn-link" />
  {!! Form::close() !!}

      @endif
      @if(Session::get('projects.permission') > 200)
      <td>
      <form>
      <input id="projectID" name="projectID" type="hidden" value="{{ $projectlist->projectsID }}">
      <input type="checkbox"  name="includeCIL"  id="includeCIL_{{ $projectlist->projectsID }}" <?php echo (@$projectlist->includeCIL == "Y") ? 'checked' : ''  ?> onclick="check(this,{{ $projectlist->projectsID }})"> 
      </form>
      </td>
      @endif
      </tr>
      	@endforeach  
    </tbody>
  </table>
</div>
  @endforeach
</div>
@stop
