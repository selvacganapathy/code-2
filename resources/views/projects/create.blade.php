@extends('includes.defaults')

@section('content')
      
    <div class="row">
      <div class="col-xs-6">
        <div class="panel-body subheading">Project Details</div>
          <div class="well">
              {!! Form::open(array('action' => 'projectsController@store', 'class' => 'form')) !!}
          		  @include('projects.form',['submitbutton' => 'Add Project'])
              {!! Form::close() !!}
@stop

