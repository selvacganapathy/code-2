@extends('includes.defaults')

@section('content')
    <div class="row">
      <div class="col-xs-6">
        <div class="panel-body subheading">Project Details</div>
          <div class="well"  >
<!--             {!! Form::model($dropdowns['projectInfo'],['method' => 'PATCH','action' => ['projectsController@update',Session::get('users.usersID')]]) !!}
 -->
<!--              {!! Form::model($dropdowns['projectInfo'][0],['method' => 'PATCH', 'action' => ['projectsController@update']]) !!}
 -->
    {!! Form::open(array('action' =>array('projectsController@update', $dropdowns['projectInfo'][0]->projectsID))) !!}

                 @include('projects.form',['submitbutton' => 'update Project'])
            {!! Form::close() !!}
	<!-- @if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif -->
@stop

