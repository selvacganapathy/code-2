@extends('includes.defaults')

@section('content')
<br>
    @foreach( $projectTables['projectInfo']  as $projectTable['projectInfo'])
    @if(Session::get('projects.permission') > 100)
    @endif
<div class="row" style="margin-left: 0px; margin-right: 0px;">    
<div class="panel panel-primary"> 
  <div class="panel-heading"  style="background-color:{{ checkOldProject($projectTable['projectInfo']->created_at) }} !important">{{ $projectTable['projectInfo']->projectName }} 
     @if($projectTable['projectInfo']->projectMood == 1)
        {!! HTML::image("images/happy.png", "Logo",array('class' => 'panel-body mood')) !!}
      @elseif($projectTable['projectInfo']->projectMood == 2)
        {!! HTML::image("images/ambient.png", "Logo",array('class' => 'panel-body mood')) !!}
      @else 
        {!! HTML::image("images/sad.png", "Logo",array('class' => 'panel-body mood')) !!}
      @endif
      <div align="panel-body edit"><a href="/projects/{{ $projectTable['projectInfo']->projectsID }}/edit" >EDIT</a></div>
  </div>
<div class="row">
  <div class="col-md-6">
    <div class="panel-body subheading">Project Details</div>
      <dl class="dl-horizontal">
        <dt>Project ID</dt><dd>{{  $projectTable['projectInfo']->projectRef }}</dd>
        <dt>Project NE</dt><dd>{{  $projectTable['projectInfo']->projectNE }}</dd>
        <dt>Project NO</dt><dd>{{  $projectTable['projectInfo']->projectNO }}</dd>
        <dt>System Id</dt><dd>{{  $projectTable['projectInfo']->systemID }}</dd>
        <dt>Project Stage</dt><dd>{{  $projectTable['projectInfo']->projectStage }}</dd>
        <dt>Prioity</dt><dd>{{  $projectTable['projectInfo']->priorityname }}</dd>

      </dl>
  </div>
  <div class="col-md-6">
    <div class="panel-body subheading">Project Information</div>
       <dl class="dl-horizontal">
        <dt>Project Manager(s)</dt><dd>  
        @foreach($projectTable['projectInfo']->pmlist as $pmlist)
            {{ $pmlist->NickName }} 
          @endforeach </dd>
        <dt>Resource(s)</dt><dd>
        @foreach($projectTable['projectInfo']->rdlist as $rdlist)
            {{ $rdlist->NickName }} 
          @endforeach</dd>
        <dt>Project Status</dt><dd>{{  $projectTable['projectInfo']->projectstatusname }}</dd>
        <dt>Department</dt><dd>{{  $projectTable['projectInfo']->departmentsname }}</dd>
        <dt>Supplier</dt><dd>{{  $projectTable['projectInfo']->supplier_user_name }}, {{  $projectTable['projectInfo']->suppliername }}</dd>
      </dl>
  </div>
</div>
  <div class="container">
     <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th colspan="2" >plan</th>
                <th colspan="2">actual</th>
            </tr>
            <tr>
                <th><b>phase</b></th>
                <th class="plancolumn">start date</th>
                <th class="plancolumn">Duration</th>
                <th class="actualColumn">start date</th>
                <th class="actualColumn">Duration</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>DISCOVER</td>
                <td class="plancolumn">{{ convertToCarbon($projectTable['projectInfo']->planDISCOVERStartDate) }}</td>
                <td class="plancolumn">{{  $projectTable['projectInfo']->planDISCOVERDays }}</td>
                <td class="actualColumn">{!! convertToCarbon($projectTable['projectInfo']->actualDISCOVERStartDate) !!}</td>
                <td class="actualColumn">{{  $projectTable['projectInfo']->actualDISCOVERDays }}</td>
            </tr>
            <tr>
                <td>PLAN</td>
                <td class="plancolumn">{{  convertToCarbon($projectTable['projectInfo']->planStartDate) }}</td>
                <td class="plancolumn">{{  $projectTable['projectInfo']->planDays }}</td>
                <td class="actualColumn">{!! convertToCarbon($projectTable['projectInfo']->actualStartDate) !!}</td>               
                <td class="actualColumn">{{  $projectTable['projectInfo']->actualDays }}</td>
            </tr>
            <tr>
                <td>DO</td>
                <td class="plancolumn">{{  convertToCarbon($projectTable['projectInfo']->planDOStartDate) }}</td>
                <td class="plancolumn">{{  $projectTable['projectInfo']->planDODays }}</td>
                <td class="actualColumn">{!! convertToCarbon($projectTable['projectInfo']->actualDOStartDate) !!}</td>
                <td class="actualColumn">{{  $projectTable['projectInfo']->actualDODays }}</td>
            </tr>
              <tr>
                <td>IMPLEMENT</td>
                <td class="plancolumn">{{  convertToCarbon($projectTable['projectInfo']->planIMPLEMENTStartDate) }}</td>
                <td class="plancolumn">{{  $projectTable['projectInfo']->planIMPLEMENTDays }}</td>
                <td class="actualColumn">{!! convertToCarbon($projectTable['projectInfo']->actualIMPLEMENTStartDate) !!}</td>
                <td class="actualColumn">{{  $projectTable['projectInfo']->actualIMPLEMENTDays }}</td>
            </tr>
            <tr>
                <td>COMPLETE</td>
                <td class="plancolumn">{{  convertToCarbon($projectTable['projectInfo']->planCOMPLETEStartDate) }}</td>
                <td class="plancolumn">{{  $projectTable['projectInfo']->planCOMPLETEDays }}</td>
                <td class="actualColumn">{{  convertToCarbon($projectTable['projectInfo']->actualCOMPLETEStartDate) }}</td>
                <td class="actualColumn">{{  $projectTable['projectInfo']->actualCOMPLETEDays }}</td>
            </tr>
        </tbody>
    </table>    
  </div>
  <div class="panel-body subheading">LATEST COMMENTS (3)</div>
  <div class="list-group-item">
    @foreach(array_slice($projectTables['commentTable'],0,3)  as $comment)
      <p class="list-group-item-heading"><b style="text-transform:uppercase">{{ $comment->firstName }} {{  $comment->lastName }}</b><span class="span3 pull-right">{{  Carbon\Carbon::parse($comment->created_at)->format('jS \\of F Y h:i A') }}</span>
      </P>
      <p class="list-group-item-text">{{ $comment->comments }}</p>
      <hr>
    @endforeach
  </div>
  @if(!array_slice($projectTables['commentTable'],3,count($projectTables['commentTable'])) <3 ) 
  <div class="panel-body subheading"> <a onclick="toggler('list-group');">ALL COMMENTS (+/-)</a></div>
  @endif
  <div class="list-group" id="list-group">
  <div class="list-group-item">
    @foreach(array_slice($projectTables['commentTable'],3,count($projectTables['commentTable']))  as $commentsList)
      <p class="list-group-item-heading"><b style="text-transform:uppercase">{{ $commentsList->firstName }} {{  $commentsList->lastName }}</b><span class="span3 pull-right">{{  Carbon\Carbon::parse($commentsList->created_at)->diffForHumans() }}</span>
      </P>
      <p class="list-group-item-text">{{ $commentsList->comments }}</p>
      <hr>
    @endforeach  
  </div>
  </div>


  <div class="panel-body subheading">ADD COMMENT</div>
    <div id="app">
    {!! Form::open(array('action' =>array('projectsController@createcomment', $projectTable['projectInfo']->projectsID ))) !!}
      {!! Form::textarea('comments', null , ['class' => 'form-control custom-control', 'rows' => 2, 'cols' => 40,"v-model"=>"comments",'required'] ) !!}      
      {!! Form::hidden('invisible', Session::get('user.usersID') , array('name' => 'userID')) !!}
      {!! Form::hidden('invisible', $projectTable['projectInfo']->projectsID , array('name' => 'projectsID')) !!}
      <span class="error" v-show="!comments">Please enter comment to submit* </span>
    {!! Form::submit('Submit',['class' => 'btn btn-primary form-control','v-show'=>'comments']) !!}
    {!! Form::close() !!}
    </div>
</div>
</div>
</div>
@endforeach
@stop
