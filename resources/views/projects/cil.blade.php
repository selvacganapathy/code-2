@extends('includes.defaults')

@section('content')
<br>

<table class="table table-bordered sort_table" style="width:50% !important;">

    <thead class="cf">
        <th >Tag No</th>
        <th >Tag Name</th>
        <th >Total Projects</th>
        <th >CIL Checked</th>
        <th >Report</th>
    </thead>
    <tbody>
      @foreach($tags as $taglist)

          <td>{{ $taglist->tagID }}</td>
          <td>{{ $taglist->tagName }}</td>
          <td>{{ CILprojects($taglist->tagID)['totalProjects'] }}</td>
          <td>{{ CILprojects($taglist->tagID)['includeCILProjects'] }}</td>
          @if(CILprojects($taglist->tagID)['includeCILProjects'] >0)
          <td><a href="/downloadCIL/{{ $taglist->tagName }}/{{ $taglist->tagID }}">Download</a></td>
          @else
          <td><a href="#" onClick="alert('No CIL available for  {{ $taglist->tagName }}!')" >Download</a></td>
          @endif
      </tr>
        @endforeach  
    </tbody>
  </table> 
</div>
@stop
