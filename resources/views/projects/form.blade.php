
@if(!isset($disable))
<?php  $disable = 'false' ?>
@endif
@if(isset($dropdowns['nextID']))
    <?php $nextID = $dropdowns['nextID']; ?>
@else
  <?php $nextID = $dropdowns['projectInfo'][0]->projectRef; ?>
@endif
              <div class="form-group">
                {!! Form::label('name','Project ID:  *') !!}      
                {!! Form::text('projectRef', $nextID, ['class' => 'form-control','required',$disable,'value'=>old('projectRef')] ) !!}      
                <p class="errors form">{{$errors->first('projectRef')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Project NE:  ') !!}      
                {!! Form::text('projectNE', null , ['class' => 'form-control',$disable,'required'] ) !!}      
                <p class="errors form">{{$errors->first('projectNE')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Project NO: ') !!}      
                {!! Form::text('projectNO', null , ['class' => 'form-control', $disable,'required'] ) !!}      
                <p class="errors form">{{$errors->first('projectNO')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','System ID:') !!}     
                {!! Form::text('systemID', null , ['class' => 'form-control', $disable,'required'] ) !!}     
                <p class="errors form">{{$errors->first('systemID')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Project Name: * ') !!}      
                {!! Form::text('projectName', null , ['class' => 'form-control', $disable,'required'] ) !!}      
                <p class="errors form">{{$errors->first('projectName')}}</p>
              </div>
          </div>
      </div>
      <div class="col-xs-6">
         <div class="panel-body subheading">Project Information</div>
          <div class="well">
              <div class="form-group">
                {!! Form::label('name','Project Mood: * ') !!}      
                {!! Form::select('projectMood', $dropdowns['projectmood']) !!}
                <p class="errors form">{{$errors->first('projectMood')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Project Manager: * ') !!}      
                <select multiple="multiple" name="projectManagerID[]" class="custom-scroll" required>
                  @foreach($dropdowns['projectmanager'] as $pmKey => $pmValue)
                  <option value="{{$pmKey}}" @if(isset($dropdowns['pmSelected'])) @foreach($dropdowns['pmSelected'] as $p) @if($pmKey == $p)selected="selected"@endif  @endforeach @endif>{{$pmValue}}</option>
                  @endforeach
                </select>
                <p class="errors form">{{$errors->first('projectManagerID')}}</p>
              </div>
              
              <div class="form-group">
                {!! Form::label('name','Select Priority: * ') !!}      
                {!! Form::select('prioritiesID', $dropdowns['priorities']) !!}
                <p class="errors form">{{$errors->first('prioritiesID')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Project Status: * ') !!}      
                {!! Form::select('projectStatusID', $dropdowns['projectStatus']) !!}    
                <p class="errors form">{{$errors->first('projectStatusID')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Departments: * ') !!}      
                {!! Form::select('departmentsID', $dropdowns['departments']) !!}     
                <p class="errors form">{{$errors->first('departmentsID')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Select Supplier: * ') !!}      
                {!! Form::select('suppliersID', $dropdowns['suppliers']) !!}     
                <p class="errors form">{{$errors->first('suppliersID')}}</p>
              </div>
              <div class="form-group">
                {!! Form::label('name','Allocate To: * ') !!}                      
                <select multiple="multiple" name="readerID[]" class="custom-scroll" required>
                  @foreach($dropdowns['reader'] as $readerKey => $readerValue)
                  <option value="{{$readerKey}}" @if(isset($dropdowns['readerSelected'])) @foreach($dropdowns['readerSelected'] as $p) @if($readerKey == $p)selected="selected"@endif  @endforeach @endif>{{$readerValue}}</option>
                  @endforeach
                </select>

                <p class="errors form">{{$errors->first('readerID')}}</p>
              </div>
              @if(Route::currentRouteName() == 'editproject' || Route::currentRouteName() == 'updateproject')
                <div class="form-group">
                {!! Form::label('name','Project Stage: * ') !!}      
                {!! Form::select('projectStage', ['DISCOVER'=>'DISCOVER',
                 'PLAN'=>'PLAN', 'DO'=>'DO','IMPLEMENT'=>'IMPLEMENT','CLOSE'=>'CLOSE'],$dropdowns['projectInfo'][0]->projectStage) !!}
              </div>
              @endif
          </div>
        </div>
      </div>
      <div class="container">
        <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th colspan="2" >plan</th>
                <th colspan="2">actual</th>
            </tr>
            <tr>
                <th><b>phase</b></th>
                <th class="plancolumn">start date</th>
                <th class="plancolumn">start days</th>
                <th class="actualColumn">start date</th>
                <th class="actualColumn">start days</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>DISCOVER</td>
                <td class="plancolumn">{!! Form::text('planDISCOVERStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'planDISCOVERStartDate','required']) !!}
                <p class="errors form">{{$errors->first('planDISCOVERStartDate')}}</p></td>
                <td class="plancolumn">{!! Form::text('planDISCOVERDays', null , ['class' => 'form-control',$disable,'required']) !!}
                <p class="errors form">{{$errors->first('planDISCOVERDays')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualDISCOVERStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'actualDISCOVERStartDate']) !!}
                <p class="errors form">{{$errors->first('actualDISCOVERStartDate')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualDISCOVERDays', null , ['class' => 'form-control'],$disable) !!}
                <p class="errors form">{{$errors->first('actualDISCOVERDays')}}</p></td>
            </tr>
            <tr>
                <td>PLAN</td>
                <td class="plancolumn">{!! Form::text('planStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'planStartDate','required']) !!}
                <p class="errors form">{{$errors->first('planStartDate')}}</p></td>
                <td class="plancolumn">{!! Form::text('planDays', null , ['class' => 'form-control',$disable,'required']) !!}
                <p class="errors form">{{$errors->first('planDays')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualStartDate', null , ['class' => 'form-control make-datepicker','id' => 'actualStartDate']) !!}
                <p class="errors form">{{$errors->first('actualStartDate')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualDays', null , ['class' => 'form-control ']) !!}
                <p class="errors form">{{$errors->first('actualDays')}}</p></td>
            </tr>
            <tr>
                <td>DO</td>
                <td class="plancolumn">{!! Form::text('planDOStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'planDOStartDate','required']) !!}
                <p class="errors form">{{$errors->first('planDOStartDate')}}</p></td>
                <td class="plancolumn">{!! Form::text('planDODays', null , ['class' => 'form-control ',$disable,'required']) !!}
                <p class="errors form">{{$errors->first('planDODays')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualDOStartDate', null , ['class' => 'form-control make-datepicker']) !!}
                <p class="errors form">{{$errors->first('actualDOStartDate')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualDODays', null , ['class' => 'form-control ']) !!}
                <p class="errors form">{{$errors->first('actualDODays')}}</p></td>
            </tr>
              <tr>
                <td>IMPLEMENT</td>
                <td class="plancolumn">{!! Form::text('planIMPLEMENTStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'planIMPLEMENTStartDate','required']) !!}
                <p class="errors form">{{$errors->first('planIMPLEMENTStartDate')}}</p></td>     
                <td class="plancolumn">{!! Form::text('planIMPLEMENTDays', null , ['class' => 'form-control ',$disable,'required']) !!}
                <p class="errors form">{{$errors->first('planIMPLEMENTDays')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualIMPLEMENTStartDate', null , ['class' => 'form-control make-datepicker']) !!}
                <p class="errors form">{{$errors->first('actualIMPLEMENTStartDate')}}</p></td>
                 <td class="actualColumn">{!! Form::text('actualIMPLEMENTDays', null , ['class' => 'form-control ']) !!}
                 <p class="errors form">{{$errors->first('actualIMPLEMENTDays')}}</p></td>
            </tr>
            <tr>
                <td>COMPLETE</td>
                <td class="plancolumn">{!! Form::text('planCOMPLETEStartDate', null , ['class' => 'form-control make-datepicker',$disable,'id' => 'planCOMPLETEStartDate','required']) !!}
                <p class="errors form">{{$errors->first('planCOMPLETEStartDate')}}</p></td>
                <td class="plancolumn">{!! Form::text('planCOMPLETEDays', null , ['class' => 'form-control ',$disable,'required']) !!}
                <p class="errors form">{{$errors->first('planCOMPLETEDays')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualCOMPLETEStartDate', null , ['class' => 'form-control make-datepicker']) !!}
                <p class="errors form">{{$errors->first('actualCOMPLETEStartDate')}}</p></td>
                <td class="actualColumn">{!! Form::text('actualCOMPLETEDays', null , ['class' => 'form-control ']) !!}
                <p class="errors form">{{$errors->first('actualCOMPLETEDays')}}</p></td>
            </tr>
        </tbody>
    </table>    
    </div>
  </div>
    <div class="form-group footer">
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-danger']) !!}
              {!! Form::submit($submitbutton,['class' => 'btn btn-primary']) !!}
    </div>