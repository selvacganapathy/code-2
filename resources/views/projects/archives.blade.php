@extends('includes.defaults')

@section('content')
<br>
<h2>ITUK PERFORMANCE <h3><span >Total projects - {{$totalProjects }}</span></h3></h2>
<div class="col-sm-12">
  <div class="row">
    <div class="col-sm-6">
      @section ('table_panel_title','Project Manager')
      @section ('table_panel_body')
        <table class="table table-striped"  id="tableSort">
          <thead>
            <tr>
              <th>Name</th>
              <th title="Total projects">TP</th>
              <th title="New Enquiry">NE</th>
              <th title="On Hold">OH</th>
              <th title="Wash Up">WU</th>
              <th title="In Progress">IP</th>
              <th title="Reject/Completed">Others</th>
              <th>PER %</th>
            </tr>
          </thead>
          <tbody>
            @foreach($projectsByPM as $projects)
            <tr>
              <td>{{ $projects->nickName }}</td>
              <td>{{ formatValue($projects->myProjects) }}</td>
              <td>{{ formatValue($projects->newEnquiry) }}</td>
              <td>{{ formatValue($projects->onHold) }}</td>
              <td>{{ formatValue($projects->washUp) }}</td>
              <td>{{ formatValue($projects->inProgress) }}</td>
              <td>{{ formatValue(($projects->completed + $projects->rejected)) }}</td>
              <td>{{ number_format(($projects->myProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      @endsection
      @include('widgets.panel', array('header'=>true, 'as'=>'table'))
    </div>
    <div class="col-sm-6">
      <div class="container">
        <h3>Project Manager Performance</h>
        <graph  :labels="['On Hold','Rejected','Washup']" 
          :values="[12,24,10]">
        </graph>
      </div>
    </div>
  </div>
  <div class="row">
  <div class="col-sm-6">
      @section ('btable_panel_title','Resource')
      @section ('btable_panel_body')
        <table class="table table-striped" id="tableSort-resource">
          <thead>
            <tr>
              <th>Name</th>
              <th title="Total projects">TP</th>
              <th title="New Enquiry">NE</th>
              <th title="On Hold">OH</th>
              <th title="Wash Up">WU</th>
              <th title="In Progress">IP</th>
              <th title="Reject/Completed">Others</th>
              <th>PER %</th>
            </tr>
          </thead>
          <tbody>
            @foreach($projectsByReader as $projects)
            <tr>
              <td>{{ $projects->nickName }}</td>
              <td>{{ formatValue($projects->myProjects) }}</td>
              <td>{{ formatValue($projects->newEnquiry) }}</td>
              <td>{{ formatValue($projects->onHold) }}</td>
              <td>{{ formatValue($projects->washUp) }}</td>
              <td>{{ formatValue($projects->inProgress) }}</td>
              <td>{{ formatValue(($projects->completed + $projects->rejected)) }}</td>
              <td>{{ number_format(($projects->myProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      @endsection
      @include('widgets.panel', array('header'=>true, 'as'=>'btable'))
    </div>
    <div class="col-sm-6">
      <div class="container">
        Graph
      </div>
    </div>
  </div>
  <div class="row">
  <div class="col-sm-6">
    @section ('ctable_panel_title','Project Status')
      @section ('ctable_panel_body')
        <table class="table table-striped" id="tableSort-status">
          <thead>
            <tr>
              <th>Type</th>
              <th>Total Projects</th>
              <th>PER %</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>On hold</td>
              <td>{{ $onHoldProjects }}</td>
              <td>{{ number_format(($onHoldProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            <tr>
              <td>Rejected</td>
              <td>{{ $rejectedProjects }}</td>
              <td>{{ number_format(($rejectedProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            <tr>
              <td>Washup</td>
              <td>{{ $washupProjects }}</td>
              <td>{{ number_format(($washupProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            <tr>
              <td>New</td>
              <td>{{ $NewProjects }}</td>
              <td>{{ number_format(($NewProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            <tr>
              <td>live</td>
              <td>{{ $liveProjects }}</td>
              <td>{{ number_format(($liveProjects/$totalProjects)*100,2) }} %</td>
            </tr>
            <tr>
              <td>complete</td>
              <td>{{ $completeProjects }}</td>
              <td>{{ number_format(($completeProjects/$totalProjects)*100,2) }} %</td>
            </tr>                                                
          </tbody>
        </table>

      @endsection
      @include('widgets.panel', array('header'=>true, 'as'=>'ctable'))
    </div>
    <div class="col-sm-6">
      <div class="container">
          Graph
      </div>
    </div>
  </div>
<div class="row">
<div class="col-sm-6">
    @section ('htable_panel_title','Department')
    @section ('htable_panel_body')
      <table class="table table-striped" id="tableSort-department">
        <thead>
            <tr>
              <th>Name</th>
              <th title="Total projects">TP</th>
              <th title="New Enquiry">NE</th>
              <th title="On Hold">OH</th>
              <th title="Wash Up">WU</th>
              <th title="In Progress">IP</th>
              <th title="Reject/Completed">CO</th>
              <th title="Reject/Completed">RJ</th>
              <th>PER %</th>
            </tr>
        </thead>
        <tbody>
          @foreach($department as $projects)
          <tr>
            <td>{{ $projects->name }}</td>
            <td>{{ $projects->myProjects }}</td>
            <td>{{ formatValue($projects->newEnquiry) }}</td>
            <td>{{ formatValue($projects->onHold) }}</td>
            <td>{{ formatValue($projects->washUp) }}</td>
            <td>{{ formatValue($projects->inProgress) }}</td>
            <td>{{ formatValue(($projects->completed)) }}</td>
            <td>{{ formatValue(($projects->rejected)) }}</td>
            <td>{{ number_format(($projects->myProjects/$totalProjects)*100,2) }} %</td>            
          </tr>
          @endforeach
        </tbody>
      </table>
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'htable'))
  </div>
    <div class="col-sm-6">
      <div class="container">
        Graph
      </div>
    </div>
  </div>
<div class="row">
<div class="col-sm-6">
    @section ('atable_panel_title','Project Mood')
    @section ('atable_panel_body')
      <table class="table table-striped" id="tableSort-mood">
        <thead>
          <tr>
            <th>Name</th>
            <th>Total Projects</th>
            <th>PER %</th>
          </tr>
        </thead>
        <tbody>
          @foreach($projectMood as $projects)
          <tr>
            <td>{{ $projects->name }}</td>
            <td>{{ $projects->myProjects }}</td>
            <td>{{ number_format(($projects->myProjects/$totalProjects)*100,2) }} %</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'atable'))
  </div>
    <div class="col-sm-6">
      <div class="container">
          Graph
      </div>
    </div>
  </div>
</div>
<div class="footer navbar-fixed-bottom" style="text-align:center;">
  <h5>TP - Total Projects | NE - New Projects | OH - On Hold | IP - In Progress | WU - Wash Up | CO - completed | RJ - Rejected</h5>
</div>
@stop
