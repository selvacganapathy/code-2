@extends('includes.defaults')


@section('content')
<br>

  <div class="table-responsive" id="no-more-tables">
<h3>{{ $title }} </h3>
 <table class="table table-bordered sort_table" style="width:50% !important;">

    <thead class="cf">
        <th>Sl.No</th> 
      	<th >Project No</th>
        <th >Project Name</th>
        <!-- <th >Project Status</th> -->
        <th >Project Status</th>
    </thead>
    <tbody>
      <?php $i=1; ?>
      @foreach($getProjects as $projectlist)
        <td>{{$i}}</td>
          <td>{{ $projectlist->projectRef }}</td>
          <td><a href="/projects/{{ $projectlist->projectsID }}">{{ $projectlist->projectName }}</a></td>
          <!-- <td>{{ $projectlist->projectStatusID }}</a></td> -->
          <td><a href="projects/{{ $projectlist->projectsID }}"> MAKE LIVE</a></td>
      </tr>
      <?php $i++?>
      	@endforeach  
    </tbody>
  </table> 
</div>
@stop
