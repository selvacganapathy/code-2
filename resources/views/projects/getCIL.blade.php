@extends('includes.defaults')

@section('content')
<br>
<div id="fullpage" class="content-page">

<div class="content downloads download-block">
  {!! Form::open(['url' => 'putCIL']) !!} 
    <div class="row center">
      <h3>ADD NEW CIL</h3>
      <select name="tagID" class="selectpicker" title="TAG" multiple data-max-options="1" data-live-search="true">
        @foreach($tags as $tagsList)
          <option value="{{ $tagsList->tagID }}">{{ $tagsList->tagName }}</option>
        @endforeach
      </select>
      <select name="departmentsID[]" class="selectpicker" title="DEPARTMENT" multiple data-max-options="10" data-live-search="true">
        @foreach($nonAssignDepartments as $noassign)
          <option value="{{ $noassign->departmentsID }}">{{ $noassign->name }}</option>
        @endforeach      
      </select>
      <div class="form-group">
        {!! Form::submit('ASSIGN',['class' => 'btn btn-danger']) !!}
      </div>
    </div>
  {!! Form::close() !!}
        <h3>ADD NEW CIL</h3>

<ul class="list-group cil" >
  <li class="list-group-item"><b>DEPARTMENT ASSIGNED</b></li>
 @foreach($assignDepartments as $assign)
         <li class="list-group-item">{{ $assign->name }}</li>
        @endforeach     
</ul>
<ul class="list-group cil" >
  <li class="list-group-item"><b>DEPARTMENT NOT ASSIGNED</b></li>
  @foreach($nonAssignDepartments as $noassign)
         <li class="list-group-item">{{ $noassign->name }}</li>
        @endforeach     
</ul>
</div>
  </div>

@stop
