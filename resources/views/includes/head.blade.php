<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">

  <meta name="_token" content="{!! csrf_token() !!}"/>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- BOOTSTRAP MINIFIED CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- JQUERY CALENDAR -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <!-- BOOTSTRAP SELECT -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
  <!-- BOOTSTRAP SORT -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

   <style type="text/css">
   @font-face 
   {
    font-family: Peugeot;
    src: url('{{ asset('fonts/Peugeot.ttf') }}'); 
   }
   .important{
    border:2px solid red;
    color:red;
    background:red;
}
.important .gantt_task_progress{
    background: #ff5956;
}
.weekend{ 
    background: #f4f7f4 !important;
}
html { overflow-y: scroll; }
		body 
    {
      margin: 0;
      padding: 0;
      width: 100%;
      display: table;
      font-weight: 100;
      font-size: 12px;
      font-family:Peugeot;
    }
    hr {
      margin-bottom: 0px !important;
      margin-top: 0px !important;

    }
    .ui-widget {
    font-family:Peugeot;
    font-size: 1.0em;
}
.cf td {
    background: #1B365E none repeat scroll 0 0 !important;
    color: #ffffff;
}
.ui-widget-header {
    background: #286090 none repeat scroll 0 0;
    border: 1px solid #aaaaaa;
    color: #fff;
    font-weight: bold;
}
.content-page {
    padding-left: 20px;
}
.dropdown {
    position: relative;
    display: inline-block;
}
.panel-body.subheading {
    color: gray;
    font-weight: bold;
    text-align: center;
     text-decoration: underline;
}
.panel-body.subheading {
    font-size: 16px;
    text-transform: uppercase;
}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
}

.dropdown:hover .dropdown-content {
    display: block;
}

    /**
     * container, panel 
     **/
    .container
    {
      width: 100%;

    }
    .container
    {
      width: 100%;

    }
    .panel-heading 
    {
    font-size: 18px;
    text-transform: uppercase;
    }
    .panel-body.subheading
    {
    font-size: 16px;
    text-transform: uppercase;
    }
    .panel-body.mood 
    { 
      margin: 0;
      padding: 0;
      height: 30px;
      width: 30px;
    }
    .panel-heading > div {
        float: right;
        color: #ffffff;
    }
    a {
    color: #ec3245;
    text-decoration: none;
}
    /**
     * navigation bar 
    **/
    .navbar-left img {
    width: 100px;
}
    .navbar-brand.navbar-brand-centered > div {
    float: right;
    font-size: 22px;
    padding-top: 13px;
    }
    .navbar-brand.navbar-brand-centered {
    padding-left: 30%;
    padding-top: 2%;
    position: absolute;
}
.navbar-default .navbar-nav > li > a {
    color: #fff;
}
    .navbar-right 
    {
    font-size: 12px;
    list-style-type: none;
    }
    .navbar-right > li 
    {
    text-align: right;
    }
    nav 
    {
    margin: 0 !important;
    color: #fff;
    background: #1b365e none repeat scroll 0 0 !important;

    }
    .navbar-nav > li > a 
    {
    padding-bottom: 5px;
    padding-top: 5px;
    }
    a:hover 
    {
    color: #1B365E;
    text-decoration: none;
    }
    .navbar-nav > li > a:hover
    {
      color: #fff !important;
      text-decoration: none;
    }
    .navbar-nav > li > a:focus
    {
      color: #fff !important;
      text-decoration: none;
    }
    .collapse.navbar-collapse 
    {
    /*margin-top: 55px;*/
    margin-left: 20%;
    }
    h2 
    {
    text-transform: uppercase;
    }
    .navbar.navbar-default {
    border-bottom: 5px solid gray;
    }
    .navbar.navbar-default.navbar-fixed-bottom {
        border-top: 5px solid gray;
    }
    nav{
      padding: 10px;
    }
    /**
     * table responsive  
    **/

    .table-responsive
    {
        overflow-x: auto;
    }
    table {
    font-size: 12px;
    margin-left: auto;
    margin-right: auto;
    padding: 10px;
    width: 97% !important;

    }
    td, th {
    text-align: center;
    padding: 5px !important;
    }
    th {
      text-transform: uppercase;
    }
    @media only screen and (max-width: 800px) {
    
      /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 

    #no-more-tables tr { 
      display: block; 
    }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  #no-more-tables thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
 
  #no-more-tables tr { border: 1px solid #ccc; }
 
  #no-more-tables td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
    white-space: normal;
    text-align:left;
  }
 
  #no-more-tables td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    text-align:left;
    font-weight: bold;
  }
   
 .input-group.datetime.datepicker {
    text-align: center;
}
  /*
  Label the data
  */
  #no-more-tables td:before { content: attr(data-title); }
}
td,th {
    text-align: center;
}
/**
 * button
 **/
.btn.btn-default.add {
    background: red none repeat scroll 0 0;
    border-radius: 0;
    color: white;
}
.btn-sq {
  width: 100px !important;
  height: 100px !important;
  font-size: 10px;
  border-radius: 0;
  margin-left: 15px;
}
.miscmenus {
    /*clear: both;*/

}
label {
    width: 30%;
}
.input-group.datetime.datepicker {
    width: 100%;
}
.form-control.make-datepicker {
    text-align: center;
}
.projectname {
    color: #B7B7B7;
    font-size: 20px;
}
.navbar-left
{
  padding: 0px;
  padding-bottom: 10px;
  list-style-type: none;
  font-size: 15px;
}
.navbar-brand {
  padding: 0px;
}
.projectmood {
    height: 29px;
    width: 29px;
}
select {
    width: 30%;
}
.list-group.comment {
    display: none;
}
#allprojects   {
    display: none;
}
.form-group.footer {
    text-align: center;
}
.table.table-bordered.sort_table button {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 0 none;
    font-size: 18px !important;
    margin: 0;
    padding: 0;
}
/*.list-group {
     display:none;
}
*/
.plancolumn {
    background: rgba(144, 197, 169,0.5) none repeat scroll 0 0 !important;
    height: 40px;
    text-align: center;

}

.actualColumn {
    background: rgba(122, 154, 149,0.5) none repeat scroll 0 0 !important;
    height: 40px;
    text-align: center;
}
.ui-datepicker td {
    border: 0 none;
    padding: 1px !important;
}
.glyphicon {
    font-size: 15px;
}  
.panel-heading a {
    color: #1b365e;
}
.errors.form {
  color: red !important;
}
.panel-heading {
    background: transparent none repeat scroll 0 0 !important;
    color: #1b365e !important;
    text-transform: uppercase !important;
}
.well {
    background-color: transparent;
    border: 1px solid #337ab7;
}
.row{
  padding: 10px;
  width: 100%;
}
.ui-datepicker-month, .ui-datepicker-year {
    color: #1b365e;
    font-family: Peugeot !important;
}
.content.downloads.download-block {
    padding-bottom: 20px;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px;
    text-align: center;
}
.form-group {
    padding-bottom: 5px;
    padding-left: 5px;
    padding-right: 5px;
    padding-top: 5px;
}
.btn.dropdown-toggle.btn-info {
    border-bottom-left-radius: 0 !important;
    border-bottom-right-radius: 0 !important;
    border-top-left-radius: 0 !important;
    border-top-right-radius: 0 !important;
    background-color: #1b365e;
    border-color:  #1b365e; 
}
.btn.btn-danger {
    border-radius: 0px !important;
  }
  .list-group.cil {
    float: left;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 10px;
    width: 50%;
}
.select {
  height: 25px;
  }
  .error {
    color: red;
    float: right;
}
.input-group {
   margin-bottom: auto;
    margin-left: auto;
    margin-right: auto;
    margin-top: auto;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 10px;
    width: 20%;
    float: right;
    padding-right: 20px !important;
}
.input-group-addon {
  background: #1B365E;
  color: white;

  }
  .footer.navbar-fixed-bottom {
    background: rgba(27, 54, 94,.8);
    height: 30px;
    color: white;
    width: 70%;
    margin:  auto;
}
  </style>
 </head>
 <body>
