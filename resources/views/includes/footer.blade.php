<div id="results"></div>
<!-- bootstrap minified js -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="{{asset('js/main.js')}}"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('#tableSort').DataTable({
          "searching": false,
          "lengthChange" : false        
        });

        $('#tableSort-department').DataTable({
          "searching": false,
          "lengthChange" : false        
        });

        $('#tableSort-resource').DataTable({
          "searching": false,
          "lengthChange" : false        
        });  
    });
    </script>

<!-- FORM SUBMIT -->
<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            Confirmation
        </div>
      <div class="modal-body">
            You are about to archive this project ,are you sure?
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <a href="#" id="archive-submit" class="btn btn-success success">Yes</a>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- DATE PICKER -->
<script>
  $(function() {
    $( ".make-datepicker" ).datepicker({ changeMonth: true,changeYear: true,yearRange : '-50:+0',dateFormat: "dd-mm-yy" }).val();
  });
</script>

<!-- COMMMENT MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">LATEST COMMENTS (5)</h4>
      </div>

      <div class="modal-body">
        <div id="bookId"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
<!-- COMMENT POP UP-->
<script type="text/javascript">
  $(document).on("click", ".btn-link", function () 
  {                
    var projectsid = $(this).data('id');
    
    var comments = $(this).data('comments');

    var data = $(this).data('comments');
    
    var result = [];
    /**
     * var comments - ARRAY OF OBJECTS
     * var result -  MULTIDIMENSIONAL ARRAY
     */
    for(var i in data)
    
    result.push([i,data[i]]);
    
    var arrayLength =  comments.length;
    
    var container = document.getElementById("bookId");
    
    document.getElementById("bookId").innerHTML = "";
    
    for(i = 0; i < arrayLength; i++) 
    {
      container.innerHTML += ''+ (i+1) + '.  ';
      
      container.innerHTML += '<label class="fullname'+ i + '">'+ comments[i].firstname +' '+ comments[i].lastname +'';
      
      container.innerHTML += '</label>';   
      
      container.innerHTML += '<label class="date'+ i + '">'+ comments[i].timeAgo +'';
      
      container.innerHTML += '</label>';   
      
      container.innerHTML += '<div class="comments'+ i + '">'+ comments[i].comments +'';
      
      container.innerHTML += '</div><hr>';   
    }
  });
</script>
<!-- TABLE TOGGLE -->
<script>
  function toggler(divId) {
    $("#" + divId).toggle();
}
</script>
<!-- TOOL TIP -->
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<script type="text/javascript">
function check(cb,projectID)
{
  var projectsid = projectID;
  
  if($(cb).is(":checked"))
    {
        var $form = $(this);
        var CILChecked = 'Y';
    }
    else
    {
        var $form = $(this);
        var CILChecked = 'N';

    }
    
    var submission = { 'includeCIL' : CILChecked,'projectsID' : projectsid,'_token': $('input[name=_token]').val() }
    $.ajax({
    type: "POST",
    url: "/CIL",
    data: submission,
    success: function(data){   
            $('#results').fadeIn(250).css('color', '#000000').html('<strong>Updated</strong>').delay(2500).fadeOut(250);
            window.location.reload();
            
    },
    error: function(xhr, status, error) {

    }          
  });

}
</script>
<script type="text/javascript">
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 1
});
</script>

<script>
  $(document).ready(function () {

    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});
</script>
<script type="text/javascript">

$('#archive-submit').click(function(){
     /* when the submit button in the modal is clicked, submit the form */
    alert('Submitted');
    $('#formfield').submit();
});

</script>
</body>
</html>
