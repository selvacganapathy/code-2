<!DOCTYPE html>
<html lang="en">
  <head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- BOOTSTRAP MINIFIED CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- JQUERY CALENDAR -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <!-- BOOTSTRAP DATE PICKER -->
  <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/v4.0.0/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

   <style type="text/css">
   @font-face 
   {
    font-family: Peugeot;
    src: url('{{ asset('fonts/Peugeot.ttf') }}'); 
   }
    /**
     * container, panel 
     **/
    .container
    {
      width: 100%;

    }
    .container
    {
      width: 100%;

    }
    .panel-heading 
    {
    font-size: 18px;
    text-transform: uppercase;
    }
    .panel-body.subheading
    {
    font-size: 16px;
    text-transform: uppercase;
    }
    .panel-body.mood 
    { 
      float:right;
      margin: 0;
      padding: 0;
    }
    /**
     * navigation bar 
    **/
    .navbar-brand.navbar-brand-centered > div {
    float: right;
    font-size: 22px;
    padding-top: 13px;
    }
    .navbar-right 
    {
    font-size: 12px;
    list-style-type: none;
    }
    .navbar-right > li 
    {
    text-align: right;
    }
    nav 
    {
    margin: 0 !important;
    }
    .navbar-nav > li > a 
    {
    padding-bottom: 5px;
    padding-top: 5px;
    }
    a:hover 
    {
    color: #EC3224;
    text-decoration: none;
    }
    .navbar-nav > li > a:hover
    {
      color: #EC3224;
      text-decoration: none;
    }
    .collapse.navbar-collapse 
    {
    margin-top: 55px;
    }
    h2 
    {
    text-transform: uppercase;
    }
    .navbar.navbar-default {
    border-bottom: 5px solid gray;
    }
    .navbar.navbar-default.navbar-fixed-bottom {
        border-top: 5px solid gray;
    }
    /**
     * table responsive  
    **/

    .table-responsive
    {
        overflow-x: auto;
    } 
    @media only screen and (max-width: 800px) {
    
      /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 

    #no-more-tables tr { 
      display: block; 
    }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  #no-more-tables thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
 
  #no-more-tables tr { border: 1px solid #ccc; }
 
  #no-more-tables td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
    white-space: normal;
    text-align:left;
  }
 
  #no-more-tables td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    text-align:left;
    font-weight: bold;
  }
   
 .input-group.datetime.datepicker {
    text-align: center;
}
  /*
  Label the data
  */
  #no-more-tables td:before { content: attr(data-title); }
}
td,th {
    text-align: center;
}
/**
 * button
 **/
.btn.btn-default.add {
    background: red none repeat scroll 0 0;
    border-radius: 0;
    color: white;
}
.btn-sq {
  width: 100px !important;
  height: 100px !important;
  font-size: 10px;
  border-radius: 0;
  margin-left: 15px;
}

body 
    {
      margin: 0;
      padding: 0;
      width: 100%;
      display: table;
      font-weight: 100;
      font-family:Peugeot;
      background-color: rgb(20, 32, 50);
      color: white;
    }
    .panel-primary > .panel-heading {
    background-color: #fff;
    border-color: #337ab7;
    color: rgb(20, 32, 50);
}
.btn.btn-sq.btn-primary {
    background: white none repeat scroll 0 0;
    color: rgb(20, 32, 50);
    margin-left: 50px;
    margin-bottom: 20px;
    display: inline-block;
}
.miscmenus {
    margin: 0 auto;
    text-align: center;
    width: 50%;
}
    </style>

   </style>
 </head>

  <body>
      @include('includes.navbar')
        <div class="container">
            @yield('content')
        </div>
    @include('includes.footer')
  
