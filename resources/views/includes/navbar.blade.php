 <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <ul class="navbar-left">
            <li>
                <a href="/"><img src={{asset('images/psa-logo.png')}} alt="Logo"></a>
            </li>     
          </ul>
          @if(Session::get('user'))
          <div class="navbar-brand navbar-brand-centered">
            <ul class="nav navbar-nav navbar-right">
            <li><a href="/projects">DASHBOARD</a></li>
            @if(Session::get('projects.permission') > 200)
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">PROJECT <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/projects/create">ADD PROJECT</a></li>
              <li><a href="/archived/OnHold">ON HOLD</a></li>
              <li><a href="/archived/complete">COMPLETE</a></li>
              <li><a href="/archived/Rejected">REJECTED</a></li>
              <li><a href="/archived/washup">WASH UP</a></li>
              <li> <a href="/gantt">GANTT</a></li>
              <li> <a href="/archive">PROJECT REPORT</a></li>
              <li> <a href="/CILview">CIL Report</a></li>
              <li> <a href="/getCIL">Assign CIL</a></li>
          </ul>
          </li> 
           <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">USER <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users">list Users</a></li>
              <li><a href="/users/create">Add User</a></li>
           </ul>
          </li> 
          @endif 
          </ul>
          </div>
          @endif
        </div>
 @if(Session::get('user'))
        <div class="collapse navbar-collapse" id="navbar-brand-centered" >
           <ul class="navbar-right">
          <li><div class="projectname">PROJECT SCHEDULER</div></li>
              @if(Session::get('user'))
              <li>Welcome {{ Session::get('user.userType') }} </li>
            <li>{{ Session::get('user.firstName') }}  {{ Session::get('user.lastName') }}, {{ Session::get('user.username') }}</li>       
            <li><a class="navlink" href="/logout">Logout</a></li>
            @endif
          </ul>
        </div>

  @endif
    </div>
</nav>

