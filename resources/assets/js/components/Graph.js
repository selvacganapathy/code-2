import Chart from 'chart.js';

export default {

	template: `
		<div>
			<canvas width="200" height="200" v-el:canvas></canvas>
		</div>

	`,
	props: {

			labels:{},
			values:{},
			color: {
				default: 'rgba(220,220,220,0.2)'
			}

	},

	ready() {

	var options = {
    	
    	maintainAspectRatio: false,
    
    	responsive: true
	
	};

  var data = {

		    labels    : this.labels,
		    
		    datasets  :   [
		        { 
		            label: 'Project Status',
		            fill: true,
		            lineTension: 0.1,
		            strokeColor: "red",
		            data: this.values
		        }
		    ]
		}

	  	const chart = new Chart(this.$els.canvas.getContext('2d'),{
	                type: 'doughnut',
	                data: data,
	                options: {
	                scales: {
	                xAxes: [{
	                display: false
	            		}]  
	        		}
	    		}
			});
			

		}

}