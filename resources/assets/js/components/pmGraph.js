import Chart from 'chart.js';

export default {

	template: `
		<div>
			<canvas width="200" height="200" v-el:canvas></canvas>
			{{{ legend }}}
		</div>

	`,
	props:['player','apponent'],

	data(){
		return {
			legend: ''
		}; 
	},

	ready(){
		var data = {
			labels:['pm'],

			datasets: [
				{
					label:this.player.name,
					 fillColor: "rgba(220,220,220,0.5)", 
                	strokeColor: "rgba(220,220,220,0.8)", 
                	highlightFill: "rgba(220,220,220,0.75)",
                	data: [this.player.pm]
				},
				{
					label:this.apponent.name,
					 fillColor: "rgba(220,220,220,0.5)", 
                	strokeColor: "rgba(220,220,220,0.8)", 
                	highlightFill: "rgba(220,220,220,0.75)",
                	data: [this.apponent.pm]
				}

			]
		};
			  	const chart = new Chart(this.$els.canvas.getContext('2d'),{
	                type: 'doughnut',
	                data: data,
	                options: {
	                scales: {
	                xAxes: [{
	                display: false
	            		}]  
	        		}
	    		}
			});
	}
}