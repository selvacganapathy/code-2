# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.33.10 (MySQL 5.1.71)
# Database: homestead
# Generation Time: 2016-05-20 11:32:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `audit`;

CREATE TABLE `audit` (
  `auditID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `projectsID` int(10) unsigned NOT NULL,
  `action` enum('CREATE','UPDATE','DELETE') COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`auditID`),
  KEY `audit_projectsid_foreign` (`projectsID`),
  CONSTRAINT `audit_projectsid_foreign` FOREIGN KEY (`projectsID`) REFERENCES `projects` (`projectsID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `commentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectsID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;

INSERT INTO `comment` (`commentID`, `projectsID`, `userID`, `title`, `comments`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'Migration','Meeting held Tuesday 8 March 2016.  Discussions to be held by user areas/brands about their approach to Market Allocation and specifically AOI.  Until then nothing further for us.','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(2,2,1,'Migration','Bini has completed technical documentation','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(3,3,1,'Migration','Discussion and Email sent to Marko Engel - 1 Dec 2015.  User Acceptance Testing.   Still with users Wednesday 9 December 2015, cross charge complete jan 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(4,4,1,'Migration','Tracey Richards indicated that the work we have done needs to be reviewed ready for DS - 6 April 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(5,5,1,'Migration','No further movement - 11 February 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(6,6,1,'Migration','Sarah Powell has asked if we can use the top level requirements that I wrote as the specification.  We could use this one as a UX example.  Sarah Powell is now moving to DS.','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(7,9,1,'Migration','No further movement - 11 February 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(8,10,1,'Migration','Content issues passed back to end users.   Tech Docs needed from BN/SC. Cross Charge Complete Jan 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(9,11,1,'Migration','mtg to be arranged Triad,PLD once iconnect & dweb historical projects have been cleared','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(10,12,1,'Migration','User never returned emails','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(11,13,1,'Migration','Talk to VM budget ? Very low priority','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(12,16,1,'Migration','testing with plan to go live mid-April','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(13,17,1,'Migration','To be given out to all Aftersales Zone Managers','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(14,19,1,'Migration','Cross Charge complete Jan 2016','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(15,20,1,'Migration','In test on Empolo test site','2016-05-10 10:48:26','2016-05-10 10:48:26'),
	(16,21,1,'Migration','To be delivered April without separate Gefco/Faurecia price file','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(17,22,1,'Migration','PDA development complete, C Francais testing.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(18,23,1,'Migration','CO Phase 2 3rd December, no direct impact on ITUK, check whether this can be closed','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(19,24,1,'Migration','This should be part of the wash-up of all projects. SW to check local/web apps','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(20,25,1,'Migration','Raised in CIL 17/02/16, Hélène Bouteleau will take over from Bryn, waiting for access to HERMES','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(21,26,1,'Migration','Most Citroen ID problems now resolved. Still 9 vehicles missing or unlinked','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(22,27,1,'Migration','Scott Morren to do a demonstration to Vijay Mistry and Denise McCabe.  Discussion needed on emails to agency or access to Aftersales Connect.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(23,28,1,'Migration','Review of existing  PLP quotation needed by Development ready for presentation to Marketing.  GDO Gestion d\' Opportuntés (Lead Management) currently being reviewed by Marketing.   Revisited 3 December 2015.  Spoke to Chris Wilkes.  Market Allocation is currently his priority.  Spoke about the Miriam Start piece.  Keep him in the loop.  Draft Quotation prepared.  Quote with ITUK Management team for review.  Current status on hold.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(24,29,1,'Migration','With Philip Brown for Maintenance aspect/budget line.  2 hours per month.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(25,30,1,'Migration','SSL and DNS completed 19/11/05.  Meeting held about contract 1/12/15 - decisions made about how to proceed.  With HR and Purchasing.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(26,31,1,'Migration','mtg 16/03/16 user not decided hwo to take forward agreed to close and raise new request in due course','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(27,33,1,'Migration','All Servers now using Authenticated email server.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(28,34,1,'Migration','Similar to SR0072 (Ed Hickin)','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(29,35,1,'Migration','cross charge for slideshow & custom playlist completed','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(30,36,1,'Migration','Waiting to speak to Steven Fahey for 2016','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(31,37,1,'Migration','PCR sites on hold','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(32,38,1,'Migration','Waiting for information from France, VM to escalate','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(33,39,1,'Migration','On hold. Not definitely planning to continue in the Rental business in 2016.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(34,40,1,'Migration','All stages completed - No User Impact','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(35,44,1,'Migration','Problematic in discussion between DG & HP, meeting 11/1/16','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(36,46,1,'Migration','Technical documents on Wiki.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(37,47,1,'Migration','Technical documents on Wiki.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(38,48,1,'Migration','completed','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(39,51,1,'Migration','Peugeot CI now live.  Support Element to be firmed up.  Back-up and Version Control to be defined.  Work on Citroen CI and DS CI to be split and worked on.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(40,53,1,'Migration','back with users to create an SR (15/10/15). ','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(41,54,1,'Migration','Changes implemented and processed successfully.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(42,55,1,'Migration','Info from AB not to progress eCommerce work will supercede requirements','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(43,56,1,'Migration','No further action required by Project Office todate.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(44,58,1,'Migration','Query with Karen, duplicate of CSI extract?  Don\'t know anything about this piece?','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(45,59,1,'Migration','Software Factory are working on the spec to suggest a solution','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(46,60,1,'Migration','Technical documents on Wiki.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(47,61,1,'Migration','Bertrand from Citroen now wants progress.  Nothing further for Peugeot.  Meeting lined up with Citroen','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(48,62,1,'Migration','Back with users','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(49,65,1,'Migration','Went live Monday 02/11/2015.  Firmed up with user that they were happy for us to close project off.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(50,66,1,'Migration','PSA DMS Survey live.  Can we cross charge?  Cross charges complete.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(51,67,1,'Migration','Technical documents on Wiki.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(52,68,1,'Migration','Completed, cross charged 02/10/2015','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(53,71,1,'Migration','On hold until FCA/4.0 complete','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(54,72,1,'Migration','Completed, cross charged 29/09/2015','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(55,73,1,'Migration','Notifed Bob Grant that investigation complete - no further action by ProjectOffice 02/11/2015','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(56,74,1,'Migration','REGANAL complete, additional work on Spender required','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(57,75,1,'Migration','Liviu has written a report that pulls the info re Autoflow together. No reccomendations made. ','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(58,77,1,'Migration','Waiting user access to Etoile, with France via Damon Clarke','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(59,78,1,'Migration','DG to write spec','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(60,80,1,'Migration','waiting for info from Andy Goodall','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(61,82,1,'Migration','Technical documents needed;  To be added to Cartography; Check Reflex.  Move on to next set of evolutions as separate project.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(62,83,1,'Migration','User is in tender process with external supplier - agreed to close 04/03/2016','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(63,84,1,'Migration','Cross Charge raised 08/12/2015','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(64,85,1,'Migration','Have user requirement.  Turnaround 2 days.  Handling of Claims/Incentives with monetary value of 2.5 Million.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(65,86,1,'Migration','Looking at a central solution  (IS)','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(66,88,1,'Migration','MHu doc sent to ISTA France, awaiting follow up response','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(67,89,1,'Migration','templates, Pat provided them to Scott to approve. Part of Exit planning','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(68,90,1,'Migration','All production at DST from 21st April. CTD final closure date 6th May.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(69,91,1,'Migration','Going out to tender, supplier selected, userst o invite ITUK to presentations','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(70,92,1,'Migration','Quotes signed and returned, Preston-6th June, Maidstone 26th September','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(71,93,1,'Migration','Not had any contact with DM','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(72,94,1,'Migration','CX required for file transfer from Autoline. DG/VM expertise required as per CIL doc.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(73,95,1,'Migration','All terms being amended by Geldards.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(74,96,1,'Migration','Complete','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(75,97,1,'Migration','Complete','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(76,98,1,'Migration','Ongoing, will upgrade pre-prod / test to E31, mid 2016','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(77,99,1,'Migration','Order placed with Verizon. ADAPT order signed & B2B set up for SMC.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(78,100,1,'Migration','Dave Connell to speak to Fidelity as quote too high','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(79,101,1,'Migration','Admin rights issue now resolved. Problem with loading VS2015. Meeting with central team 12/4 to clarify project timescales. Meeting to be organised with MHe, DG and MT .this week to discuss testing plan.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(80,102,1,'Migration','Files now being received individually + combined.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(81,103,1,'Migration','ITUK work complete - Alex to chase Codestorm to ensure it\'s implemented','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(82,104,1,'Migration','Had second meeting with Terry Ward this now needs to be included. WIP.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(83,105,1,'Migration','2nd meeting to be arranged ','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(84,107,1,'Migration','SSO info provided to developer','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(85,108,1,'Migration','project slipped to allow SR0141 eCommerce phase 1 to be completed','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(86,109,1,'Migration','Cross Charge sent to DSINADMIN 19.01.16','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(87,110,1,'Migration','We now have budget line.  Lead testing will take place on 6 January 2016.','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(88,111,1,'Migration','NetDev want to go with Qlikview - Vijay to write to NetDev','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(89,112,1,'Migration','Resubmitted to Dev for estimate 10/02/16 JR','2016-05-10 10:48:27','2016-05-10 10:48:27'),
	(90,113,1,'Migration','workshop arranged for 18th April','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(91,114,1,'Migration','Fanny will manage this, waiting new files to create the SSIS packages (MB/PA)','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(92,115,1,'Migration','Live','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(93,116,1,'Migration','Review Cyber Essentials and self-audit. Apply recommendations','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(94,117,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(95,118,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(96,119,1,'Migration','Citroen and outstanding Peu staff to move w/c 25/4 - Ranj/Stuart','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(97,120,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(98,121,1,'Migration','Next planned is Manchester (MR) 18-20 April','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(99,122,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(100,123,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(101,124,1,'Migration','Cross Charge sent to DSIN Admin 09/02/2016 SJW','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(102,125,1,'Migration','Audio planned for Monday 11 April to review PMC Combi Code progress','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(103,126,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(104,127,1,'Migration','Cross Charge Completed Nov 2015 SJW','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(105,128,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(106,129,1,'Migration','quote for admin facility given to user - waiting sign off','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(107,130,1,'Migration','VN and AV domains swop over week 49','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(108,131,1,'Migration','ON HOLD 07/01/2016 pending email confirmation from Ed Hickin','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(109,132,1,'Migration','Pinnacle demo , MyEngine demo complete waiting for CDK demo','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(110,133,1,'Migration','Cross Charge sent to DSIN Admin 02/02/2016 SJW','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(111,136,1,'Migration','17/02/16 user still deciding, as may be able to be part of GDO,before spec being written','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(112,137,1,'Migration','EPG upgrade planned for the 3rd + 4th of May.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(113,138,1,'Migration','user no longer requires project','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(114,139,1,'Migration','Need to work on this piece with Mark Tweedie','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(115,140,1,'Migration','place holder - looking at for 2016 based on progress of ITUK portal progress','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(116,141,1,'Migration','domain has been set up -no charge for this one.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(117,142,1,'Migration','Retrospective request in order to to secure 2015 budget.  System request in, Estimate in, Budget line defined.  PO being raised by PLD. ','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(118,143,1,'Migration','Waiting feedback from HR as several requests from different sources there','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(119,144,1,'Migration','Developer progressing well despite massive changes to spec which we are currently working through','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(120,145,1,'Migration','Output rerouted to the correct printers (CYIP16 + THIPA5) and confirmed.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(121,146,1,'Migration','Quote submitted to user','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(122,147,1,'Migration','Follow on from spender Rollover, quote prepared and sent, cross charge sent to DSINADMIN 19.01.16','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(123,148,1,'Migration','AM still pending feedback from PCR, Andrew Newman.  Second priority after Used Car Prep','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(124,149,1,'Migration','20/01/16 Pending new  evolution of Brand TV','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(125,150,1,'Migration','Julie Cozens has confirmed happy with Payment process - 6 April 2016','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(126,151,1,'Migration','LAN separated; laptops all approved; softphone solution uncertain','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(127,152,1,'Migration','POC ufinal tweak taking place ready for demo week commencing the 11th of April. ','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(128,153,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(129,154,1,'Migration','system request & quote sent to user 22/01/16','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(130,155,1,'Migration','Underway','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(131,156,1,'Migration','Pilot sites complete, plan for rest proposed','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(132,157,1,'Migration','Training documentation, user IDs and software being set up for move','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(133,158,1,'Migration','Workshop planned for 26/04/16','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(134,159,1,'Migration','This project is on hold at the minute Re: DG','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(135,160,1,'Migration','SMMT announced delay of at least 1 month to go-live','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(136,161,1,'Migration','2 domains outstanding','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(137,162,1,'Migration','tech docs required from Dev.  Support docs issued, on reflex and cartography','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(138,163,1,'Migration','Dev started, first run of data -user v happy','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(139,164,1,'Migration','MT visited Image+ - problems resolved','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(140,165,1,'Migration','iPads returned','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(141,166,1,'Migration','Quote and Plan to be issues.  The principal users do not wish to continue with the project.  Project archived on Trello.','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(142,167,1,'Migration','Daniel Bailey has now arranged a follow up meeting, ready to review Responses','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(143,168,1,'Migration','loan extended iPads to be returned on 26th  April 2016 (Jenny Price rqst 18/04/2016) ','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(144,169,1,'Migration','Signed quote received & Script implemntd 18/02/2016 JR','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(145,170,1,'Migration','Quote approval received','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(146,171,1,'Migration','Waiting for written confirmation of name change and firm up of charte graphic - Quarter 2 June 2016','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(147,172,1,'Migration','Live 06/04/16','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(148,173,1,'Migration','No progress','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(149,174,1,'Migration','04/03 - user confirmed emails working','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(150,175,1,'Migration','advsed user - Proof of Concept for big data / or may go to S/war factory - will updt April','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(151,176,1,'Migration','Quote Prepared - wait approval before sending to user','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(152,177,1,'Migration','1st Live run Sunday 3rd of April confirmed receipt of file by Indus - monitor 1 wk','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(153,178,1,'Migration','DG looking into the secruity WeTransfer','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(154,179,1,'Migration','AM pulled most data, MH working on orders without registration data','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(155,180,1,'Migration','Quote & Service request sent out to Scott Westerby 08/03/2016','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(156,181,1,'Migration','Complete','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(157,182,1,'Migration','1st meeting with Dev team wk 15','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(158,183,1,'Migration','provided additional data to user - on hold pending NFA 12/04/2016','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(159,184,1,'Migration','met with user 11/04/2016 - reviewed draft screens','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(160,185,1,'Migration','spoke to Pat, emailed Mujahid for further info','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(161,186,1,'Migration','Meeting with Parts 19/4 to discuss tender','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(162,187,1,'Migration','waiting for domain name & server details','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(163,190,1,'Migration','signed quote received','2016-05-10 10:48:28','2016-05-10 10:48:28'),
	(164,191,1,'Migration','will be part of SR0152','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(165,192,1,'Migration','To be done in 2 phases - waiting signed quote return','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(166,193,1,'Migration','Reprinting to email possible, raising original invoice to email from dealer not possible due to network restrictions. Emma OK.','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(167,194,1,'Migration','Rebrand all local systems for PSA, change email signatures','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(168,195,1,'Migration','To make our life easier','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(169,197,1,'Migration','?','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(170,198,1,'Migration','met with user 18/04/2016 - started outline scope - will probably be a multi-phased project','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(171,200,1,'Migration','Temporary functionality setup - secure process to be looked into','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(172,201,1,'Migration','Waiting for info from Central','2016-05-10 10:48:29','2016-05-10 10:48:29'),
	(173,1,1,'Migration','Bini has completed technical documentation','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(174,2,1,'Migration','Discussion and Email sent to Marko Engel - 1 Dec 2015.  User Acceptance Testing.   Still with users Wednesday 9 December 2015, cross charge complete jan 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(175,3,1,'Migration','DS Investor and Workflow incorporated.  Development have pushed to live.  Principal Users informed.  Net Dev still have monies left in the pot for the assessment piece that was put hold.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(176,4,1,'Migration','No further movement - 11 February 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(177,5,1,'Migration','Sarah Powell has asked if we can use the top level requirements that I wrote as the specification.  We could use this one as a UX example.  Sarah Powell is now moving to DS.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(178,8,1,'Migration','Aftersales Connect Evolutions fulfils the requirement - 4 May 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(179,9,1,'Migration','Content issues passed back to end users.   Tech Docs needed from BN/SC. Cross Charge Complete Jan 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(180,10,1,'Migration','mtg to be arranged Triad,PLD once iconnect & dweb historical projects have been cleared','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(181,11,1,'Migration','Meeting held Tuesday 8 March 2016.  Discussions to be held by user areas/brands about their approach to Market Allocation and specifically AOI.  Until then nothing further for us.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(182,12,1,'Migration','User never returned emails','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(183,13,1,'Migration','Talk to VM budget ? Very low priority','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(184,16,1,'Migration','Lou writing user notes','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(185,17,1,'Migration','To be given out to all Aftersales Zone Managers','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(186,19,1,'Migration','Cross Charge complete Jan 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(187,20,1,'Migration','HR want to launch wk 32','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(188,21,1,'Migration','Regional deliveries in test','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(189,22,1,'Migration','MADD - AM to complete work for Preprod','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(190,23,1,'Migration','CO Phase 2 3rd December, no direct impact on ITUK, check whether this can be closed','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(191,24,1,'Migration','This should be part of the wash-up of all projects. SW to check local/web apps','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(192,25,1,'Migration','Raised in CIL 17/02/16, Hélène Bouteleau will take over from Bryn, waiting for access to HERMES','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(193,26,1,'Migration','PFM files used for both marques this month','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(194,27,1,'Migration','Moving to live Wednesday 18 May 2016.  One element missing documentation is to be moved at a later agreed date.  This element with Ian Sedgwick for test.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(195,28,1,'Migration','Review of existing  PLP quotation needed by Development ready for presentation to Marketing.  GDO Gestion d\' Opportuntés (Lead Management) currently being reviewed by Marketing.   Revisited 3 December 2015.  Spoke to Chris Wilkes.  Market Allocation is currently his priority.  Spoke about the Miriam Start piece.  Keep him in the loop.  Draft Quotation prepared.  Quote with ITUK Management team for review.  Current status on hold.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(196,29,1,'Migration','With Philip Brown for Maintenance aspect/budget line.  2 hours per month.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(197,30,1,'Migration','SSL and DNS completed 19/11/05.  Meeting held about contract 1/12/15 - decisions made about how to proceed.  With HR and Purchasing.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(198,31,1,'Migration','mtg 16/03/16 user not decided hwo to take forward agreed to close and raise new request in due course','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(199,33,1,'Migration','All Servers now using Authenticated email server.','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(200,34,1,'Migration','Similar to SR0072 (Ed Hickin)','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(201,35,1,'Migration','cross charge for slideshow & custom playlist completed','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(202,36,1,'Migration','Waiting to speak to Steven Fahey for 2016','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(203,37,1,'Migration','PCR sites on hold','2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(204,38,1,'Migration','Waiting for information from France, VM to escalate','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(205,39,1,'Migration','On hold. Not definitely planning to continue in the Rental business in 2016.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(206,40,1,'Migration','All stages completed - No User Impact','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(207,44,1,'Migration','Problematic in discussion between DG & HP, meeting 11/1/16','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(208,46,1,'Migration','Technical documents on Wiki.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(209,47,1,'Migration','Technical documents on Wiki.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(210,48,1,'Migration','completed','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(211,51,1,'Migration','Peugeot CI now live.  Support Element to be firmed up.  Back-up and Version Control to be defined.  Currently working on Citroen and DS CI.  Pending quotes from Suppliers.  Pending discussion on which supplier to go with before moving forward.  ITUK and Network Dev','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(212,53,1,'Migration','back with users to create an SR (15/10/15). ','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(213,54,1,'Migration','Changes implemented and processed successfully.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(214,55,1,'Migration','Info from AB not to progress eCommerce work will supercede requirements','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(215,56,1,'Migration','No further action required by Project Office todate.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(216,58,1,'Migration','Query with Karen, duplicate of CSI extract?  Don\'t know anything about this piece?','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(217,59,1,'Migration','Software Factory are working on the spec to suggest a solution','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(218,60,1,'Migration','Technical documents on Wiki.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(219,61,1,'Migration','Bertrand from Citroen now wants progress.  Nothing further for Peugeot.  Meeting lined up with Citroen','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(220,62,1,'Migration','Back with users','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(221,65,1,'Migration','Went live Monday 02/11/2015.  Firmed up with user that they were happy for us to close project off.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(222,66,1,'Migration','PSA DMS Survey live.  Can we cross charge?  Cross charges complete.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(223,67,1,'Migration','Technical documents on Wiki.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(224,68,1,'Migration','Completed, cross charged 02/10/2015','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(225,71,1,'Migration','On hold until FCA/4.0 complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(226,72,1,'Migration','Completed, cross charged 29/09/2015','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(227,73,1,'Migration','Notifed Bob Grant that investigation complete - no further action by ProjectOffice 02/11/2015','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(228,74,1,'Migration','REGANAL complete, additional work on Spender required','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(229,75,1,'Migration','Liviu has written a report that pulls the info re Autoflow together. No reccomendations made. ','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(230,77,1,'Migration','Waiting user access to Etoile, with France via Damon Clarke','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(231,78,1,'Migration','DG to write spec','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(232,80,1,'Migration','waiting for info from Andy Goodall','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(233,82,1,'Migration','Technical documents needed;  To be added to Cartography; Check Reflex.  Move on to next set of evolutions as separate project.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(234,83,1,'Migration','User is in tender process with external supplier - agreed to close 04/03/2016','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(235,84,1,'Migration','Cross Charge raised 08/12/2015','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(236,85,1,'Migration','Have user requirement.  Turnaround 2 days.  Handling of Claims/Incentives with monetary value of 2.5 Million.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(237,86,1,'Migration','Looking at a central solution  (IS)','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(238,88,1,'Migration','MHu doc sent to ISTA France, awaiting follow up response','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(239,89,1,'Migration','templates, Pat provided them to Scott to approve. Part of Exit planning','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(240,90,1,'Migration','All production at DST from 21st April. CTD final closure date 6th May.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(241,91,1,'Migration','Going out to tender, supplier selected, userst o invite ITUK to presentations','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(242,92,1,'Migration','Preston-6th June, Maidstone 26th September. Attended discovery meeting 16/5','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(243,93,1,'Migration','Not had any contact with DM','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(244,94,1,'Migration','CX required for file transfer from Autoline. DG/VM expertise required as per CIL doc.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(245,95,1,'Migration','All terms being amended by Geldards.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(246,96,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(247,97,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(248,98,1,'Migration','Ongoing, will upgrade pre-prod / test to E31, mid 2016','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(249,99,1,'Migration','Awaiting install date from Verizon. Audio held with CDK to kick off project/inform progress ','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(250,100,1,'Migration','Dave Connell to speak to Fidelity as quote too high','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(251,101,1,'Migration','Modification of Application-Manager-Agent rules made centrally. Tested but VS2015 still won’t install via V3. Fed back to central team. A standalone laptop has been built with Win10 and Office 2016 Pro to enable application testing in the meantime.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(252,102,1,'Migration','Files now being received individually + combined.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(253,103,1,'Migration','ITUK work complete - Alex to chase Codestorm to ensure it\'s implemented','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(254,104,1,'Migration','Had second meeting with Terry Ward this now needs to be included. WIP.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(255,105,1,'Migration','2nd meeting to be arranged ','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(256,107,1,'Migration','SSO info provided to developer','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(257,108,1,'Migration','project re-started 03/05/2016','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(258,109,1,'Migration','Cross Charge sent to DSINADMIN 19.01.16','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(259,110,1,'Migration','We now have budget line.  Lead testing will take place on 6 January 2016.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(260,111,1,'Migration','NetDev want to go with Qlikview - Vijay to write to NetDev','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(261,112,1,'Migration','Submitted quote to Users 10/05/2016','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(262,113,1,'Migration','project back-log created','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(263,114,1,'Migration','Fanny will manage this, waiting new files to create the SSIS packages (MB/PA)','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(264,115,1,'Migration','Live','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(265,116,1,'Migration','Review Cyber Essentials and self-audit. Apply recommendations','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(266,117,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(267,118,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(268,119,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(269,120,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(270,121,1,'Migration','Next planned is Manchester (MR) 18-20 April','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(271,122,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(272,123,1,'Migration','Complete','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(273,124,1,'Migration','Cross Charge sent to DSIN Admin 09/02/2016 SJW','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(274,125,1,'Migration','Still have issue with Status drop down box appearing in Programmer view for Combi Code for Iconnect.  Issue has been passed on to Triad for resolution.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(275,126,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.  Incident currently active.','2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(276,127,1,'Migration','Cross Charge Completed Nov 2015 SJW','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(277,128,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(278,129,1,'Migration','quote for admin facility given to user - waiting sign off','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(279,130,1,'Migration','VN and AV domains swop over week 49','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(280,131,1,'Migration','ON HOLD 07/01/2016 pending email confirmation from Ed Hickin','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(281,132,1,'Migration','Pinnacle demo , MyEngine demo complete waiting for CDK demo','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(282,133,1,'Migration','Cross Charge sent to DSIN Admin 02/02/2016 SJW','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(283,136,1,'Migration','17/02/16 user still deciding, as may be able to be part of GDO,before spec being written','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(284,137,1,'Migration','EPG upgraded, waiting on HSM (Hardware) + Application split.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(285,138,1,'Migration','user no longer requires project','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(286,139,1,'Migration','Need to work on this piece with Mark Tweedie','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(287,140,1,'Migration','place holder - looking at for 2016 based on progress of ITUK portal progress','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(288,141,1,'Migration','domain has been set up -no charge for this one.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(289,142,1,'Migration','Retrospective request in order to to secure 2015 budget.  System request in, Estimate in, Budget line defined.  PO being raised by PLD. ','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(290,143,1,'Migration','Existing docinfo domain for HR being cleared down and this will be used for their documentation','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(291,144,1,'Migration','Progressed with Lead Time Calculation.  Testing of screens has commenced.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(292,145,1,'Migration','Output rerouted to the correct printers (CYIP16 + THIPA5) and confirmed.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(293,146,1,'Migration','Project cancelled - 10/05/2016 ','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(294,147,1,'Migration','Follow on from spender Rollover, quote prepared and sent, cross charge sent to DSINADMIN 19.01.16','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(295,148,1,'Migration','Email sent to David Male asking for feedback, re closing project 27/04/2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(296,149,1,'Migration','20/01/16 Pending new  evolution of Brand TV','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(297,150,1,'Migration','Julie Cozens has confirmed happy with Payment process - 6 April 2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(298,151,1,'Migration','LAN separated; laptops all approved; softphone solution uncertain','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(299,152,1,'Migration','POC ufinal tweak taking place ready for demo week commencing the 11th of April. ','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(300,153,1,'Migration','Technical documentation required.  Should already be on Cartography.  Check on Reflex.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(301,154,1,'Migration','new requirement received, PA estimating','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(302,155,1,'Migration','Underway','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(303,156,1,'Migration','Pilot sites complete, plan for rest proposed','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(304,157,1,'Migration','Training documentation in progress. Remote training 19/20 May and Vigo 12-18 Jun','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(305,158,1,'Migration','To go out to tender','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(306,159,1,'Migration','This project is on hold at the minute Re: DG','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(307,160,1,'Migration','New proposed launch 18th July, to be formally confirmed','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(308,161,1,'Migration','2 domains outstanding','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(309,162,1,'Migration','tech docs required from Dev.  Support docs issued, on reflex and cartography','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(310,163,1,'Migration','MH running test runs, added SFTP to scope & xtra quote submitted to user','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(311,164,1,'Migration','MT visited Image+ - problems resolved','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(312,165,1,'Migration','iPads returned','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(313,166,1,'Migration','Quote and Plan to be issues.  The principal users do not wish to continue with the project.  Project archived on Trello.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(314,167,1,'Migration','Draft Quotation pending approval.  Info needed for Project Plan from Development.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(315,168,1,'Migration','All iPAd sreturned by 29/04/2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(316,169,1,'Migration','Signed quote received & Script implemntd 18/02/2016 JR','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(317,170,1,'Migration','Quote approval received','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(318,171,1,'Migration','Waiting for written confirmation of name change and firm up of charte graphic - Quarter 2 June 2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(319,172,1,'Migration','Live 06/04/16','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(320,173,1,'Migration','No progress','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(321,174,1,'Migration','04/03 - user confirmed emails working','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(322,175,1,'Migration','advsed user - Proof of Concept for big data / or may go to S/war factory - will updt April','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(323,176,1,'Migration','user cancelled project - no budget plus Citroen & DS not launching e-commerce in 2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(324,177,1,'Migration','1st Live run Sunday 3rd of April confirmed receipt of file by Indus - monitor 1 wk','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(325,178,1,'Migration','DG looking into the secruity WeTransfer','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(326,179,1,'Migration','AM pulled most data, MH working on orders without registration data,','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(327,180,1,'Migration','Quote & Service request sent out to Scott Westerby 08/03/2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(328,181,1,'Migration','Complete','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(329,182,1,'Migration','Work continuing','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(330,183,1,'Migration','user agreed NFA','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(331,184,1,'Migration','Submitted to Dev for Quote 10/05/2016','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(332,185,1,'Migration','meeting with Finance and marketing to discuss options for short tem and long term fix','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(333,186,1,'Migration','Supplier presentations 16/5. Decision is to remain with Modulus','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(334,187,1,'Migration','waiting for domain name & server details','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(335,188,1,'Migration','domain transfer from RTL w/c 16/05/16','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(336,191,1,'Migration','will be part of SR0152','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(337,192,1,'Migration','To be done in 2 phases - waiting signed quote return','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(338,193,1,'Migration','Reprinting to email possible, raising original invoice to email from dealer not possible due to network restrictions. Emma OK.','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(339,194,1,'Migration','Rebrand all local systems for PSA, change email signatures','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(340,195,1,'Migration','To make our life easier','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(341,197,1,'Migration','Mtg to be held with Jennifer Roberts 18th May','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(342,198,1,'Migration','2nd meeting held 26/04/16 - ongoing ','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(343,199,1,'Migration','Waiting on Estimate ','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(344,200,1,'Migration','Temporary functionality setup - secure process to be looked into','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(345,201,1,'Migration','Waiting for info from Central & signed quote','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(346,202,1,'Migration','MB has created proposal and estimate','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(347,203,1,'Migration','Discoverty meeting with Dave M held. Meeting booked with Alan for next Wednesday','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(348,204,1,'Migration','Discovery meeting planned with Ed 10/05/16','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(349,207,1,'Migration','Discovery meeting to be planned','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(350,208,1,'Migration','Meeting to be arranged - no decision on any change has yet been taken','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(351,209,1,'Migration','PA had further questions for Scott','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(352,210,1,'Migration','cross-charge requested','2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(353,211,1,'Migration','Initial meeting arranged for 20/5','2016-05-19 09:53:47','2016-05-19 09:53:47'),
	(354,213,1,'Migration','cross-charge requested','2016-05-19 09:53:47','2016-05-19 09:53:47');

/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `departmentsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`departmentsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`departmentsID`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'PMC ','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(2,'Network Development','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(3,'PMC Sales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(4,'Events','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(5,'Parts and Service','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(6,'Warranty','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(7,'HR','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(8,'PLD','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(9,'PMC Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(10,'ITUK','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(11,'Aftersales & Quality','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(12,'DSP','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(13,'HR/UVO/PCD/PCR','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(14,'Finance','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(15,'UVO','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(16,'Finance/PMC Sales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(17,'CUK Sales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(18,'Sales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(19,'Fleet  ','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(20,'PMC Events','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(21,'Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(22,'PCR','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(23,'Aftersales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(24,'Dealer Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(25,'Citroen Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(26,'Digital','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(27,'Homologation','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(28,'DFNE','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(29,'Support Services','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(30,'PMC Dealer Events','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(31,'Network Facilities','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(32,'RHNE','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(33,'Payroll','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(34,'Fleet & Sales','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(35,'Fleet','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(36,'Facilities','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(37,'Programme Development','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(38,'Various','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(39,'on behalf of HR','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(40,'DSIN','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(41,'Quality/Academy','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(42,'MD Office','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(43,'DS Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(44,'Quality','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(45,'DEUR/DIPS','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(46,'BPF','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(47,'DS marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(48,'Parts & Service','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(49,'Fleet Operations','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(50,'PMC Sales Programmes','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(51,'PCR/UK','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(52,'Peugeot Marketing','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(53,'Sales, PLD','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(54,'Customer Relations','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(55,'Peugeot Events','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(56,'Academy','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(57,'Communications','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(58,'Commercial Finance','','2016-04-27 15:03:04','2016-04-27 15:03:04'),
	(59,'DDCE','','2016-04-27 15:03:04','2016-04-27 15:03:04');

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gantt_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gantt_links`;

CREATE TABLE `gantt_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `gantt_links` WRITE;
/*!40000 ALTER TABLE `gantt_links` DISABLE KEYS */;

INSERT INTO `gantt_links` (`id`, `source`, `target`, `type`)
VALUES
	(1,1,2,'1'),
	(2,2,3,'2'),
	(3,1,4,'1'),
	(4,4,5,'2'),
	(5,1,6,'1'),
	(6,1,7,'1'),
	(7,1,8,'1'),
	(8,9,10,'1'),
	(9,10,11,'2'),
	(10,9,12,'1'),
	(11,12,13,'2'),
	(12,9,14,'1'),
	(13,14,15,'2'),
	(14,9,16,'1'),
	(15,16,17,'2'),
	(16,9,18,'1'),
	(17,18,19,'2'),
	(18,20,21,'1'),
	(19,20,22,'1'),
	(20,20,23,'1'),
	(21,20,24,'1'),
	(22,20,25,'1'),
	(23,26,27,'1'),
	(24,27,28,'2'),
	(25,26,29,'1'),
	(26,26,30,'1'),
	(27,26,31,'1'),
	(28,26,32,'1'),
	(29,33,34,'1'),
	(30,34,35,'2'),
	(31,33,36,'1'),
	(32,36,37,'2'),
	(33,33,38,'1'),
	(34,38,39,'2'),
	(35,33,40,'1'),
	(36,40,41,'2'),
	(37,33,42,'1'),
	(38,42,43,'2'),
	(39,44,45,'1'),
	(40,45,46,'2'),
	(41,44,47,'1'),
	(42,47,48,'2'),
	(43,44,49,'1'),
	(44,49,50,'2'),
	(45,44,51,'1'),
	(46,51,52,'2'),
	(47,44,53,'1'),
	(48,53,54,'2'),
	(49,55,56,'1'),
	(50,55,57,'1'),
	(51,55,58,'1'),
	(52,55,59,'1'),
	(53,55,60,'1');

/*!40000 ALTER TABLE `gantt_links` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gantt_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gantt_tasks`;

CREATE TABLE `gantt_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `duration` int(11) NOT NULL,
  `progress` float NOT NULL,
  `sortorder` double NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `planned_start` datetime DEFAULT NULL,
  `planned_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `gantt_tasks` WRITE;
/*!40000 ALTER TABLE `gantt_tasks` DISABLE KEYS */;

INSERT INTO `gantt_tasks` (`id`, `text`, `start_date`, `end_date`, `duration`, `progress`, `sortorder`, `parent`, `deadline`, `planned_start`, `planned_end`)
VALUES
	(1,'PSA Investor Database','2016-02-22 00:00:00','2016-03-30 00:00:00',38,0,0,0,NULL,NULL,NULL),
	(2,'DISCOVER','2016-02-22 00:00:00','2016-02-23 00:00:00',1,0,0,1,NULL,NULL,NULL),
	(3,'Actual','2016-02-24 00:00:00','2016-02-23 00:00:00',1,0,0,2,NULL,NULL,NULL),
	(4,'PLAN','2016-02-29 00:00:00','2016-03-03 00:00:00',3,0,0,1,NULL,NULL,NULL),
	(5,'Actual','2016-03-23 00:00:00','2016-03-03 00:00:00',3,0,0,4,NULL,NULL,NULL),
	(6,'DO','2016-03-22 00:00:00','2016-03-27 00:00:00',5,0,0,1,NULL,NULL,NULL),
	(7,'IMPLEMENT','2016-03-16 00:00:00','2016-03-22 00:00:00',6,0,0,1,NULL,NULL,NULL),
	(8,'CLOSE','2016-03-30 00:00:00','2016-03-31 00:00:00',1,0,0,1,NULL,NULL,NULL),
	(9,'PMC Network Facilities Website','2016-02-16 00:00:00','2016-03-15 00:00:00',35,0,0,0,NULL,NULL,NULL),
	(10,'DISCOVER','2016-02-16 00:00:00','2016-02-17 00:00:00',1,0,0,9,NULL,NULL,NULL),
	(11,'Actual','2016-02-18 00:00:00','2016-02-17 00:00:00',0,0,0,10,NULL,NULL,NULL),
	(12,'PLAN','2016-02-21 00:00:00','2016-03-02 00:00:00',10,0,0,9,NULL,NULL,NULL),
	(13,'Actual','2016-02-24 00:00:00','2016-03-02 00:00:00',5,0,0,12,NULL,NULL,NULL),
	(14,'DO','2016-02-24 00:00:00','2016-02-28 00:00:00',4,0,0,9,NULL,NULL,NULL),
	(15,'Actual','2016-02-21 00:00:00','2016-02-28 00:00:00',3,0,0,14,NULL,NULL,NULL),
	(16,'IMPLEMENT','2016-02-29 00:00:00','2016-03-18 00:00:00',18,0,0,9,NULL,NULL,NULL),
	(17,'Actual','0000-00-00 00:00:00','2016-03-18 00:00:00',0,0,0,16,NULL,NULL,NULL),
	(18,'CLOSE','2016-03-15 00:00:00','2016-03-22 00:00:00',7,0,0,9,NULL,NULL,NULL),
	(19,'Actual','0000-00-00 00:00:00','2016-03-22 00:00:00',0,0,0,18,NULL,NULL,NULL),
	(20,'Brand TV','2016-02-29 00:00:00','2016-03-26 00:00:00',28,0,0,0,NULL,NULL,NULL),
	(21,'DISCOVER','2016-02-29 00:00:00','2016-03-02 00:00:00',2,0,0,20,NULL,NULL,NULL),
	(22,'PLAN','2016-02-25 00:00:00','2016-02-28 00:00:00',3,0,0,20,NULL,NULL,NULL),
	(23,'DO','2016-03-02 00:00:00','2016-03-07 00:00:00',5,0,0,20,NULL,NULL,NULL),
	(24,'IMPLEMENT','2016-03-23 00:00:00','2016-03-24 00:00:00',1,0,0,20,NULL,NULL,NULL),
	(25,'CLOSE','2016-03-26 00:00:00','2016-03-28 00:00:00',2,0,0,20,NULL,NULL,NULL),
	(26,'Bluebox Audit','2016-02-01 00:00:00','2016-02-21 00:00:00',30,0,0,0,NULL,NULL,NULL),
	(27,'DISCOVER','2016-02-01 00:00:00','2016-02-11 00:00:00',10,0,0,26,NULL,NULL,NULL),
	(28,'Actual','2016-02-04 00:00:00','2016-02-11 00:00:00',15,0,0,27,NULL,NULL,NULL),
	(29,'PLAN','2016-02-11 00:00:00','2016-02-15 00:00:00',4,0,0,26,NULL,NULL,NULL),
	(30,'DO','2016-02-16 00:00:00','2016-02-18 00:00:00',2,0,0,26,NULL,NULL,NULL),
	(31,'IMPLEMENT','2016-02-20 00:00:00','2016-02-21 00:00:00',1,0,0,26,NULL,NULL,NULL),
	(32,'CLOSE','2016-02-21 00:00:00','2016-03-02 00:00:00',10,0,0,26,NULL,NULL,NULL),
	(33,'PMC Sales Enquiry Application','2016-02-02 00:00:00','2016-02-25 00:00:00',25,0,0,0,NULL,NULL,NULL),
	(34,'DISCOVER','2016-02-02 00:00:00','2016-02-04 00:00:00',2,0,0,33,NULL,NULL,NULL),
	(35,'Actual','2016-02-01 00:00:00','2016-02-04 00:00:00',3,0,0,34,NULL,NULL,NULL),
	(36,'PLAN','2016-02-10 00:00:00','2016-02-11 00:00:00',1,0,0,33,NULL,NULL,NULL),
	(37,'Actual','2016-02-10 00:00:00','2016-02-11 00:00:00',2,0,0,36,NULL,NULL,NULL),
	(38,'DO','2016-02-12 00:00:00','2016-02-22 00:00:00',10,0,0,33,NULL,NULL,NULL),
	(39,'Actual','2016-02-13 00:00:00','2016-02-22 00:00:00',10,0,0,38,NULL,NULL,NULL),
	(40,'IMPLEMENT','2016-02-22 00:00:00','2016-02-23 00:00:00',1,0,0,33,NULL,NULL,NULL),
	(41,'Actual','2016-02-22 00:00:00','2016-02-23 00:00:00',1,0,0,40,NULL,NULL,NULL),
	(42,'CLOSE','2016-02-25 00:00:00','2016-02-27 00:00:00',2,0,0,33,NULL,NULL,NULL),
	(43,'Actual','2016-02-01 00:00:00','2016-02-27 00:00:00',4,0,0,42,NULL,NULL,NULL),
	(44,'Lecturn maintenance 2','2016-02-21 00:00:00','2016-02-10 00:00:00',6,0,0,0,NULL,NULL,NULL),
	(45,'DISCOVER','2016-02-21 00:00:00','2016-03-02 00:00:00',10,0,0,44,NULL,NULL,NULL),
	(46,'Actual','2016-02-21 00:00:00','2016-03-02 00:00:00',2,0,0,45,NULL,NULL,NULL),
	(47,'PLAN','2016-02-23 00:00:00','2016-02-29 00:00:00',6,0,0,44,NULL,NULL,NULL),
	(48,'Actual','2016-03-01 00:00:00','2016-02-29 00:00:00',3,0,0,47,NULL,NULL,NULL),
	(49,'DO','2016-02-29 00:00:00','2016-03-03 00:00:00',3,0,0,44,NULL,NULL,NULL),
	(50,'Actual','2016-02-27 00:00:00','2016-03-03 00:00:00',3,0,0,49,NULL,NULL,NULL),
	(51,'IMPLEMENT','2016-03-05 00:00:00','2016-03-09 00:00:00',4,0,0,44,NULL,NULL,NULL),
	(52,'Actual','2016-03-06 00:00:00','2016-03-09 00:00:00',4,0,0,51,NULL,NULL,NULL),
	(53,'CLOSE','2016-02-10 00:00:00','2016-02-15 00:00:00',5,0,0,44,NULL,NULL,NULL),
	(54,'Actual','2016-03-10 00:00:00','2016-02-15 00:00:00',5,0,0,53,NULL,NULL,NULL),
	(55,'PMC Goodwood Festival of Speed','2016-02-25 00:00:00','2016-03-30 00:00:00',35,0,0,0,NULL,NULL,NULL),
	(56,'DISCOVER','2016-02-25 00:00:00','2016-02-27 00:00:00',2,0,0,55,NULL,NULL,NULL),
	(57,'PLAN','2016-03-05 00:00:00','2016-03-08 00:00:00',3,0,0,55,NULL,NULL,NULL),
	(58,'DO','2016-03-10 00:00:00','2016-03-13 00:00:00',3,0,0,55,NULL,NULL,NULL),
	(59,'IMPLEMENT','2016-03-24 00:00:00','2016-03-25 00:00:00',1,0,0,55,NULL,NULL,NULL),
	(60,'CLOSE','2016-03-30 00:00:00','2016-03-31 00:00:00',1,0,0,55,NULL,NULL,NULL);

/*!40000 ALTER TABLE `gantt_tasks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2015_12_17_101124_create_projects_table',1),
	('2015_12_17_101152_create_comment_table',1),
	('2015_12_17_101209_create_audit_table',1),
	('2015_12_17_101229_create_projectStatus_table',1),
	('2015_12_17_101303_create_priorities_table',1),
	('2015_12_17_101325_create_departments_table',1),
	('2015_12_17_101343_create_suppliers_table',1),
	('2016_01_12_161305_projectMood',1),
	('2016_02_16_141810_create_gantt_links_table',1),
	('2016_02_16_141949_create_gantt_tasks_table',1),
	('2016_03_10_112207_create_projectaccess_table',1),
	('2016_04_25_151125_create_supplier_users_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table priorities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `priorities`;

CREATE TABLE `priorities` (
  `prioritiesID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`prioritiesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `priorities` WRITE;
/*!40000 ALTER TABLE `priorities` DISABLE KEYS */;

INSERT INTO `priorities` (`prioritiesID`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(0,'ND','Not Defined','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(1,'T0','S2NE Tracked','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(2,'T1','ITUK Tracked, High','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(3,'T2','ITUK Tracked, Medium','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(4,'T3','ITUK Tracked - Low','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(5,'P1','Untracked, High','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(6,'P2','Untracked, High','2015-12-17 12:34:52','2015-12-17 12:34:52'),
	(7,'P3','Untracked - Low','2015-12-17 12:34:52','2015-12-17 12:34:52');

/*!40000 ALTER TABLE `priorities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_statuses`;

CREATE TABLE `project_statuses` (
  `projectStatusID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `colorCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`projectStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `project_statuses` WRITE;
/*!40000 ALTER TABLE `project_statuses` DISABLE KEYS */;

INSERT INTO `project_statuses` (`projectStatusID`, `name`, `description`, `colorCode`, `created_at`, `updated_at`)
VALUES
	(1,'New Enquiry','customer request new enquiry about develop  project','#E060D1','2015-12-17 12:34:52','2015-12-17 13:59:27'),
	(2,'On Hold','Project put on hold for further discussions','#9E9C37','2015-12-18 13:09:51','2015-12-18 13:09:51'),
	(3,'Rejected','project get rejected due to facts','#BFBFBF','2015-12-18 13:10:44','2015-12-18 13:10:44'),
	(4,'Wash Up','project washed up \r\n','#D7EA00','2015-12-18 13:11:39','2015-12-18 13:11:39'),
	(6,'In Progress','','#F4F144','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,'Complete','','#30A84E','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `project_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projectaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projectaccess`;

CREATE TABLE `projectaccess` (
  `accessID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usersID` int(11) NOT NULL,
  `projectsID` int(11) NOT NULL,
  `userType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`accessID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projectaccess` WRITE;
/*!40000 ALTER TABLE `projectaccess` DISABLE KEYS */;

INSERT INTO `projectaccess` (`accessID`, `usersID`, `projectsID`, `userType`, `created_at`, `updated_at`)
VALUES
	(5,16,6,'PM','2016-03-10 15:06:22','2016-03-10 15:06:22'),
	(6,19,6,'PM','2016-03-10 15:06:22','2016-03-10 15:06:22'),
	(7,10,6,'D','2016-03-10 15:06:22','2016-03-10 15:06:22'),
	(8,11,6,'D','2016-03-10 15:06:22','2016-03-10 15:06:22'),
	(9,16,7,'','2016-03-10 15:10:28','2016-03-10 15:10:28'),
	(10,21,7,'','2016-03-10 15:10:28','2016-03-10 15:10:28'),
	(11,8,7,'','2016-03-10 15:10:28','2016-03-10 15:10:28'),
	(12,9,7,'','2016-03-10 15:10:28','2016-03-10 15:10:28'),
	(13,16,8,'','2016-03-10 15:26:31','2016-03-10 15:26:31'),
	(14,21,8,'','2016-03-10 15:26:31','2016-03-10 15:26:31'),
	(15,10,8,'','2016-03-10 15:26:31','2016-03-10 15:26:31'),
	(16,11,8,'','2016-03-10 15:26:31','2016-03-10 15:26:31'),
	(17,21,9,'','2016-03-11 11:07:01','2016-03-11 11:07:01'),
	(18,22,9,'','2016-03-11 11:07:01','2016-03-11 11:07:01'),
	(19,23,9,'','2016-03-11 11:07:01','2016-03-11 11:07:01'),
	(20,10,9,'','2016-03-11 11:07:01','2016-03-11 11:07:01'),
	(21,18,202,'','2016-05-18 14:53:25','2016-05-18 14:53:25'),
	(22,9,202,'','2016-05-18 14:53:25','2016-05-18 14:53:25');

/*!40000 ALTER TABLE `projectaccess` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projectMood
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projectMood`;

CREATE TABLE `projectMood` (
  `projectmoodID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`projectmoodID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projectMood` WRITE;
/*!40000 ALTER TABLE `projectMood` DISABLE KEYS */;

INSERT INTO `projectMood` (`projectmoodID`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'GOOD','project on track ','2015-10-01 02:01:00','2015-10-01 02:01:00'),
	(2,'AVERAGE','project stage is ok ','2015-10-01 02:01:00','2015-10-01 02:01:00'),
	(3,'SAD','project not on track','2015-10-01 02:01:00','2015-10-01 02:01:00');

/*!40000 ALTER TABLE `projectMood` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `projectsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectRef` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `projectNE` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `projectNO` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `projectShow` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `systemID` int(11) DEFAULT NULL,
  `projectName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `prioritiesID` int(11) DEFAULT NULL,
  `projectStatusID` int(11) DEFAULT NULL,
  `suppliersID` int(11) DEFAULT NULL,
  `departmentsID` int(11) DEFAULT NULL,
  `usersID` int(11) DEFAULT NULL,
  `projectManagerID` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `readerID` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `projectStage` enum('DISCOVER','PLAN','DO','IMPLEMENT','CLOSE','NOT DEFINED') COLLATE utf8_unicode_ci NOT NULL,
  `projectMood` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `planDISCOVERStartDate` date DEFAULT NULL,
  `planStartDate` date DEFAULT NULL,
  `planDOStartDate` date DEFAULT NULL,
  `planIMPLEMENTStartDate` date DEFAULT NULL,
  `planCOMPLETEStartDate` date DEFAULT NULL,
  `planDISCOVERDays` int(11) DEFAULT NULL,
  `planDays` int(11) DEFAULT NULL,
  `planDODays` int(11) DEFAULT NULL,
  `planIMPLEMENTDays` int(11) DEFAULT NULL,
  `planCOMPLETEDays` int(11) DEFAULT NULL,
  `actualDISCOVERStartDate` date DEFAULT NULL,
  `actualStartDate` date DEFAULT NULL,
  `actualDOStartDate` date DEFAULT NULL,
  `actualIMPLEMENTStartDate` date DEFAULT NULL,
  `actualCOMPLETEStartDate` date DEFAULT NULL,
  `actualDISCOVERDays` int(11) DEFAULT NULL,
  `actualDays` int(11) DEFAULT NULL,
  `actualDODays` int(11) DEFAULT NULL,
  `actualIMPLEMENTDays` int(11) DEFAULT NULL,
  `actualCOMPLETEDays` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`projectsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`projectsID`, `projectRef`, `projectNE`, `projectNO`, `projectShow`, `systemID`, `projectName`, `prioritiesID`, `projectStatusID`, `suppliersID`, `departmentsID`, `usersID`, `projectManagerID`, `readerID`, `projectStage`, `projectMood`, `planDISCOVERStartDate`, `planStartDate`, `planDOStartDate`, `planIMPLEMENTStartDate`, `planCOMPLETEStartDate`, `planDISCOVERDays`, `planDays`, `planDODays`, `planIMPLEMENTDays`, `planCOMPLETEDays`, `actualDISCOVERStartDate`, `actualStartDate`, `actualDOStartDate`, `actualIMPLEMENTStartDate`, `actualCOMPLETEStartDate`, `actualDISCOVERDays`, `actualDays`, `actualDODays`, `actualIMPLEMENTDays`, `actualCOMPLETEDays`, `created_at`, `updated_at`)
VALUES
	(1,'PRJ0001',NULL,NULL,'Y',NULL,'Lecterns Maintenance',7,7,13,9,456,'16','21','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(2,'PRJ0002',NULL,NULL,'Y',NULL,'PMC Network Facilities Website',6,2,13,2,457,'10','21,24','NOT DEFINED','1','2015-06-08','2015-07-28','2015-06-19','2015-09-15','2015-09-16',1,18,4,4,1,'2015-06-08','2015-08-04','2015-06-19',NULL,NULL,1,18,4,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(3,'PRJ0003',NULL,NULL,'Y',NULL,'PSA Investor Database',7,4,13,2,458,'10','16','NOT DEFINED','1','2015-05-20','2015-06-18','2015-06-12','2015-07-10','2015-07-22',1,11,1,3,1,'2015-05-20','2015-06-18','2015-06-12','2016-05-11','2016-05-12',1,11,1,3,1,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(4,'PRJ0004',NULL,NULL,'Y',NULL,'Bluebox Audits',6,2,13,2,459,'10','21','NOT DEFINED','1','2015-06-08','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(5,'PRJ0005',NULL,NULL,'Y',NULL,'PMC Sales Enquiry Application',7,2,14,3,460,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-10-15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(6,'PRJ0010',NULL,NULL,'Y',NULL,'PMC Goodwood Festival of Speed',7,7,13,4,461,'10','23,16,19','NOT DEFINED','1','2015-06-08','1970-01-01','2015-06-10','1970-01-01','1970-01-01',1,2,1,2,NULL,'2015-06-08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(7,'PRJ0011',NULL,NULL,'Y',NULL,'Brand TV',7,7,13,5,462,'9','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(8,'PRJ0012',NULL,NULL,'Y',NULL,'Technical Operation Web Forms',7,3,13,6,463,'10,8','21','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(9,'PRJ0013',NULL,NULL,'Y',NULL,'PSA Careers Development',3,7,15,7,464,'10','22,21,13','NOT DEFINED','1','2015-08-10','2015-09-18','2015-08-11','2015-10-07','2015-10-08',1,10,1,2,1,'2015-08-10','2015-09-18','2015-08-11','2015-11-03',NULL,1,10,1,2,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(10,'PRJ0014',NULL,NULL,'Y',NULL,'iConnect Free Release - 1st and 2nd piece',7,7,16,8,465,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(11,'PRJ0015',NULL,NULL,'Y',NULL,'PMC Market Allocation',7,2,13,9,456,'10,11','25,18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(12,'PRJ0018',NULL,NULL,'Y',NULL,'Enhanced 360 Dealership Tour ',7,3,13,2,466,'10','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(13,'PRJ0019',NULL,NULL,'Y',NULL,'iSell Click & Go',7,3,13,10,467,'2','25,23','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(14,'PRJ0020','','','Y',0,'Supplier Portal',2,6,13,10,468,'8','16','NOT DEFINED','1','2015-07-20','1970-01-01','1970-01-01','1970-01-01','2016-05-27',2,2,2,12,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 12:39:55'),
	(15,'PRJ0021',NULL,NULL,'Y',NULL,'Internal Portal',7,6,13,10,468,'1','16','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(16,'PRJ0022',NULL,NULL,'Y',NULL,'Web based request process',3,6,13,10,468,'2','23','NOT DEFINED','','2015-10-02','2015-11-15','2015-10-05','2016-05-05','1970-01-01',2,20,1,1,NULL,'2015-10-02','2015-11-15','2015-10-05',NULL,NULL,2,20,1,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(17,'PRJ0023',NULL,NULL,'Y',NULL,'Expansion to PSA Secure File Transfer for Dealers',7,6,13,11,469,'2','23,22,24','NOT DEFINED','','2015-06-01','2015-06-12','2015-06-11','2015-07-31','1970-01-01',4,25,1,1,NULL,'2015-06-01','2015-06-12','2015-06-11','2015-07-31',NULL,4,25,1,1,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(18,'PRJ0024',NULL,NULL,'Y',NULL,'ACIS',7,7,13,12,462,'9','24','NOT DEFINED','1','2015-07-06','2015-07-17','2015-07-14','2015-09-01','1970-01-01',5,5,2,1,NULL,'2015-07-06','2015-07-17','2015-07-14','2015-09-01',NULL,5,5,2,1,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(19,'PRJ0025',NULL,NULL,'Y',NULL,'ACIS Evolutions',7,7,13,12,462,'9','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(20,'PRJ0026',NULL,NULL,'Y',NULL,'Empolo FCA ',4,6,13,7,470,'8','23','NOT DEFINED','1','2015-01-02','2016-03-14','2015-01-02','2015-06-01','2015-06-30',NULL,NULL,NULL,NULL,NULL,'2015-01-02','2016-03-14','2015-01-02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(21,'PRJ0027',NULL,NULL,'Y',NULL,'Empolo 4.0 ',7,6,13,13,471,'8','23','NOT DEFINED','1','2015-06-23','2016-04-22','1900-01-22','2015-06-01','2015-06-30',NULL,NULL,NULL,NULL,NULL,'2015-06-23','1900-01-22','1900-01-22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(22,'PRJ0028',NULL,NULL,'Y',NULL,'Exit planning study',5,6,13,14,472,'8','19','NOT DEFINED','','2015-02-01','2015-03-01','2015-02-01','2015-03-31','2015-04-07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(23,'PRJ0029',NULL,NULL,'Y',NULL,'Lion Country LOT2 ',3,7,17,15,473,'8','19','NOT DEFINED','1','2015-04-01','2016-01-01','1900-04-21','2016-02-01','1970-01-01',1,17,1,5,NULL,'2015-04-01',NULL,'1900-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(24,'PRJ0030',NULL,NULL,'Y',NULL,'Local system cartography ',2,7,13,10,474,'8','8','NOT DEFINED','','2015-07-01','2015-09-01','2015-07-01','2015-12-01','2016-02-01',20,154,10,NULL,NULL,'2015-07-01','2015-07-01','2015-07-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(25,'PRJ0031',NULL,NULL,'Y',NULL,'Special Dealer Claims ',4,2,18,16,475,'10','16','NOT DEFINED','','2014-08-20','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(26,'PRJ0032',NULL,NULL,'Y',NULL,'Fleet Module Direct Debits',5,7,13,7,470,'8','23','NOT DEFINED','1','2015-01-01','2015-01-01','2015-01-01','2016-03-01','2016-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(27,'PRJ0033',NULL,NULL,'Y',NULL,'Aftersales Connect',7,6,16,6,476,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(28,'PRJ0035',NULL,NULL,'Y',NULL,'PLP Evolutions  ',7,2,13,9,477,'10','23','NOT DEFINED','1','2015-04-01','2015-04-01','2015-04-01','2015-10-01','2015-11-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(29,'PRJ0036',NULL,NULL,'Y',NULL,'iConnect Demo Sharing ',7,7,16,17,478,'10','','NOT DEFINED','1','2015-07-20','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(30,'PRJ0037',NULL,NULL,'Y',NULL,'PSA Pretium Website',7,7,19,7,479,'10','2','NOT DEFINED','1','2015-06-29','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(31,'PRJ0038',NULL,NULL,'Y',NULL,'Body Shop Certificates - Phase 2 ',6,3,13,5,480,'11','24','NOT DEFINED','1','2015-12-14','1970-01-01','1970-01-01','1970-01-01','1970-01-01',1,NULL,NULL,NULL,NULL,'2015-07-13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(32,'PRJ0039',NULL,NULL,'Y',NULL,'Customer Incentive Redemption - 2008 Dealer Reactivity',7,7,13,18,481,'10','22','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(33,'PRJ0040',NULL,NULL,'Y',NULL,'SMTP authenticated emails',1,7,13,10,482,'17,13','18,17,13','NOT DEFINED','1','2015-07-21','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-07-21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(34,'PRJ0041',NULL,NULL,'Y',NULL,'Comms Suite Dealers 2015',7,3,20,10,474,'2,24','24,23,16,2','NOT DEFINED','','2015-07-20','2015-09-21','2015-09-01','2015-10-05','2015-10-05',1,7,1,1,1,'2015-07-20',NULL,NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:13:58'),
	(35,'PRJ0042',NULL,NULL,'Y',NULL,'Fleet ipad presenter',6,6,13,19,483,'1','23','NOT DEFINED','','2015-07-31','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-07-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(36,'PRJ0043',NULL,NULL,'Y',NULL,'Ipad Data Capture Application',5,6,13,20,461,'1','23','NOT DEFINED','1','2015-07-27','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 09:53:44'),
	(37,'PRJ0044',NULL,NULL,'Y',NULL,'Isell  Peugeot',7,2,13,21,484,'2','7,11','NOT DEFINED','','2015-07-21','2015-09-03','2015-08-03','2015-10-12','2015-10-21',0,9,3,0,0,'2014-07-21','2015-11-17','2015-08-03','2015-11-23',NULL,0,9,3,NULL,NULL,'2016-05-19 09:53:44','2016-05-19 10:19:28'),
	(38,'PRJ0045',NULL,NULL,'Y',NULL,'EPGC Parts Catalogue',7,6,13,5,485,'1','25','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(39,'PRJ0046',NULL,NULL,'Y',NULL,'Daily Rentals DB',7,3,13,22,486,'9','25','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(40,'PRJ0047',NULL,NULL,'Y',NULL,'File server - Disk Reorganisation',2,7,13,10,474,'13','13','NOT DEFINED','1','2014-10-10','2015-11-11','2015-11-11','2015-11-11','2016-01-25',3,3,3,3,0,'2014-10-10','2015-11-11','2015-11-11','2015-11-11','2016-01-25',3,3,3,3,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(41,'SR0003',NULL,NULL,'Y',NULL,'PLP - Tyre uplift changes',7,7,13,23,487,'9','23','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(42,'SR0004',NULL,NULL,'Y',NULL,'Manheim DNS Migration - Peugeot',7,7,13,24,488,'9','24','NOT DEFINED','1','2015-11-11','2015-11-11','2015-11-11','2015-11-11','2015-11-11',1,1,1,1,1,'2015-11-11','2015-11-11','2015-11-11',NULL,NULL,1,1,1,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(43,'SR0005',NULL,NULL,'Y',NULL,'Manheim DNS Migration - Citroen',7,7,13,25,489,'9','24','NOT DEFINED','1','2015-08-05','1970-01-01','1970-01-01','2015-11-25','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-08-05',NULL,NULL,'1900-01-02',NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(44,'SR0006',NULL,NULL,'Y',NULL,'PCR Email',7,3,13,22,490,'9,2','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(45,'SR0007',NULL,NULL,'Y',NULL,'PX Widget',7,7,13,26,484,'9','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(46,'SR0008',NULL,NULL,'Y',NULL,'Customer Incentive Redemption',7,7,13,3,481,'10','16,22','NOT DEFINED','1','2015-09-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(47,'SR0009',NULL,NULL,'Y',NULL,'Dialog VTS for CUK - VTS1, VTS2, VTS3',7,7,13,8,491,'10','19','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(48,'SR0010',NULL,NULL,'Y',NULL,'Environmental Labels for DS',7,7,13,27,492,'11','22','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(49,'SR0011',NULL,NULL,'Y',NULL,'CarFest North 2015 (and now South 2015)',7,7,13,4,493,'10','23','NOT DEFINED','1','2015-08-25','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-08-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(50,'SR0012',NULL,NULL,'Y',NULL,'Brand TV - Promo Video',7,7,13,23,487,'9','22','NOT DEFINED','1','2015-11-18','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-11-18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(51,'SR0013 ',NULL,NULL,'Y',NULL,'Corporate ID ',7,6,21,2,457,'9,10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(52,'SR0015',NULL,NULL,'Y',NULL,'CSI extracts to Spa',7,7,13,10,494,'','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(53,'SR0016',NULL,NULL,'Y',NULL,'PCR New Job Card',7,2,22,23,495,'9','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(54,'SR0017',NULL,NULL,'Y',NULL,'BACS File Rejection',7,7,13,28,496,'9','17','NOT DEFINED','1','2015-09-10','2015-10-02','2015-09-10','2015-10-19','2015-10-19',1,12,1,2,2,'2015-08-26','2015-10-05','2015-09-10','2015-11-02','2015-11-02',1,12,1,2,2,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(55,'SR0018',NULL,NULL,'Y',NULL,'Outsourcery Migration',3,3,13,25,489,'9','24,13,16','NOT DEFINED','','2015-09-08','2015-09-15','2015-09-14','2015-09-21','2015-09-21',1,3,1,1,1,'2015-09-08','2015-09-15','2015-09-14','2015-09-21',NULL,1,3,1,1,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(56,'SR0019',NULL,NULL,'Y',NULL,'Royal Mail DAF Interface',7,3,13,18,497,'10','','NOT DEFINED','1','2015-10-19','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(57,'SR0020',NULL,NULL,'Y',NULL,'Brand TV - Q3 Changes ',6,7,23,5,487,'9','24,16','NOT DEFINED','','2015-09-14','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-09-14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(58,'SR0021',NULL,NULL,'Y',NULL,'Retail Group Data Files',7,7,13,29,494,'10','24','NOT DEFINED','','2015-09-09','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-09-09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(59,'SR0022',NULL,NULL,'Y',NULL,'Parts Pricing Database Changes',7,6,23,5,498,'2','24,16','NOT DEFINED','1','2015-09-17','1970-01-01','2015-10-01','1970-01-01','1970-01-01',0,NULL,0,NULL,NULL,'2015-09-17',NULL,NULL,NULL,NULL,6,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(60,'SR0023',NULL,NULL,'Y',NULL,'Dialog VTS for CUK - VTS4',7,7,13,8,499,'10','19','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','2015-10-07','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(61,'SR0024',NULL,NULL,'Y',NULL,'Sales Ringround',7,6,13,18,500,'10','16','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(62,'SR0025',NULL,NULL,'Y',NULL,'E-signature',7,3,13,25,501,'9','24,16','NOT DEFINED','1','2015-11-01','2016-01-01','2015-12-01','1970-01-01','1970-01-01',2,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(63,'SR0026',NULL,NULL,'Y',NULL,'TLS Certificate - R&D',7,7,13,22,490,'9','24,16','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(64,'SR0027',NULL,NULL,'Y',NULL,'TLS Certificate - CRG',7,3,13,22,490,'9','24,16','NOT DEFINED','1','2015-09-03','1970-01-01','2015-09-03','2015-10-07','2015-10-07',1,NULL,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(65,'SR0028',NULL,NULL,'Y',NULL,'Children in Need 2015',6,7,24,30,502,'10','','NOT DEFINED','1','2015-09-24','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(66,'SR0029',NULL,NULL,'Y',NULL,'iCan DMS Dealer Survey',5,7,13,31,503,'10','22','NOT DEFINED','1','2015-09-24','2015-11-30','2015-11-23','2015-12-07','2015-12-17',1,6,1,1,2,'2015-09-24','2015-11-27','2015-11-26',NULL,NULL,2,NULL,1,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(67,'SR0030',NULL,NULL,'Y',NULL,'Automation of Juste Apres for Citroen',7,7,13,5,504,'10','19,24,13','NOT DEFINED','1','2015-09-25','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(68,'SR0031',NULL,NULL,'Y',NULL,'Domain Renewals',7,7,13,22,490,'11','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(69,'SR0032',NULL,NULL,'Y',NULL,'Gefco/Faurecia extracts for Fleet Module ',7,3,14,32,470,'8','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(70,'SR0033',NULL,NULL,'Y',NULL,'New vehicle discount automation',7,3,14,32,470,'8','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(71,'SR0034',NULL,NULL,'Y',NULL,'PCR demo vehicles - Empolo',7,2,13,33,505,'8','23','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(72,'SR0035',NULL,NULL,'Y',NULL,'Purchase of domain',7,7,13,26,506,'11','24','NOT DEFINED','1','2015-01-03','1970-01-01','2015-01-30','2015-12-31','1970-01-01',20,NULL,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(73,'SR0036',NULL,NULL,'Y',NULL,'CAP to Autofutura extract',5,7,13,34,507,'11','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(74,'SR0037',NULL,NULL,'Y',NULL,'Buyer Types',7,7,13,8,491,'8','','NOT DEFINED','','2015-10-02','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(75,'SR0038',NULL,NULL,'Y',NULL,'R&D Chiswick - software',7,2,13,5,508,'11','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(76,'SR0039',NULL,NULL,'Y',NULL,'Netnames portal tidy up',7,6,13,10,474,'24','24,25','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(77,'SR0040',NULL,NULL,'Y',NULL,'Etoile workflows',7,6,13,35,483,'24','24','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(78,'SR0041',NULL,NULL,'Y',NULL,'Mail Server Enhancements',7,6,74,10,474,'24','24,13,25','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(79,'SR0042',NULL,NULL,'Y',NULL,'Brand TV boot and build',7,6,13,10,474,'24','25','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(80,'SR0043',NULL,NULL,'Y',NULL,'Vehicle Recalls Widget',7,6,13,6,509,'9,2','23','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(81,'SR0044',NULL,NULL,'Y',NULL,'MIS Payment Processing',2,7,13,28,496,'9','20','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(82,'SR0046',NULL,NULL,'Y',NULL,'Citroen Brand Champion',7,7,13,17,510,'','21','NOT DEFINED','1','2015-09-28','2015-11-30','2015-11-30','2015-01-06','2015-01-07',2,8,1,1,2,'2015-09-28','2015-11-30','2015-11-30','2016-01-18',NULL,2,8,1,1,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(83,'SR0047',NULL,NULL,'Y',NULL,'Special Tools Audit for Aftersales',7,3,14,23,511,'11','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(84,'SR0048',NULL,NULL,'Y',NULL,'Peugeot Shopping Centre Tour (Data Capture)',5,7,13,4,493,'11,24','23,24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','2015-11-19',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2015-12-08',NULL,NULL,NULL,NULL,1,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(85,'SR0049',NULL,NULL,'Y',NULL,'CIR Q4 Dealer Reactivity & PFS Renewing Customers',5,7,13,3,481,'10','22,16','NOT DEFINED','1','2015-10-02','2015-10-05','2015-10-05','2015-10-13','2015-11-12',1,4,1,1,1,'2015-10-02','2015-10-05','2015-10-05','2015-10-13',NULL,1,4,1,1,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(86,'SR0050',NULL,NULL,'Y',NULL,'Archive System',7,2,13,36,512,'12','25','NOT DEFINED','','2015-10-06','2015-10-07','2015-10-06','2015-10-08','2015-10-14',1,1,1,1,1,'2015-10-06','2015-10-07','2015-10-06','2015-10-08',NULL,1,1,1,1,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(87,'SR0051',NULL,NULL,'Y',NULL,'Bodyshop Security phase 1',6,7,13,37,513,'8,11','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(88,'SR0052',NULL,NULL,'Y',NULL,'Nomad LSI investigation',2,7,13,10,468,'8','19','NOT DEFINED','1','2015-10-06','2015-10-19','1970-01-01','2015-10-21','2015-11-19',1,2,NULL,1,1,NULL,'2015-11-09','2015-11-05','2015-11-18','2015-12-03',NULL,NULL,1,2,1,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(89,'SR0053',NULL,NULL,'Y',NULL,'Spender new layout creation',6,3,13,35,483,'8','18','NOT DEFINED','','2015-10-01','2015-11-01','2015-11-01','1970-01-01','1970-01-01',1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(90,'SR0054',NULL,NULL,'Y',NULL,'CTD Externalisation',1,7,13,38,514,'5','5','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2016-04-22',NULL,NULL,NULL,NULL,10,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(91,'SR0055',NULL,NULL,'Y',NULL,'Code Invent Replacement',4,6,14,21,514,'1','','NOT DEFINED','1','1970-01-01','2015-09-01','1970-01-01','2015-10-12','2015-12-01',NULL,20,NULL,35,15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(92,'SR0056',NULL,NULL,'Y',NULL,'Warehouse Management System',0,6,75,22,515,'6','6','NOT DEFINED','1','1970-01-01','2016-01-01','1970-01-01','2016-12-01','2017-01-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(93,'SR0057',NULL,NULL,'Y',NULL,'Service Customer Prospecting',0,6,13,22,516,'6','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(94,'SR0058',NULL,NULL,'Y',NULL,'Autotrader',0,6,13,22,516,'6','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(95,'SR0059',NULL,NULL,'Y',NULL,'Parts hub - Ts and Cs for K Print',5,6,13,22,517,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(96,'SR0060',NULL,NULL,'Y',NULL,'PCR Chingford',0,7,13,22,518,'6','6,15','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(97,'SR0061',NULL,NULL,'Y',NULL,'Citroen City Warranty and Aftersales',0,7,13,22,495,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(98,'SR0062',NULL,NULL,'Y',NULL,'E30 Upgrade',0,6,75,22,518,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(99,'SR0063',NULL,NULL,'Y',NULL,'CDK Support Connection ',2,6,13,22,519,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(100,'SR0065',NULL,NULL,'Y',NULL,'Fidelity SSO Pensions',4,2,76,39,520,'2','23','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(101,'SR0066',NULL,NULL,'Y',NULL,'Windows 10 testing',2,6,77,40,521,'','6','NOT DEFINED','','2015-11-04','2016-01-12','2016-01-06','2016-02-10','2016-02-13',3,20,4,2,1,'2015-11-04',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(102,'SR0067',NULL,NULL,'Y',NULL,'SAGAI to AA feed for DS',4,4,78,6,509,'2,13','1,2','NOT DEFINED','1','2015-10-25','2016-02-25','2015-02-25','2016-03-01','2016-03-07',1,2,1,1,1,'2016-10-25','2016-02-25','2016-02-25','2016-04-11','2016-05-17',2,2,1,1,1,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(103,'SR0068',NULL,NULL,'Y',NULL,'DS Webstore',7,7,79,5,462,'9,2','24','NOT DEFINED','','2015-10-26','2015-11-20','2015-11-10','2015-12-02','2015-12-03',5,1,1,1,1,'2015-10-26','2015-11-20','2015-11-10','2015-12-02','2015-12-03',5,1,1,1,1,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(104,'SR0069',NULL,NULL,'Y',NULL,'PCR Dealership Set-up and Disengagement Process 2015',7,6,13,10,468,'','10','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(105,'SR0070',NULL,NULL,'Y',NULL,'eCRM Report',7,2,13,22,522,'8','17','NOT DEFINED','1','2015-10-23','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:19:28'),
	(106,'SR0071',NULL,NULL,'Y',NULL,'New DS4 Website',7,7,13,25,523,'9','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(107,'SR0072',NULL,NULL,'Y',NULL,'New communications platform Citroen / DS (INForm)',5,7,80,17,524,'2','24','NOT DEFINED','1','2015-12-21','2016-01-10','2016-01-07','2016-02-22','2016-03-01',1,1,1,1,1,'2015-12-21','2016-01-10','2016-01-07','2016-02-22','2016-03-01',1,1,1,1,1,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(108,'SR0073',NULL,NULL,'Y',NULL,'Operational Standards Repository',7,6,13,41,525,'11','22','NOT DEFINED','1','2015-12-14','2016-05-18','2016-04-05','2016-06-23','2016-07-11',1,13,1,1,1,'2016-12-14','2016-05-18','2016-04-05',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(109,'SR0074',NULL,NULL,'Y',NULL,'Spender Rollover',5,7,13,35,483,'8','18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(110,'SR0075',NULL,NULL,'Y',NULL,'Citroen Online Booking',6,7,81,5,487,'11','23,24','NOT DEFINED','','2015-11-01','2015-11-16','2015-11-10','2015-12-01','2015-12-20',1,10,1,6,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(111,'SR0076',NULL,NULL,'Y',NULL,'Dealer Targeting Project',7,3,82,2,503,'2','13,24,23','NOT DEFINED','1','2015-11-05','1970-01-01','1970-01-01','2015-11-24','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24',NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(112,'SR0077',NULL,NULL,'Y',NULL,'POLK data extract',7,6,83,5,497,'11','','NOT DEFINED','','2015-10-01','2016-01-10','2015-11-25','2016-02-15','2015-02-22',4,4,2,2,1,'2015-10-01',NULL,NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(113,'SR0078',NULL,NULL,'Y',NULL,'Brand TV phase 2',0,6,14,5,462,'9,2','23,20,24','NOT DEFINED','1','2015-11-02','2016-05-15','2016-04-25','1970-01-01','1970-01-01',1,120,1,NULL,NULL,'2015-11-02',NULL,'2016-05-10',NULL,NULL,1,NULL,2,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(114,'SR0079','219B3','224C8','Y',NULL,'E-Billing',5,6,77,28,472,'9','17,18','NOT DEFINED','','2015-11-20','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-11-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(115,'SR0080',NULL,NULL,'Y',NULL,'Christmas Campaign Microsite',0,7,84,25,526,'11','16,24','NOT DEFINED','1','2015-10-25','2015-12-01','2015-11-09','2016-02-01','2016-09-30',0,3,0,3,0,'2015-10-25','2015-12-11','2015-11-09','2015-12-18','2015-12-18',NULL,1,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(116,'SR0081',NULL,NULL,'Y',NULL,'Cyber Security',2,6,13,42,527,'4','','NOT DEFINED','1','2015-10-25','2015-12-01','2015-11-09','2015-02-01','2016-09-30',0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(117,'SR0082',NULL,NULL,'Y',NULL,'Peugeot CI changes - 5 sites',0,7,13,22,528,'6','6','NOT DEFINED','1','2015-11-24','1970-01-01','1970-01-01','2015-11-24','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(118,'SR0083',NULL,NULL,'Y',NULL,'R&D Walton redevelopment',0,7,13,22,528,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(119,'SR0084',NULL,NULL,'Y',NULL,'R&D and Citroen Glasgow redevelopment',0,7,13,22,528,'6','6','NOT DEFINED','1','1970-01-01','2016-04-25','1970-01-01','1970-01-01','2016-05-03',NULL,5,NULL,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(120,'SR0085',NULL,NULL,'Y',NULL,'R&D Romford redevelopment',0,7,13,22,528,'6','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(121,'SR0086',NULL,NULL,'Y',NULL,'Autoline showroom rollout for Citroen ',0,6,13,22,529,'15','15','NOT DEFINED','1','2015-12-03','2015-12-03','2015-11-26','2015-12-03','2015-12-03',0,0,0,0,0,'2015-11-26','2015-12-03','2015-12-03','2015-12-03','2015-12-03',0,0,0,0,0,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(122,'SR0087',NULL,NULL,'Y',NULL,'Rochester site sale to Motorline',0,7,13,22,530,'6','15,6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(123,'SR0088',NULL,NULL,'Y',NULL,'R&D Hanwell site close',0,7,13,22,528,'6','15,6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(124,'SR0089',NULL,NULL,'Y',NULL,'Arsenal Predictor',0,7,13,25,531,'10','24','NOT DEFINED','','2015-11-17','2015-12-09','2015-12-09','2016-01-02','2016-01-04',1,2,1,0,1,'2015-11-17','2015-12-09','2015-12-09',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(125,'SR0090',NULL,NULL,'Y',NULL,'D-Web and iConnect Combination Code',0,6,16,8,499,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 09:53:45'),
	(126,'SR0091',NULL,NULL,'Y',NULL,'Aftersales Data Portal',0,4,13,12,532,'10','23','NOT DEFINED','1','2015-11-23','1970-01-01','1970-01-01','1970-01-01','1970-01-01',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:45','2016-05-19 10:13:58'),
	(127,'SR0092',NULL,NULL,'Y',NULL,'Sub Domain for DS',0,7,13,43,523,'11','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(128,'SR0093',NULL,NULL,'Y',NULL,'PLP and Mystery Shop',0,4,13,44,533,'10','16,23','NOT DEFINED','1','2015-11-20','2015-12-14','2015-12-14','2015-12-17','2015-12-17',1,4,1,5,5,'2015-11-20','2015-12-14','2015-12-14',NULL,NULL,1,4,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(129,'SR0094',NULL,NULL,'Y',NULL,'BrandTV – PCR exempt accounts',7,2,14,5,487,'2','20','NOT DEFINED','1','2015-12-22','1970-01-01','2016-01-15','1970-01-01','1970-01-01',1,NULL,1,NULL,NULL,'2015-12-22',NULL,'2016-01-15',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(130,'SR0095',NULL,NULL,'Y',NULL,'A48 Citroën Ireland',1,4,85,45,534,'3','3','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(131,'SR0096',NULL,NULL,'Y',NULL,'iSell Citroen (2nd phase pilots)',1,2,14,21,484,'2','','NOT DEFINED','','2015-06-30','2015-08-24','2015-08-10','2015-09-28','2015-12-14',7,5,10,35,5,'2015-06-30','2015-08-24','2015-08-10','2015-09-28',NULL,7,5,10,35,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(132,'SR0097',NULL,NULL,'Y',NULL,'IC@N ',2,6,86,2,503,'1','','NOT DEFINED','','2015-12-03','2015-12-04','2015-12-03','2016-02-25','2015-03-11',2,60,1,10,1,'2015-12-03',NULL,'2015-12-03',NULL,NULL,2,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(133,'SR0098',NULL,NULL,'Y',NULL,'January Sales Domains',7,7,13,22,490,'9,2','24','NOT DEFINED','1','2015-12-02','2015-12-10','2015-12-02','2015-12-10','2015-12-15',1,1,1,0,0,'2015-12-02','2015-12-10','2015-12-02','2015-12-10','2015-12-15',1,1,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(134,'SR0099',NULL,NULL,'Y',NULL,'Redhill Server replacement',6,1,14,46,535,'7','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(135,'SR0100',NULL,NULL,'Y',NULL,'Dynamic Hybrid',6,1,14,22,518,'7','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(136,'SR0101',NULL,NULL,'Y',NULL,'Delivery of CRM data to dealers',7,2,14,25,536,'2','23','NOT DEFINED','','2015-12-15','1970-01-01','1970-01-01','1970-01-01','1970-01-01',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(137,'SR0102',NULL,NULL,'Y',NULL,'Experian Payment Gateway Upgrade',0,2,13,10,537,'13','13,17','NOT DEFINED','','2015-12-07','2016-05-03','2016-04-04','1970-01-01','1970-01-01',2,2,1,NULL,NULL,'2015-12-07','2016-05-19','2016-04-04',NULL,NULL,2,0,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(138,'SR0103',NULL,NULL,'Y',NULL,'Webmaster for Drive DS',5,3,13,43,523,'11,23','24','NOT DEFINED','1','2015-12-09','2015-12-14','2015-12-09','2015-12-14','2015-12-15',0,0,0,1,0,'2015-12-09',NULL,'2015-12-09',NULL,NULL,0,NULL,0,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(139,'SR0104',NULL,NULL,'Y',NULL,'eMaster and Customer Incentive Redemption',0,6,13,3,538,'10','16','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(140,'SR0105',NULL,NULL,'Y',NULL,'PCR Portal',6,6,14,22,486,'1','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(141,'SR0106',NULL,NULL,'Y',NULL,'UK Greeting Cards DNS',5,7,87,47,539,'11','24','NOT DEFINED','1','2015-12-16','2015-12-17','2015-12-17','2015-12-17','2015-12-17',0,0,0,0,0,'2015-12-17','2015-12-17','2015-12-17','2015-12-17','2015-12-17',0,0,0,0,0,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(142,'SR0107',NULL,NULL,'Y',NULL,'D-Web and I-Connect Lead Time Calculator',0,1,16,8,499,'10','','NOT DEFINED','1','2015-12-16','1970-01-01','1970-01-01','1970-01-01','1970-01-01',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(143,'SR0108',NULL,NULL,'Y',NULL,'Paperless HR',7,3,14,7,542,'9','','NOT DEFINED','1','2015-10-26','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2015-10-26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(144,'SR0109',NULL,NULL,'Y',NULL,'Used Car Prep Centre Software',1,6,13,22,516,'2','7','NOT DEFINED','1','2015-12-24','2016-03-01','2016-02-28','2016-05-09','2016-07-01',15,80,1,7,2,'2015-12-24','2016-03-01','2016-02-28',NULL,NULL,15,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(145,'SR0110',NULL,NULL,'Y',NULL,'SAP central billing invoice printing ex. CTD',5,7,13,48,497,'13','13','NOT DEFINED','1','2015-12-22','2016-01-11','2016-01-11','2016-01-11','2016-02-01',1,1,1,1,0,'2015-12-22','2016-01-11','2016-01-11','2016-01-11','2016-02-01',1,1,1,1,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(146,'SR0111',NULL,NULL,'Y',NULL,'Goldmine Harmonisation - email migration',6,3,13,49,483,'11','24','NOT DEFINED','1','2015-12-22','2016-04-25','2016-04-19','2016-04-25','2016-04-28',0,0,0,1,0,'2015-12-22',NULL,'2016-04-19',NULL,NULL,0,NULL,0,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(147,'SR0112',NULL,NULL,'Y',NULL,'Spender - FDP Rollover',5,7,13,35,483,'8','18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(148,'SR0113',NULL,NULL,'Y',NULL,'Update to AutoVHC Webservice',0,6,14,22,516,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(149,'SR0114',NULL,NULL,'Y',NULL,'Brand TV - Change the Tyre Screen & Welcome Screen to assets',0,3,14,48,487,'2','20','NOT DEFINED','1','2016-01-05','2016-01-20','2016-01-19','2016-02-03','2016-02-29',1,10,1,2,1,'2016-01-05',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(150,'SR0115',NULL,NULL,'Y',NULL,'Payment of Handling Fees move from Essor to Spender',0,7,14,14,541,'10','18','NOT DEFINED','1','2016-01-07','2016-01-25','2016-01-25','2016-01-14','1970-01-01',1,6,1,3,1,'1900-01-22',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(151,'SR0116','P059004',NULL,'Y',NULL,'Pinley House Consolidation',1,6,86,7,542,'4','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(152,'SR0117','P059019',NULL,'Y',NULL,'Data mining / reporting system (BOARD or Exadata + Qlikview)',1,6,88,50,543,'1','13,19','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(153,'SR0118',NULL,NULL,'Y',NULL,'DS Applicant Portal',5,4,16,2,458,'10','21','NOT DEFINED','1','2016-01-14','2016-01-18','2016-01-15','2016-01-27','2016-01-30',1,7,1,1,1,'2016-01-15','2016-01-18','2016-01-18',NULL,NULL,1,7,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(154,'SR0119',NULL,NULL,'Y',NULL,'Spender - Unpaid Payments Archiving',5,6,87,14,544,'8','18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(155,'SR0120',NULL,NULL,'Y',NULL,'PCR/UK Synergimpro',1,6,14,51,545,'3','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(156,'SR0121','22155',NULL,'Y',NULL,'Remplacement  PSAV4 PCR UK',1,6,14,51,545,'3','','NOT DEFINED','1','2014-11-07','2015-09-01','1970-01-01','2015-10-01','2016-11-03',NULL,NULL,NULL,NULL,1,'2014-11-07','2015-09-01',NULL,'2015-10-01',NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(157,'SR0122',NULL,NULL,'Y',NULL,'Autoline L1 Vigo',1,6,89,22,486,'6','','NOT DEFINED','1','1970-01-01','2016-03-15','1970-01-01','2016-06-28','2016-07-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(158,'SR0123',NULL,NULL,'Y',NULL,'Voluntary Benefits Review',7,6,14,7,470,'2','','NOT DEFINED','1','2016-04-11','2016-05-15','2016-05-01','2016-11-01','2016-12-01',2,NULL,1,NULL,NULL,'2016-04-11',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(159,'SR0124',NULL,NULL,'Y',NULL,'Mobile5 Redirect to new server',7,2,87,52,546,'12','24','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(160,'SR0125',NULL,NULL,'Y',NULL,'SMMT migration to new platform',6,6,90,53,14,'8','','NOT DEFINED','','2016-01-28','2016-01-28','2016-01-28','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(161,'SR0126',NULL,NULL,'Y',NULL,'PCR 6 additional domains',5,7,91,22,490,'2','24','NOT DEFINED','1','2016-02-01','2016-02-03','2016-02-02','2016-02-04','2016-02-04',1,1,1,1,1,'2016-02-01','2016-02-03','2016-02-02','2016-02-04','2016-02-04',1,1,1,1,1,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(162,'SR0127',NULL,NULL,'Y',NULL,'CSI Reward Programme 2016',6,4,13,44,548,'10','22','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','2016-04-06','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-04-06',NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(163,'SR0128',NULL,NULL,'Y',NULL,'Juste Apres Dialog Extract for Peugeot',7,6,14,54,504,'11','19','NOT DEFINED','1','2016-02-09','2016-04-11','2016-03-09','2016-04-27','2016-05-06',1,11,1,1,1,'2016-02-09','2016-04-12','2016-03-08',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(164,'SR0129',NULL,NULL,'Y',NULL,'Laravel Standards and Servers for PCR',7,7,92,22,490,'11','13,16','NOT DEFINED','1','2016-02-09','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,'2016-02-09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(165,'SR0130',NULL,NULL,'Y',NULL,'5 iPads for Data Capture',7,7,14,55,461,'11','24','NOT DEFINED','1','2016-02-08','2016-01-12','2016-02-09','2016-02-12','2016-02-24',0,0,0,0,0,'2016-02-08',NULL,'2016-02-09',NULL,NULL,0,NULL,0,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(166,'SR0131',NULL,NULL,'Y',NULL,'Evolution on Citroën Brand Champion website',7,3,14,21,549,'10','','NOT DEFINED','1','2016-02-08','1970-01-01','2016-04-15','1970-01-01','1970-01-01',1,NULL,1,NULL,NULL,'2016-02-08',NULL,'2016-04-15',NULL,'2016-04-25',1,NULL,1,NULL,1,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(167,'SR0132',NULL,NULL,'Y',NULL,'Employee New Vehicles Sales Form',0,6,14,7,550,'10','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(168,'SR0133',NULL,NULL,'Y',NULL,'Values Training (iPads)',7,7,13,7,551,'11','','NOT DEFINED','1','2016-02-15','2016-02-18','2016-02-16','2016-02-24','2016-04-26',0,1,NULL,0,0,'2016-02-15','2016-02-18','2016-02-16','2016-02-24','2016-04-29',0,1,0,0,0,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(169,'SR0134',NULL,NULL,'Y',NULL,'PCR Cron Job set up',5,7,13,22,490,'11','24','NOT DEFINED','1','2016-02-18','2016-02-18','2016-02-18','2016-02-18','2016-02-18',0,0,0,0,0,'2016-02-18','2016-02-18','2016-02-18','2016-02-18',NULL,0,NULL,NULL,0,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(170,'SR0135',NULL,NULL,'Y',NULL,'Trackback',6,7,13,9,456,'11','16','NOT DEFINED','1','2016-02-23','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(171,'SR0136',NULL,NULL,'Y',NULL,'PCR Corporate ID Rebranding',0,6,14,22,529,'10','6','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(172,'SR0137',NULL,NULL,'Y',NULL,'DS Applicant Portal Phase 2',5,4,13,2,552,'10','16','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','2016-04-06','1970-01-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'2016-04-06',NULL,NULL,NULL,NULL,1,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(173,'SR0138',NULL,NULL,'Y',NULL,'AFRL Infrastructure Changes April 2017',0,6,14,8,499,'13','19','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','2017-04-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(174,'SR0139',NULL,NULL,'Y',NULL,'DS Drive Email',0,7,14,56,553,'11','','NOT DEFINED','1','2016-02-25','2016-03-02','2016-03-01','2016-03-02','2016-03-04',1,0,0,1,NULL,'2016-02-25','2016-03-03','2016-03-03','2016-03-03','2016-03-04',1,0,0,1,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(175,'SR0140',NULL,NULL,'Y',NULL,'Parts & Service Database',0,2,14,48,511,'11','','NOT DEFINED','1','2016-03-04','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(176,'SR0141',NULL,NULL,'Y',NULL,'eCommerce Dealer Sign-up Portal',0,3,14,21,554,'11','','NOT DEFINED','1','2016-03-08','2016-04-20','2016-04-20','2016-05-11','2016-05-18',1,NULL,NULL,NULL,NULL,'2016-03-08','2016-04-19','2016-04-18',NULL,NULL,2,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(177,'SR0142',NULL,NULL,'Y',NULL,'CSSB UK registration lookup & automation',6,7,14,48,555,'2','19,24,13','NOT DEFINED','1','2016-02-29','2016-03-08','2016-03-08','2016-04-03','2016-04-11',1,1,1,1,1,'2016-02-29','2016-03-08','2016-03-08','2016-04-03','2016-04-11',1,1,1,1,0,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(178,'SR0143',NULL,NULL,'Y',NULL,'WeTransfer',0,2,14,57,556,'12','24','NOT DEFINED','1','2016-03-03','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(179,'SR0144',NULL,NULL,'Y',NULL,'Ekimetrics',0,6,93,21,557,'11','17','NOT DEFINED','1','2016-03-04','2016-03-30','2016-04-04','2016-04-03','2016-04-27',1,5,1,1,1,'2016-03-04','2016-03-31','2016-03-30',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(180,'SR0145',NULL,NULL,'Y',NULL,'Spender Additional fields & * removal',0,4,14,58,558,'12','18,19','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(181,'SR0146',NULL,NULL,'Y',NULL,'Fleet Module changes for HMRC',5,7,14,7,559,'8','23','NOT DEFINED','1','2016-03-02','2016-03-03','2016-03-02','2015-04-11','2015-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(182,'SR0147',NULL,NULL,'Y',NULL,'Local Systems Handover',5,6,14,10,468,'8','24,18,19','NOT DEFINED','','2016-03-11','2016-03-11','2016-03-11','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(183,'SR0148',NULL,NULL,'Y',NULL,'Fleet 3 Pillars Programme',0,3,94,48,513,'11','','NOT DEFINED','1','2016-03-16','1970-01-01','2016-04-11','1970-01-01','1970-01-01',1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(184,'SR0149',NULL,NULL,'Y',NULL,'Aftersales Data Portal - Service Retention System Enhancements',0,6,14,48,511,'11','','NOT DEFINED','1','2016-03-16','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(185,'SR0150',NULL,NULL,'Y',NULL,'VPD / AO paint codes',7,1,14,14,560,'8','18','NOT DEFINED','1','2016-03-17','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(186,'SR0151',NULL,NULL,'Y',NULL,'SMR, FMP & RMP Tender',6,6,14,48,497,'8','','NOT DEFINED','1','2016-03-29','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(187,'SR0152',NULL,NULL,'Y',NULL,'eCommerce domain creation & SSL',6,2,14,21,561,'2','24','NOT DEFINED','1','2016-03-21','2016-04-20','2016-04-01','2016-04-21','2016-04-22',1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:19:28'),
	(188,'SR0153',NULL,NULL,'Y',NULL,'Trade Team Platform',0,6,14,48,462,'2','','NOT DEFINED','1','2016-03-22','2016-05-11','2016-05-01','2016-05-17','2016-05-20',1,1,1,1,1,'2016-03-22',NULL,'2016-05-01',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(189,'SR0154',NULL,NULL,'Y',NULL,'5 ipads for Gadget Show',5,7,14,55,461,'2','24','NOT DEFINED','1','2016-03-22','2016-03-23','2016-03-22','2016-03-24','2016-04-05',0,0,0,0,1,'2016-03-22','2016-03-23','2016-03-22','2016-03-24','2016-04-04',0,0,0,0,1,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(190,'SR0155',NULL,NULL,'Y',NULL,'10 ipads for CV show and additional dev work',6,7,14,55,562,'2','23,24','NOT DEFINED','1','2016-04-05','2016-04-20','2016-04-19','2016-04-26','2016-05-04',1,2,1,1,0,'2016-04-05','2016-04-20','2016-04-19','2016-04-26',NULL,1,2,1,1,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(191,'SR0156',NULL,NULL,'Y',NULL,'Selling Online – PX and Insurance domain',6,3,95,21,456,'2','24','NOT DEFINED','1','2016-03-29','1970-01-01','1970-01-01','2016-04-18','1970-01-01',1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(192,'SR0157',NULL,NULL,'Y',NULL,'CUK Sales Secure File Transfer',7,6,14,17,563,'2','23,24','NOT DEFINED','1','2016-04-06','2016-04-25','2016-04-11','1970-01-01','1970-01-01',1,5,1,NULL,NULL,'2016-04-06',NULL,'2016-04-11',NULL,NULL,1,NULL,1,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(193,'SR0158',NULL,NULL,'Y',NULL,'Kerridge – Direct email facility for invoices',7,7,75,28,564,'6','6','NOT DEFINED','1','2016-04-04','2016-04-13','2016-04-11','1970-01-01','2016-04-30',1,1,2,NULL,1,'2016-04-04','2016-04-04',NULL,NULL,'2016-04-04',1,1,NULL,NULL,1,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(194,'SR0159',NULL,NULL,'Y',NULL,'PSA Peugeot Citroen rebranding',6,6,14,10,565,'4','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(195,'SR0160',NULL,NULL,'Y',NULL,'Citroen Core Dealer Database improve reliability',7,6,14,10,566,'2','20,17','NOT DEFINED','1','2016-04-11','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(196,'SR0161',NULL,NULL,'Y',NULL,'Unified Communications Platform (Lync)',2,1,96,59,542,'4','7','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(197,'SR0162',NULL,NULL,'Y',NULL,'Network Management Tools',6,1,97,2,459,'9','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(198,'SR0163',NULL,NULL,'Y',NULL,'Parts Product Directory',7,6,14,48,567,'11','','NOT DEFINED','1','2016-04-19','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(199,'SR0164',NULL,NULL,'Y',NULL,'PSA Careers Website Changes',6,4,14,7,551,'12','','NOT DEFINED','1','2016-04-26','1970-01-01','1970-01-01','1970-01-01','1970-01-01',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 10:13:58'),
	(200,'SR0165',NULL,NULL,'Y',NULL,'INFORM – Online PDF / Image Hosting',7,1,14,21,524,'2','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(201,'SR0166',NULL,NULL,'Y',NULL,'Independent Garages Website',7,6,14,48,462,'2','24,1','NOT DEFINED','1','2016-04-22','1970-01-01','1970-01-01','2016-06-01','1970-01-01',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(202,'SR0167',NULL,NULL,'Y',NULL,'Buyback Changes',7,6,14,14,14,'8','18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(203,'SR0168',NULL,NULL,'Y',NULL,'PSA Online Application Update',7,6,13,8,499,'9','17','NOT DEFINED','1','2016-05-05','1970-01-01','2016-05-11','2016-12-01','2017-02-01',1,NULL,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(204,'SR0169',NULL,NULL,'Y',NULL,'DS Franchising Sign-Off',6,6,14,2,14,'9','','NOT DEFINED','1','2016-05-10','1970-01-01','2016-05-10','2016-05-15','1970-01-01',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(205,'SR0170',NULL,NULL,'Y',NULL,'Groupe PSA - Signator Creator',2,6,14,7,542,'4','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(206,'SR0171',NULL,NULL,'Y',NULL,'Centralised Storage for Dealer Files',6,1,14,22,529,'6','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(207,'SR0172',NULL,NULL,'Y',NULL,'Sales Types Harmonisation Procedures',7,6,14,22,529,'9','17','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(208,'SR0173',NULL,NULL,'Y',NULL,'DVLA File Processing',6,6,14,8,499,'8','','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(209,'SR0174',NULL,NULL,'Y',NULL,'Spender Extended Warranty & Breakdown Cover Accruals',7,6,14,35,483,'8','18','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(210,'SR0175',NULL,NULL,'Y',NULL,'iPads & Data Capture for Tennis Aegon Events',7,6,13,55,493,'9','24,23','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:46','2016-05-19 09:53:46'),
	(211,'SR0176',NULL,NULL,'Y',NULL,'Citroen Invoice Risk',6,6,14,28,14,'8','17','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:47','2016-05-19 09:53:47'),
	(212,'SR0177',NULL,NULL,'Y',NULL,'ECRM changes',6,1,14,22,529,'9','17','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:47','2016-05-19 09:53:47'),
	(213,'SR0178',NULL,NULL,'Y',NULL,'iPads & changes Royal Bath & West',6,6,14,55,493,'9','24','NOT DEFINED','1','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:47','2016-05-19 09:53:47'),
	(214,'SR0179',NULL,NULL,'Y',NULL,'CSI Rewards websites updates',6,1,14,44,548,'10','','NOT DEFINED','','1970-01-01','1970-01-01','1970-01-01','1970-01-01','1970-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-05-19 09:53:47','2016-05-19 09:53:47');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table supplier_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier_users`;

CREATE TABLE `supplier_users` (
  `supplier_usersID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`supplier_usersID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `supplier_users` WRITE;
/*!40000 ALTER TABLE `supplier_users` DISABLE KEYS */;

INSERT INTO `supplier_users` (`supplier_usersID`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(456,'Chris Wilkes','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(457,'Marko Engel','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(458,'Tracey Richards','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(459,'Jennifer Roberts','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(460,'Sarah Powell','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(461,'Steven Fahey','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(462,'Colin Start','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(463,'Ian Sedgwick','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(464,'Philipa Hunter','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(465,'PLD','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(466,'Andrew Saunders','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(467,'Denise McCabe','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(468,'Vijay Mistry','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(469,'Will Bracken/Suzanne Cole','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(470,'Rebecca Seymour','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(471,'Rebecca Seymour/Mark Gardner/Anne Lyndon/Andrew Newman','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(472,'Bryn Thomas','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(473,'Chris Stewart','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(474,'ITUK','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(475,'Bryn Thomas/Simon Bisp','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(476,'Spencer Carter','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(477,'Chris Wilkes/Andrew Hone','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(478,'Philip Brown','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(479,'Nicola-Jane Thomas','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(480,'Janet Andrews/Peter Ireland','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(481,'Gareth Houston/Simon Bisp','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(482,'DSIN Central','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(483,'Scott Westerby','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(484,'Maria Rogers','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(485,'Dominic Walton','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(486,'Simon Lawrence','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(487,'Alex Ollerearnshaw','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(488,'Karen Dean','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(489,'Richard Dewsbury','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(490,'Harry Pennington','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(491,'John Smitton','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(492,'Phil Howkins','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(493,'Sylvie Planchard','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(494,'Mike Heath','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(495,'John Apicella','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(496,'Guy McCarthy','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(497,'Janet Andrews','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(498,'Shane Calloway','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(499,'David McKenna','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(500,'Stuart Bullimore/Bertrand Lizon','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(501,'Ségolène Lerouge','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(502,'Louise Dowling','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(503,'Annemieke McKeown','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(504,'Brian Roskell','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(505,'Anita Bains','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(506,'Andy Baird','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(507,'Bob Grant','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(508,'Bob Footner','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(509,'Simon Owen','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(510,'Laurent Baumant','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(511,'James Whiting','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(512,'Kerry Gough','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(513,'Peter Ireland','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(514,'Various','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(515,'Toni Foryszeski','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(516,'David Male','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(517,'Mark Baddeley','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(518,'PCR','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(519,'CDK','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(520,'David Male / David Connell','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(521,'All','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(522,'Christine Stewart','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(523,'Pri Patel','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(524,'Ed Hickin','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(525,'Miriam Start/Helen Bradshaw','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(526,'Lynda Carey','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(527,'Stephane Le Guevel','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(528,'Nigel Wells','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(529,'Andrew Newman','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(530,'David March','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(531,'Segolene Lerouge','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(532,'Andrew Hall','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(533,'Miriam Start','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(534,'Olivier Daurele','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(535,'Alex Mayor','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(536,'Neil Starling','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(537,'Paul Cozens','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(538,'Lynn Woods/Gareth Houston','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(539,'Priyank Patel','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(540,'Nichola-Jayne Thomas','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(541,'Julie Cozens','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(542,'David Connell','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(543,'Simon Bisp','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(544,'Bryn Thomas/Julie Cozens','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(545,'PCR/UK Dealers','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(546,'Zoe Macleay','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(547,'Sarah Powell, John Smitton','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(548,'Suzanne Cole','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(549,'Anna Bereau / Laurent Baumann','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(550,'Rebecca Seymour/Daniel Bailey','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(551,'Philippa Hunter','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(552,'Jessica Johnston','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(553,'Phil Price / Ellen Beane','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(554,'Andrew Baird','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(555,'Dominic Walton/Romaric Cothenet','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(556,'Neville Staines','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(557,'Laura Smith','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(558,'Cullen Shaw/Julie Cozens','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(559,'Lianne Godfrey','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(560,'Mujahid Raja','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(561,'Ollie Koczkowski','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(562,'Syvlie Planchard/Steven Fahey','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(563,'Cath Sharpe','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(564,'Emma Lawrence','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(565,'Ian Shilstone','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(566,'Mark Tweedie, Annemieke McKeown','','2016-04-27 15:11:07','2016-04-27 15:11:07'),
	(567,'John Truman','','2016-04-27 15:11:07','2016-04-27 15:11:07');

/*!40000 ALTER TABLE `supplier_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table suppliers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `suppliersID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`suppliersID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;

INSERT INTO `suppliers` (`suppliersID`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(13,'ITUK','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(14,'TBD','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(15,'Cornerstone','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(16,'Triad','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(17,'DSIN/ITUK','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(18,'Spain','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(19,'Shilling','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(20,'Gordano,Image+,ITUK','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(21,'Blue Moon','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(22,'Autoline','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(23,'AVCO','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(24,'ITUK/Image+','','2016-04-27 15:12:31','2016-04-27 15:12:31'),
	(62,'ITUK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(63,'TBD','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(64,'Cornerstone','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(65,'Triad','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(66,'DSIN/ITUK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(67,'Spain','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(68,'Shilling','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(69,'Gordano,Image+,ITUK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(70,'Blue Moon','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(71,'Autoline','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(72,'AVCO','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(73,'ITUK/Image+','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(74,'Gordano,ITUK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(75,'CDK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(76,'Fidelity','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(77,'FR','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(78,'FR, ITUK','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(79,'Codestorm','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(80,'Challengera','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(81,'Gforces','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(82,'CACI','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(83,'ITUK/HIS','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(84,'Mobile5','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(85,'CDK Global','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(86,'Various','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(87,'DSIN','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(88,'BOARD or Group','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(89,'Vigo','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(90,'SMMT','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(91,'Netnames','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(92,'Image+','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(93,'Ekimetrics','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(94,'Rawww','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(95,'Summitmedia','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(96,'TRVD','','2016-04-27 15:14:31','2016-04-27 15:14:31'),
	(97,'TBC','','2016-04-27 15:14:31','2016-04-27 15:14:31');

/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `usersID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userType` enum('SYSTEMADMIN','PROJECTADMIN','PROJECTMANAGER','READER') COLLATE utf8_unicode_ci NOT NULL,
  `userGroup` enum('BUILD','RUN') COLLATE utf8_unicode_ci NOT NULL,
  `isDeleted` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NickName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`usersID`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`usersID`, `username`, `password`, `email`, `userType`, `userGroup`, `isDeleted`, `firstName`, `lastName`, `NickName`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'PF27934','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','Vijay.mistry@mpsa.com','SYSTEMADMIN','BUILD','N','vijay','mistry','VM','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'PF55069','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','Denise.mccabe@mpsa.com','SYSTEMADMIN','BUILD','N','Denise','McCabe','DM','','2015-12-29 10:24:13','2015-12-29 10:24:13'),
	(3,'PF26990','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','stephen.wall@mpsa.com','PROJECTADMIN','BUILD','N','Stephen','Wall','SJW','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,'CX00750','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','ian.shilstone@mpsa.com','PROJECTADMIN','BUILD','N','Ian','Shilstone','IS','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,'PF30299','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','peter.quince@mpsa.com','PROJECTADMIN','BUILD','N','Peter','Quince','PQ','','2016-01-13 11:57:56','2016-01-13 11:57:56'),
	(6,'PF31734','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','michael.heath@mpsa.com','PROJECTADMIN','BUILD','N','Mike','Heath','MHe','','2016-01-17 15:39:48','2016-01-17 15:39:48'),
	(7,'PF31696','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','terry.ward@mpsa.com','PROJECTADMIN','BUILD','N','Terry','Ward','TW','','2016-01-17 15:41:06','2016-01-17 15:41:06'),
	(8,'PF30222','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','richard.short@mpsa.com','PROJECTMANAGER','BUILD','N','Richard','Short','RS','','2016-03-01 17:07:26','2016-03-01 17:07:26'),
	(9,'PF26166','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','louise.dodd@mpsa.com','PROJECTMANAGER','BUILD','N','Louise','Dodd','LD','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,'E381713','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','karen.tranter@ext.mpsa.com','PROJECTMANAGER','BUILD','N','Karen','Tranter','KT','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,'E455430','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','julie.rogers@ext.mpsa.com','PROJECTMANAGER','BUILD','N','Julie','Rogers','JR','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,'E421920','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','sarah.waring@ext.mpsa.com','PROJECTMANAGER','BUILD','N','Sarah','Waring','SRW','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,'PF27347','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','stephen.iwanowicz@mpsa.com','PROJECTMANAGER','BUILD','N','Stephen','Iwanowicz','SI','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,'PF30046','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','robert.mcdonald@mpsa.com','PROJECTMANAGER','BUILD','N','Bob','McDonald','BMc','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,'U445108','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','kevin.delieu@mpsa.com','PROJECTMANAGER','BUILD','N','Kevin','Delieu','KD','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,'U092203','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','mark.tweedie@mpsa.com','READER','BUILD','N','Mark','Tweedie','MT','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,'PF30374','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','alan.moores@mpsa.com','READER','BUILD','N','Alan','Moores','AM','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,'E182411','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','patrick.arnold@ext.mpsa.com','READER','BUILD','N','Pat','Arnold','PA','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(19,'PF54790','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','mark.hubbard@mpsa.com','READER','BUILD','N','Mark','Hubbard','MHu','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,'E473628','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','edward.talbot@ext.mpsa.com','READER','BUILD','N','Ed','Talbot','ET','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,'E464770','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','selva.chinnakalai@ext.mpsa.com','READER','BUILD','N','Selva','Chinnakalai','SC','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(22,'E421029','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','bini.nair@ext.mpsa.com','READER','BUILD','N','Bini','Nair','BN','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(23,'E453254','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','simon.wilmer@ext.mpsa.com','READER','BUILD','N','Simon','Wilmer','SW','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(24,'E364233','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','david.gearey@ext.mpsa.com','READER','BUILD','N','Dave','Gearey','DG','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(25,'U466624','$2y$10$8JGa.KKjHKU1b8trahXhSOkrbkwS3gW.BZFJYUNS45KaSWEvyLAC2','liviu-andrei.popescu@mpsa.com','READER','BUILD','N','Liviu','Popescu','LP','','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
