<?php

return [
        /**
        *   CRON TASKS
        */
        'GRACE_PERIOD_CLOSE'      			=>      2,       // 2 DAYS
        'GRACE_PERIOD_ON_HOLD'      		=>      2,       // 2 DAYS
        'GRACE_PERIOD_COMPLETE'      		=>      2,       // 2 DAYS
        'GRACE_PERIOD_REJECTED'      		=>      2,       // 2 DAYS
        'GRACE_PERIOD_WASHUP'      			=>      2,       // 2 DAYS
        /**
         * PROJECT PHASE COLOR CODES
        */
        'DISCOVER'                  =>      "#03C414",       
        'PLAN'                      =>      "#BEC403",       
        'DO'                        =>      "#C45903",       
        'IMPLEMENT'                 =>      "#03C485",       
        'COMPLETE'                  =>      "#03C485",       
        'NOT DEFINED'               =>      "#F2DEDE",       
        'CLOSE'                     =>      "#25626b",       
        'NOT STARTED'               =>      "#ccc57f",       
        'PM-Y'                      =>      "#F2DEDE",       
        'PM-D'                      =>      "#F2DEDE",
        'NI-S'                      =>      "#FFFFFF",

        /**
        * ABBREVATIONS D - DEFINITION
        */
        'PM-D-D'            => "ALERT 1: Missing planning data",
        'PM-Y-D'            => "ALERT 2: Missing planning data",  
        'NI-S-D'            => "ALERT 3: Project started! But project is not in any stage at the moment",
        'NOT DEFINED-D'     => "ALERT 4: Current project stage is not defined by project manager",
       /**
        *  LOGGING TASKS
        */
        'OLD_PROJECTS'      =>  "2016-06-30 00:00:00", //30th june 2016
       /**
        * 
        */
    ];

?>